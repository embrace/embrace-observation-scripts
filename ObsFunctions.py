"""
$Id: ObsFunctions.py 
     python script defining some functions for EMBRACE observations
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$license: GPLv3 or later, see https://www.gnu.org/licenses/gpl-3.0.txt
$created: Sat Jul 30 17:31:19 CEST 2011

$modified: Wed Sep  7 16:07:44 CEST 2011
  calibration method using GPS-F1
  and generalized to any GPS satellite

$modified: Fri Sep 16 11:47:39 CEST 2011
  adding logging method

$modified: Sat Sep 17 17:08:10 CEST 2011
  the y-offset for the satellite drift scan is now optional (method: SatDrift)

$modified: Mon Sep 19 12:14:23 CEST 2011
  renamed from Observation-functions.py to ObsFunctions.py
  so that it can be imported.  python does not allow minus sign in the filename for imports

$modified: Mon Sep 19 13:40:07 CEST 2011
  finally found the bug in the satcal method.
  I was missing the pointing update compute before writing to the digital beam text file

$modified: Fri Sep 23 09:37:12 CEST 2011
  in satcal()
    - rfcaltime becomes a class variable instead of local to satcal()
    - digcaltime is a function of the number of beamlets (1.1 seconds per beamlet)
    - new keyword "now", which forces the calibration to occur exactly when requested
      and not at next_transit which is the default
  new method Acquire()

$modified: Fri Sep 30 12:24:26 CEST 2011
  new method: FindSat()
  download TLE data, and find the requested satellite
  this is now extended beyond the GPS satellites.  eg. GIOVE

$modified: Thu Oct  6 10:15:55 CEST 2011
  added RFcentre verification to the satcal() method

$modified: Fri Oct  7 14:17:58 CEST 2011
  new method parseargs() to parse arguments from the command line of the main scripts
  satname becomes a class global variable

$modified: Sat Oct  8 04:55:15 CEST 2011
  new method ReadSourceCatalog()
  starting on method SourceTrack()

$modified: Mon Oct 10 13:27:02 CEST 2011
  continued development on SourceTrack()
  for digital calibration time:
    Christophe T. told me last week that 1 second per beamlet is enough, plus 10 seconds on top of it all

$modified: Wed Oct 26 13:20:32 CEST 2011
   added warning in pointing text file in case satellite is not visible
   modified SourceTrack() so it can track a satellite

$modified: Tue Nov 15 12:17:19 CET 2011
   SourceTrack() comment to FITS file and log file for AZEL begin and end times
   SatPointing() return False if satellite below horizon 

$modified: Wed Nov 16 17:41:32 CET 2011
  in satcal()  replaced 'rf' with 'RF' in the RF beam pointing text file name
  found bug:  left over from doing calibrations around transit time
              subtracting time to start before transit
              but this causes a problem when we want to do it "now"
              moving this into the "if" statement

$modified: Thu Nov 17 11:09:09 CET 2011
  class variable myObs, instead of passing MyObservation as an argument
  methods: setMyObservation(), isSet_MyObservation()
  new method: printFrequencyRange()

  in satcal(): pointing files read within the method, instead of doing it in the main script

  new method: readPointingFiles()

$modified: Fri Nov 18 11:29:32 CET 2011
  as of today, all times are in UT
  no more UT2local conversions
  for now, simply set ut2local=0

  new method: addFITScomment()

  timeformats in class variables
  isoTimeFmt
  FilenameTimeFmt
  PointingTimeFmt

  add submit log name as a comment in the FITS file

$modified: Mon Nov 21 09:57:55 CET 2011
  satcal() returns the start and end times, as was done for SourceTrack()
  previously, satcal() returned True or False.  Obs scripts must also be
  modified.

  satDrift() also returns start and end times

  removing UT2local stuff

$modified: Tue Nov 22 08:30:22 CET 2011
  satcal() removing superfluous "type_cal=current"

  record the arguments given on the command line

  readPointingFiles()  option to not read digital pointing
  this is implemented to try and track down the problem of oscillating calibration results
  Christophe T. says maybe it's the digital cal interfering with the RF cal (not waiting long enough?)

  satcal() new keyword digcal, digcal=True by default
  digital pointing files are always generated, but they are not read if digcal=False

$modified: Wed Nov 23 09:53:40 CET 2011
  SourceTrack() fixed bug:  when source is no longer visible, check if it's an astro source or not
  so we know to use next_pass instead of next_rising

$modified: Sat Nov 26 22:21:54 CET 2011
  stepsize changed from 5 seconds to 20 seconds

$modified: Tue Nov 29 11:42:03 CET 2011
  stepsize back to 5 seconds
  Christophe T. will capture dig cal parameters to see what's going on

$modified: Thu Dec  1 12:30:56 CET 2011
  in satcal() can specify obsdate up to seconds in a command line argument

$modified: Tue Dec  6 15:55:18 CET 2011
  having trouble with satcal() with drift, it hasn't worked since 17 Nov.
  adding another tstep before the drift scan

  SatDrift() : making total drift time a variable, instead of hardwired to 40 minutes
  duration is a keyword with default 10 minutes
  adding comment to FITS file for drift scan start

$modified: Wed Dec  7 12:05:36 CET 2011
  SatDrift() : I had removed "type_cal=current" from satcal(), and so SatDrift was applying
  csx calibration, which was the existing state of the type_cal in the pointing files.
  Now SatDrift() begins with "type_cal=current"

  SatDrift() : added comment to FITS file for y-peak time (I had forgotten that one)

$modified: Wed Dec  7 17:00:48 CET 2011
  SourceTrack() : added log comment for source location at requested time

$modified: Wed Dec  7 22:40:25 CET 2011
  new method: str2dt() convert a string to a datetime structure
  for use in satcal() and SourceTrack()

$modified: Thu Dec  8 13:46:03 CET 2011
  str2dt() now used in satcal()

$modified: Mon Mar 26 14:40:35 CEST 2012
  allowing for Beam-B
    printFrequencyRange()
    readPointingFiles()
    satcal()
    SourceTrack()  
    Acquire()

$modified: Thu Mar 29 14:20:53 CEST 2012
  parseargs() updated, but still not implemented, see notes below

  completing the implementation of keyword duration for satcal()

  adding some error checking for nBeamlets() and Bandwidth()

$modified: Fri Apr 13 13:56:30 CEST 2012
  default return for str2dt is None

  SourceTrack can handle the Sun

  satcal adapted to do calibration on the Sun.  Now we're left with a legacy name.
  new method suncal() which calls satcal after assigning "satname"  

  added tot_seconds() method to convert timedelta to total number of seconds

$modified: Mon Apr 23 14:24:36 CEST 2012
  FindSat() accepts "Sun" as a calibrator "satellite"

$modified: Wed Apr 25 12:22:03 CEST 2012
  new method next_transit() used in SourceTrack() and satcal()
  takes care of satellite/astro source and whether or not it is circumpolar

$modified: Mon May  7 17:09:36 CEST 2012
  Afristar is accepted as a calibrator in FindSat()
  This is *not* fully implemented yet!

$modified: Mon May 14 13:37:37 CEST 2012
  trying to implement the Tobia experiment:  tracking source with RF but not with digital beam
  methods:  stationBeamSize(), tilesetBeamSize(), tileBeamSize()

$modified: Mon May 28 17:25:14 CEST 2012
  fixed bug with printing beam sizes
  updated array width using maximum along the diagonal E-W.
  see also, tileset_positions.py

$modified: Tue May 29 14:54:58 CEST 2012
  fixed bug in next transit.  I wasn't printing the transit time.

$modified: Mon Jun 25 12:11:09 CEST 2012
  fixed bug in SourceTrack():  it was always in Tobia mode (no dig tracking)
  because isinstance(digtrack,int) returns "True" for digtrack boolean!

$modified: Thu Jul  5 14:33:42 CEST 2012
 SourceTrack() : OffBeam can be "ahead" or "behind"
 define offbeam either 3deg ahead or 3deg behind in the same sky track as the source

$modified: Sun Jul  8 16:00:58 CEST 2012
  some weird thing in pyEphem for next_rising: 
  have to specify start time even though date was set in the observer (ie. embraceNancay)

$modified: Mon Jul 16 11:02:21 CEST 2012
  bug:  math.acos craps out when the argument is 1.0 (or probably slightly higher)
  fixing by testing with try and except

$modified: Tue Jul 17 11:06:11 CEST 2012
  cleaning up SatPointing to use eph.name

  new method: Pointing() based on SatPointing() but doesn't write to pointing file
  maybe use this for a more general pointing method...

$modified: Wed 18 Jul 2012 16:10:13 CEST
  new method: navstar63()
  this one is not on the celestrak GPS operational list because it's not operational
  but it's transmitting at 1176.5!

  correction: GPS BIIF-1 and F-2 frequency is 1176.45, *not* 1175.6 !!!!!

$modified: Thu 19 Jul 2012 10:34:41 CEST
  bug introduced with addition of navstar63() yesterday
  fixed in FindSat():  if... elif instead of if... if

$modified: Wed 25 Jul 2012 11:20:27 CEST
  new method makeDigitalBeam()
  replacing what is in the main scripts to ensure uniformity

  adding class variable self.ndigbeams

  bug: Vivaldi width corrected from 0.124 to 0.125

  new method: nDigitalBeams()

$modified: Thu 26 Jul 2012 17:27:50 CEST
  stepsize for repointing:  
   allowing different stepsize for calibration and for tracking
  new class variable self.calibration_stepsize

$modified: Fri 27 Jul 2012 15:04:44 CEST
  nicknames for satellites and sources: f1, f2 etc

$modified: Fri 27 Jul 2012 18:03:37 CEST
  SourceTrack() : if offsource not found, return instead of continuing with offsource=source

  new method: verifySource() : extracted from SourceTrack() 
  trying to clean up so I can implement differential pointing update times for RF and digital tracking

$modified: Sat 28 Jul 2012 16:35:40 CEST
  angularSeparation() : new method extracted from code in SourceTrack()

  float2str() : new method, to standardize all the float outputs

$modified: Sun 29 Jul 2012 22:48:15 CEST
  cleaning up SourceTrack()
  new class: openPointingFiles() moves stuff out of SourceTrack() to make it easier to read

  finally got rid of the last LocalTime stuff... I think (see mod Nov 18 11:29:32 CET 2011)

  using isoformat(' ') for all dates

$modified: Mon 30 Jul 2012 11:16:00 CEST
  improving method Pointing() for "off" "ahead" "behind"

  SourceTrack() : looping until endobs, rather than calculating npointings


  but I want to work on differential RF and digital beampointing update time  
  
$modified: Wed 01 Aug 2012 16:18:00 CEST
  new method: makeRFBeam()

$modified: Wed 21 Nov 2012 13:51:18 CET
  ReadSourceCatalog(): 
   try to find radio.edb catalog in case it's not in the users home dir

$modified: Tue 27 Nov 2012 17:31:42 CET
  new method:  source_fullname()  translate satellite and source nicknames

$modified: Fri 30 Nov 2012 11:02:27 CET
  source_fullname() : more sophisticated nicknames, using regex

$modified: Tue 08 Jan 2013 12:43:12 CET
  new RF calibration algorithm takes more time.  rfcaltime now set to 120 seconds

  SatDrift() : can optionally specify the time for the peak of the drift scan
               default is half way through duration
  satcal() : modified for new SatDrift()

$modified: Fri 11 Jan 2013 12:16:50 CET
  satcal() : return value is now a dictionary, with start time, end time, and drift peak time
             must also modify the scripts that use satcal() (ie. all of them)

$modified: Tue 22 Jan 2013 09:16:06 CET
  satcal() : satcalParms now has satcalEnd (ie, end of cal and before drift scan begins)

$modified: Thu 07 Feb 2013 14:20:37 CET
  creating methods that help clean up the observation main scripts

  initialize() : initialize MyObservation
  submit() : submit the observation, recording the times

$modified: Thu 14 Feb 2013 19:23:22 CET
  trying to amalgamate stuff so there are not multiple execfile()
 
$modified: Fri 15 Feb 2013 09:51:57 CET
  function FullPathname()
  using FullPathname() in ReadSourceCatalog()

$modified: Fri 15 Feb 2013 14:11:16 CET
  in satcal() : NeverUpError for a source which does not rise above the horizon
                (this error is different from NeverUpError for a source which never transits)

  FindSat() : default frequency for the Sun as calibrator is 1420MHz

$modified: Sat 16 Feb 2013 07:34:24 CET
  satcal() : uncommented if not SatPointingStatus: EndTime=None
             otherwise I get an infinite loop in sat-Track-and-Cal.py
             why had I commented it out?
 
             changed my mind... using a new entry in satcalParms instead
             satcalParms['PointingStatus']
             so EndTime can still be used (not None)

$modified: Mon 18 Feb 2013 13:59:10 CET
  source_fullname() : fixed problem with nicks for GPS
                      must search first for RM
                      something about how regex works in python

$modified: Mon 18 Feb 2013 16:39:04 CET
  Scriptname() : check for modification time of the main script
  this is used to avoid problems with incompatibility of old scripts 
  to the current version of satorchi_OBSinit.py
  the minimum acceptable date is given in variable: dtmodern

$modified: Tue 19 Feb 2013 14:55:16 CET
  makeRFBeam() : time of beam creation is logged

$modified: Thu 21 Feb 2013 13:27:18 CET
  makeDigitalBeam() : time of beam creation is logged

$modified: Fri 08 Mar 2013 11:12:15 CET
  printFrequencyRange() : added units to printout MHz

$modified: Thu 14 Mar 2013 14:09:24 CET
  writeTLE2FITS() : write TLE to FITS comments for later use
                    called from FindSat()

$modified: Fri 19 Apr 2013 14:51:38 CEST
  source_fullname() : nicknames for the Sun

$modified: Wed 12 Jun 2013 14:38:10 CEST
  default number of beamlets changed from 62 to 61
  we should flash the RSPs with the Kant/Benthem fix

$modified: Thu 04 Jul 2013 10:50:05 CEST
  modifications to allow for multiple digital beams
  readPointingFiles() : new keywork argument:  digbeam_index

$modified: Thu 04 Jul 2013 15:14:29 CEST
  added self.onesid, and self.onesol: useful time deltas

$modified: Mon 08 Jul 2013 14:02:59 CEST
  self.caldriftduration: default duration of drift scan during calibration 
  see also satorchi_OBSinit.py

$modified: Mon 22 Jul 2013 13:27:23 CEST
  nicknames for B0329 and B1133

$modified: Tue 23 Jul 2013 14:55:49 CEST
  next_transit() : trying to find a work-around for the bug in PyEphem regarding next_transit
   it does not always return the next transit, but instead a previous one
   see also:  https://bugs.launchpad.net/pyephem/+bug/572402

  created individual methods for next_transit_astro() next_rise_astro() next_setting_astro()

$modified: Tue 23 Jul 2013 16:37:40 CEST
  makeDigitalBeam() : new keyword nbeamlets (number of beamlets per digital beam)

$modified: Mon 05 Aug 2013 14:02:02 CEST
  bug fix:  next_transit_astro(), etc, dtnow was undefined
  bug fix:  next_rise_astro(), for sources always up, give requested date +1 minute for rise time

$modified: Tue 06 Aug 2013 18:48:21 CEST
  modifications to enable using stored calibration parameters
  note that RF calibration parameters are passed by the SCU to the LCU through the python script
  but the digital calibration parameters are read from file directly by the BeamServer
  see for example on LCU:  /opt/embrace/etc/BeamerServer-I0.conf
  FindSat() : check for calibration file, or use default 'stored'

$modified: Wed 07 Aug 2013 11:09:58 CEST
  readCalibration() : read RF calibration parameters from file, or use default values
  assignCalibration() : apply calibration table to the RF Beam

$modified: Tue 13 Aug 2013 15:49:12 CEST
  source_Fullname() : fixed bug with matching nicknames

$modified: Mon 19 Aug 2013 12:47:35 CEST
  methods to create xml files for transfering to Andante for pulsar observing

  printFrequencyRange() : now takes digbeam_index as optional argument
                          returns a dictionary with freq range

$modified: Fri 23 Aug 2013 10:11:42 CEST
  removed deprecated suncal() : use satcal() with satname=Sun

$modified: Mon 26 Aug 2013 12:29:01 CEST
  tot_seconds() and str2td() moved to datefunctions.py

$modified: Tue 27 Aug 2013 16:28:45 CEST
  satcal() : digital pointing is also updated during calibration phase

$modified: Wed 28 Aug 2013 15:56:07 CEST
  SourceTrack() : call to openPointingFiles uses srcEph.name rather than source, which could be a nickname

$modified: Wed 04 Sep 2013 12:24:50 CEST
  Pointing() : possible bug fixed in calling angularSeparation()

$modified: Thu 05 Sep 2013 23:21:37 CEST
  self.source renamed to self.sourcecat, and initialized in __init__ rather than set to None

$modified: Fri 06 Sep 2013 10:08:12 CEST
  ReadSourceCatalog() : no longer reports catalog name to log file, because the log file hasn't yet been defined
                        using a simple print instead

  Acquire() : a bit more info printed in case of an error

$modified: Sat 07 Sep 2013 18:12:56 CEST
  bug fix:  lots of places where "obsfunc" appears instead of "self"
            this comes from moving stuff from the main script to ObsFunctions
            apparently, error messages that have never appeared so far
            python only signals an error when the code is invoked at run time

  ObsTitle() : assign an observation title for a given beam

$modified: Fri 15 Nov 2013 15:13:36 CET
  changes to allow for independence of A and B
  the idea is to have an obsfunc object for each RF beam
  
  Scriptname() : submittime can be given as input so that Beam-A and Beam-B have the same output log

$modified: Sun 17 Nov 2013 08:19:47 CET
  continuing towards independence of Beam-A and Beam-B:
  caldate, obsdate for each beam

$modified: Mon 18 Nov 2013 09:56:27 CET
  continuing...
  duration is now a parameter of the ObsFunctions class

$modified: Tue 19 Nov 2013 10:29:38 CET
  mkPulsarXML() : argument is now "duration" instead of "startTime" and "endTime"  
  SourceTrack() : using self.duration, etc, instead of arguments

$modified: Thu 06 Feb 2014 22:53:17 CET
  added web site for satellites: www.tle.info
  note that the names are not the same:  eg. GPS BIIF-2 is NAVSTAR 66

$modified: Wed 19 Feb 2014 12:33:24 CET
  PulsarXML() : added duration comment to pulsar XML
  mkPulsarXML() : added source name to xmlfilename

$modified: Wed 19 Feb 2014 17:14:41 CET
  mkRunArtemis() : generate the shell script for running ARTEMIS

$modified: Tue 18 Mar 2014 10:22:26 CET
  submitAtBackend() : submit jobs to atq on the backend (andante, borsen,...)

$modified: Tue 18 Mar 2014 16:32:31 CET
  driftscan() : setup a driftscan (adapted from driftscan.py, last mod: Mon Dec 23 08:45:44 CET 2013)

$modified: Wed 26 Mar 2014 12:20:43 CET
  __init__() : error in initialization A -> 'A' (thanks Benj!)
    but this never revealed itself because ObsFunctions is called by satorchi_OBSinit.py which always specifies 'A' or 'B'

$modified: Sun 30 Mar 2014 17:53:19 CEST
  digcalDuration() : time required to complete a digital calibration.  this was previously recalculated in two methods
  digcal() : do a digital calibration on an astro source

$modified: Sat 05 Apr 2014 19:16:41 CEST
  digcal() : follow source using AZEL instead of J2000.  and finish with SetCal('current')

$modified: Thu 10 Apr 2014 19:57:04 CEST
  RFcal() : RF calibration.  no frills so far.
  RFcalDuration() : time to complete an RF calibration (to have symmetry with digcalDuration() )

$modified: Tue 15 Apr 2014 11:37:23 CEST
  verifySource() : now determines start time given parameters, as was done with digcal()
                   code in digcal() moved to verifySource()

$modified: Tue 15 Apr 2014 16:49:36 CEST
  currentLocation() : moved reused code into it's own def
  using traceback to retrieve exceptions from next_pass()

$modified: Wed 16 Apr 2014 11:43:47 CEST
  driftscan() : fixing OFFSOURCE bug, moving determination of offEph to verifySource()
  default offsource is "ahead" rather than "None"

$modified: Wed 16 Apr 2014 17:55:24 CEST
  driftscan() : bug due to pyephem which returns a "next transit" which is earlier than start time

$modified: Wed 16 Apr 2014 20:10:24 CEST
  RFcal(), digcal() : set centre frequency and cal frequency

$modified: Thu 17 Apr 2014 16:24:48 CEST
  digcal() : bug, cut-and-paste error.  I was doing cal='ssx' for the RFBeam!
             bug:  after first pointing, don't ask for more calibrations!
  RFcal() :  bug:  after first pointing, go back to 'current' calibration

$modified: Thu 17 Apr 2014 20:25:53 CEST
  driftscan() : corrected bug for offset pointing for Y-direction

$modified: Thu 17 Apr 2014 23:55:08 CEST
  next_rising_astro() : weird bug with inconsistent datetime (imprecision).  added margin.

$modified: Tue 29 Apr 2014 20:17:29 CEST
  assignCalibration() readCalibration() : debugging for reading calibration parameters

$modified: Tue 06 May 2014 15:44:52 CEST
  verifySource() : bug fix: check for visibility
  assignCalibration() : use makeRFBeam() instead of getRFBeam() so that we assign frequencies
  makeRFBeam() : bug fix: set calibration frequency

$modified: Wed 07 May 2014 11:12:56 CEST
  assignCalibration() : need set cal type "scu"

$modified: Tue 13 May 2014 17:23:14 CEST
  assignCalibration() : need to set cal type "none" for reading digital calibration from file on LCU

$modified: Tue 20 May 2014 08:08:34 CEST
  driftscan() : removed SetCal("current").  is this interfering with calibration from stored parameters?

$modified: Fri 20 Jun 2014 11:19:10 CEST
  mkPulsarXML() : added Crab pulsar

$modified: Thu 10 Jul 2014 16:56:57 CEST
  mkRunARTEMIS() : name for file using given source

$modified: Thu 31 Jul 2014 12:00:51 CEST
  FitsFilename() : remove parentheses from name

$modified: Wed 13 Aug 2014 10:34:06 CEST
  acquisition directory changed on Andante from /Quartet to /Data/incoming
  Quartet has gone wonky along with all our pulsar data!

$modified: Thu 14 Aug 2014 11:12:38 CEST
  startstopLCUprocs() : remotely start/stop LCU procs

$modified: Wed 20 Aug 2014 11:01:04 CEST
  assignCalibration() : add wait time when using saved calibration parameters

$modified: Wed 20 Aug 2014 16:49:54 CEST
  SourceTrack() : clean up.  removing "type_cal" everywhere except in satcal()
  but PRenaud's AddSubscanFromFile() requires a type_cal statement!!!!
  note: satcal() ends with type_cal=current

$modified: Thu 21 Aug 2014 14:22:32 CEST
  satcal() : improving start time selection.  must have a minumum setup time.

$modified: Fri 22 Aug 2014 12:16:59 CEST
  startstopStatisticsCapturer() : login as scuadmin
  whichNetwork() : determine if we are on the Nancay network

$modified: Wed 27 Aug 2014 15:10:17 CEST
  verifySource() : determines RF pointing for ON/OFF symmetric around centre of FoV

$modified: Thu 30 Oct 2014 12:14:02 CET
  satcal() : bug fix: comparing timedelta to total number of seconds

$modified: Mon 19 Jan 2015 09:59:18 CET
  makeDigitalBeam() : bug fix: ndigbeam_index for multiple digital beams  

$modified: Wed 11 Feb 2015 13:32:05 CET
  satcal() : bug fix: define driftdate even if no drift has been selected

$modified: Mon 23 Feb 2015 12:38:07 CET
  startstopCalparmsCapturer() : start/stop Calibration Parameters acquisition

$modified: Fri 13 Mar 2015 08:54:51 CET
  log() : print '*' instead of 'None' for beam id

$modified: Fri 13 Mar 2015 12:32:21 CET
  Pointing() : bug fix:  angularSeparation takes argument in radians, not degrees!
  driftscan() : added pointing info to returned parameters

$modified: Wed 08 Jul 2015 17:13:47 CEST
  powerupTiles() : power up tiles and ping for a while until they all answer
  startupTiles() : send the startup command to the tiles

$modified: Sun 12 Jul 2015 21:14:43 CEST
  lcucommand() : send a command to the LCU
  scucommand() : send a command to the SCU

$modified: Tue 21 Jul 2015 11:25:01 CEST
  submit_process() : for sending a system command and getting the output

$modified: Wed 22 Jul 2015 12:43:54 CEST
  source_fullname(), mkPulsarXML(), PulsarXML() : pulsar B0950+08

$modified: Mon 27 Jul 2015 15:59:04 CEST
  various LCU/SCU actions are now using lcucommand() scucommand()
  powercycleTiles() renamed from powerupTiles()  (power cycling is no longer necessary since cable change on 0:e3)
  powerupTiles(), poweroffTiles()

$modified: Tue 25 Aug 2015 18:06:45 CEST
  StatisticsCapturer uses normal userid rather than scuadmin

$modified: Thu 27 Aug 2015 07:36:04 CEST
  verifySource() : define drift_yoffset in time minutes = difference between src and off pointing

$modified: Mon 31 Aug 2015 16:31:50 CEST
  startLCUprocs() : use wrapper script "restartLCUprocs.sh" which clears any running procs.

$modified: Wed 27 Jul 2016 10:16:16 CEST
  poweroffTiles(), powerupTiles() :  also stops/starts power to RSP and CDC racks
  tleurls : tle.info is not responding, going back to celestrack

$modified: Mon 29 Aug 2016 09:16:15 CEST
  netnames(), scucommand() : fixing the bug so it works on the SCU, as well as on remote stations

$modified: Mon 26 Sep 2016 14:49:17 CEST
  stopLCUprocs() : kill all LCU services, including ServiceBroker, MAT, and ECB.  Change from killMostLCUprocs.sh

$modified: Sun 08 Jan 2017 17:26:48 CET
  log() : option for master level  
  submit_EMBRACE_observation() : new def for last few commands that appear in all scripts
  mkBackendCommands() : generate the backend commands for pulsar observing

"""
import sys,os,socket,time,subprocess,traceback
import datetime as dt
import ephem as eph
import urllib as url
import math
import re
from datefunctions import *
from copy import *

def whichNetwork():
    """
    determine our current internet network
    """
    netnames={} # dictionary for return values
    cmd="/sbin/ifconfig"
    chk=subprocess.Popen(cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=False)
    (out,err)=chk.communicate()

    findnet=out.find("193.55.144.92")
    if findnet>=0:
        print "we're on the SCU."
        netnames['scu']='localhost'
        netnames['scuadmin']='scuadmin'
        netnames['lcu']='lcu'
        return netnames

    
    findnet=out.find("193.55.144.")
    if findnet<0:
        print "not on the Nancay network"
        netnames['scu']='scu'
        netnames['scuadmin']='scuadmin'
        netnames['lcu']='lcu'
        return netnames

    print "we're somewhere within Nancay observatory"
    netnames['scu']='scudirect'
    netnames['scuadmin']='scuadmindirect'
    netnames['lcu']='rspctl'
    return netnames

def submit_process(submit_cmd):
    print "submitting command:\n   "+submit_cmd
    submitproc=subprocess.Popen(submit_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE,shell=True)
    (out,err)=submitproc.communicate()
    ret_str=[]
    if out!="":
        ret_str.append(out)
    if err!="":
        for line in err.split('\n'):
            if line.find('warning')<0 and line!="":ret_str.append(line)
    return ret_str

def lcucommand(at=None,action=None):
    """
    send a command to the LCU
    """
    if action==None:
        print "must supply an action for the LCU command"
        return False
    
    netnames=whichNetwork()
    lcu=netnames['lcu']
    if at==None:
        cmd=str("echo embrace|sudo -S %s" % action)
    else:
        CmdTime=str2dt(at)
        if CmdTime==None:return False
        CmdTime_str=CmdTime.strftime('%H:%M %Y-%m-%d')
        cmd=str("echo -e 'embrace\n%s'|sudo -S TZ=UT at %s" % (action,CmdTime_str))

    submit_cmd=str('ssh %s "%s"' % (lcu,cmd))
    return submit_process(submit_cmd)

def scucommand(at=None,action=None,user=None):
    """
    send a command to the SCU
    """
    if action==None:
        print "must supply an action for the SCU command"
        return False
    
    netnames=whichNetwork()
    if user=='scuadmin':
        scu=netnames['scuadmin']
    else:
        scu=netnames['scu']
    if at==None:
        cmd=action
    else:
        CmdTime=str2dt(at)
        if CmdTime==None:return False
        CmdTime_str=CmdTime.strftime('%H:%M %Y-%m-%d')
        cmd=str("echo '%s'|TZ=UT at %s" % (action,CmdTime_str))

    if scu=="localhost":
        submit_cmd=cmd
    else:
        submit_cmd=str('ssh %s "%s"' % (scu,cmd))
    return submit_process(submit_cmd)

def sleepmode(at=None):
    """
    put the tiles in sleep mode (48V still connected, but tiles not "started up")
    except for troublesome tiles which are in startup mode and pinged
    """
    return lcucommand(at,action='/home/embrace/semisleepmode.sh')

def shutdownmode(at=None):
    """
    put the tiles in sleep mode (48V still connected, but tiles not "started up")
    """
    return lcucommand(at,action='/home/embrace/sleepmode.sh')

def wakeup(at=None):
    """
    wake up tiles after sleep mode
    """
    return lcucommand(at,action='/home/embrace/preobservation_startup.sh')

def powercycleTiles(at=None):
    """
    power up the tiles and switching off/on to jiggle bad tiles to finally respond
    """
    netnames=whichNetwork()
    lcu=netnames['lcu']
    scu=netnames['scu']

    # SCU: supply 48V to tiles (Off and On for ~15 minutes)
    # LCU: ping the troublesome tiles for ~15 minutes
    ret=scucommand(at,action='/home/torchinsky/scripts/loopOnOff.sh')
    if not ret:return ret
    return lcucommand(at,action='/home/embrace/checkproblemtiles.sh')

def poweroffTiles(at=None):
    """
    power off the tiles.  This should be done after tiles put to sleep.
    this also shuts down group-2 of the UPS Eaton Evolution S 3000 (the RSP and CDC racks)
    """
    #action_script="echo y|python /home/censier/power_OFF.py"
    action_script="/home/torchinsky/scripts/power_down.sh"
    return scucommand(at,action=action_script)

def powerupTiles(at=None):
    """
    apply the 48V to the tiles.  This does not include the start up commands.
    this also starts up group-2 of the UPS Eaton Evolution S 3000 (the RSP and CDC racks)
    """
    #action_script="python /home/censier/power_ON.py"
    action_script="/home/torchinsky/scripts/power_up.sh"
    return scucommand(at,action=action_script)

def startupTiles(at=None):
    """
    send the startup command to all the tiles
    """
    action='/home/embrace/startuptiles.sh'
    return lcucommand(at,action=action)
    
def startLCUprocs(at=None):
    """
    start LCU processes either now (at=None) or a future time (at=datetime)
    """
    return lcucommand(at,action="/home/embrace/restartLCUprocs.sh")

def stopLCUprocs(at=None):
    """
    stop LCU processes either now (at=None) or a future time (at=datetime)
    """
    #return lcucommand(at,action="/home/embrace/killMostLCUprocs.sh")
    return lcucommand(at,action="/home/embrace/killLCUprocs.sh -A")

def startStatisticsCapturer(at=None):
    """
    start Statistics Capturer on SCU
    """
    return scucommand(at,action="/home/torchinsky/scripts/startStatisticsCapturer.sh")
    
def stopStatisticsCapturer(at=None):
    """
    stop Statistics Capturer on SCU
    """
    return scucommand(at,action="/home/torchinsky/scripts/killDataCapturer.sh")

def startCalparmsCapturer(at=None):
    """
    start Calibration parameters Capturer on SCU
    """
    action_script={}
    action_script['A']="/home/torchinsky/scripts/startCalparmsCapturerA.sh"
    action_script['B']="/home/torchinsky/scripts/startCalparmsCapturerB.sh"

    ret_str=[]
    beams=['A','B']
    for beam in beams:
        ret_str+=scucommand(at,action=action_script[beam])
    return ret_str

def stopCalparmsCapturer(at=None):
    """
    stop Calibration parameters Capturer on SCU
    """
    return scucommand(at,action="/home/torchinsky/scripts/killCalParmsCapturer.sh")

def FullPathname(filename):
    homedirs=['.',
              os.environ['HOME'],
              '/home/steve',
              '/home/satorchi',
              '/home/torchinsky']
    xephemdirs=[d+'/.xephem' for d in homedirs]
    for d in homedirs+xephemdirs+sys.path:
        fullpathname=d+'/'+filename
	if os.path.exists(fullpathname):
            print 'found file: '+fullpathname
            return fullpathname
    print "Error:  could not find file: "+filename
    return None

def submit_EMBRACE_observation(obsfunc):
    beams=obsfunc.keys()
    beam=beams[0]
    if obsfunc[beam].submit():
        for beam in beams:
	        obsfunc[beam].submitAtBackend()
    return


################# constants used for RF Beam-A and Beam-B #############
### default calibration tables
calTable={}
default_RFcalfreq={}

# these are values measured on 2013-08-06
calTable['A']= [0, 17,  2,  8,
                0, 23, 13, 13,
                0, 11, 11,  7,
                0,  7,  8,  5, 
                0, 17,  6, 19,
                0, 11,  8, 17,
                0, 19,  9,  6,
                0, 19, 18, 20,
                0, 19, 17, 13,
                0,  5,  8,  1,
                0, 17, 13,  2,
                0,  9,  8, 13,
                0,  7, 22, 13,
                0, 11, 15, 13,
                0, 23, 20,  1,
                0,  2,  9,  8]
        
default_RFcalfreq['A']=1420.40575177


# these are values measured on 2013-08-07
calTable['B']=[0,  2, 22, 22,
               0, 16, 14,  2,
               0, 16,  6, 14,
               0, 23,  9, 19,
               0, 15, 14, 19,
               0,  5, 22, 10,
               0, 21, 17, 17,
               0,  2, 16,  2,
               0,  8,  3,  3,
               0, 13, 22, 23,
               0,  3,  6,  7,
               0, 18, 22, 20,
               0, 21,  9,  5,
               0,  1,  2,  3,
               0,  3,  5, 10,
               0,  9, 14, 10]
default_RFcalfreq['B']=1420.40575177

###### end constants definition ################################################


class ObsFunctions:
    """
$Id: ObsFunctions.py 
     python script defining some functions for EMBRACE observartions
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Sat Jul 30 17:31:19 CEST 2011

### HOWTO use this python script:

# In your observation script somewhere near the top add the following lines:
execfile('/home/torchinsky/scripts/ObsFunctions.py')
#
# alternatively, if you have /home/torchinsky/scripts listed in your PYTHONPATH 
# then you can import instead:
from ObsFunctions import ObsFunctions
#
# create an object with the class:
obsfunc=ObsFunctions()
# 
#
# 
# here's an example for bandwidth of 10MHz
bw=obsfunc.Bandwidth(10)
print 'bandwidth set to ',bw,' MHz' # the answer should be 9.9609375
print 'number of beamlets: ',obsfunc.nBeamlets # the answer should be 51
#
#
# It is also possible to initialize the ObsFunctions class with desired values
# for example:
#
obsfunc=ObsFunctions(bandwidth=30)
#
# or
#
obsfunc=ObsFunctions(nbeamlets=40)
#
# if you specify both nbeamlets and bandwidth, then bandwidth will take precedence
#
# afterwards you should use the values in your observation script
# for example:
#
RFcentre=1420.0 # frequencies given in MHz
if not obsfunc.RFokay(RFcentre):
  print 'quitting now.  Re-edit your script and try again'
  quit()
bw=obsfunc.Bandwidth(30.0)
nbeamlets=obsfunc.nBeamlets()

StartFreq=RFcentre-bw/2.0
DigitalBeam1=SCAN_A.scan.VI.AddDigitalBeam(StartFreq*1e6,bw*1e6) # frequencies given in Hz
    """



    # constructor
    def __init__(self,beam=None,bandwidth=None,nbeamlets=61):

        # initialize the object for RF beam, A or B
        if beam==None:self.beam='A'
        else: self.beam=beam

        self.ndigbeams=0

        # EMBRACE@Nancay array dimensions
        vivaldi_width=0.125

        tile_width=vivaldi_width*6/math.sqrt(2)
        tileset_width=4*tile_width
        array_width=4*tileset_width

        vivaldi_maxno_NS=23+1+23+1+23+1+23 # see Chenanceau notebook 2012-05-22
        vivaldi_maxseparation_NS=vivaldi_maxno_NS-1

        vivaldi_maxno_EW=4*24
        vivaldi_maxseparation_EW=vivaldi_maxno_EW-1

        #### this is along the array length/width
        ##self.arrayWidth=8*12*0.124/math.sqrt(2) # EMBRACE width of the 8x8 array
        
        #### this is the maximum width, ie. the smallest dimension for beam size
        self.arrayWidth=vivaldi_maxseparation_EW*vivaldi_width

        self.chanwidth=100.0/512 # this is the frequency width of a beamlet
        self.nbeamlets=nbeamlets # default value
        if bandwidth==None:
            self.bandwidth=self.nbeamlets*self.chanwidth
        else:
            trueBW=self.Bandwidth(bandwidth)
        self.filerootname="EMBRACE-data"
        self.filenametime=dt.datetime.today()
        self.RFcentre=None
        self.RFcalfreq=None
        self.DataDir();
        self.scriptname='unspecified-EMBRACE-observation-script'
        self.submitlogname='unspecified-EMBRACE-observation-script.submitlog'

        # useful constants
        self.rad2deg=180.0/math.pi
        self.speedolight=299.792458 # m * MHz
        # useful time delta, one solar, and one sidereal day
        self.onesid=dt.timedelta(hours=23)+dt.timedelta(minutes=56)
        self.onesol=dt.timedelta(hours=24)


        # figure out the time difference to UT
        delta_ut2local=dt.datetime.now()-dt.datetime.utcnow()
        ut2local_hours=round((delta_ut2local.seconds+delta_ut2local.microseconds/1e6)/3600)
        self.ut2local=dt.timedelta(hours=ut2local_hours)

        # Fri Nov 18 11:29:32 CET 2011
        # everything is in UT from now on
        self.ut2local=dt.timedelta(hours=0)


        # for satellite tracking etc
        # our location EMBRACE@Nancay
        self.embraceNancay = eph.Observer()
        self.embraceNancay.long = '2:11:57.48'    # angle 'deg:min:sec.xx' or x.y radians
        self.embraceNancay.lat = '47:22:55.2'    # angle 'deg:min:sec.xx' or x.y radians
        self.embraceNancay.horizon='45:00:00.0' # minimum elevation
        self.embraceNancay.elevation = 100
        self.embraceNancay.pressure=0 # to ignore atmospheric corrections (optical only)

        self.satname=None
        self.sateph=None

        # source catalog
        self.ReadSourceCatalog()

        # source for pointing
        self.source=None
        self.srcParms=None

        # for off source pointing
        self.offsource='ahead'

        # the amount of time to wait for RF beam calibration in seconds
        # Tue 08 Jan 2013 12:41:29 CET
        # new RF calibration algorithm takes more time
        self.rfcaltime=120.0

        # cal type for digital beams.  This is set to "none" in assignCalibration()
        self.digbeam_caltype="current"

        # format for printing angles in the pointing text files
        self.floatPrintFmt="%.3f"

        # variables possibly requested by the main script arguments
        #
        # datetime object for date in UT for the planned observation
        self.obsdate=None
        # datetime object for date in UT for the calibration
        self.caldate=None
        #
        # force the observation to occur when requested, rather than at next transit
        self.doit_now=False
        #
        # drift scan offset of digital beam Y-direction, given in minutes
        self.drift_yoffset=4

        # minimum time needed to set things up
        self.minimum_setupdelay=dt.timedelta(minutes=2)

        # timestep in seconds for recalculating the pointing when tracking
        # Sat Nov 26 22:21:54 CET 2011
        # stepsize changed to 20 seconds, maybe this is better for CasA tracking
        # Tue Nov 29 11:42:03 CET 2011
        # stepsize back to 5 seconds
        self.stepsize=5.0

        # stepsize for RF pointing, may be different
        self.RFstepsize=5.0

        # timestep in seconds for recalculating pointing during calibration
        self.calibration_stepsize=5.0

        # default duration of drift scan during calibration phase
        self.caldriftduration=dt.timedelta(minutes=10)

        # duration of the observation (default 30 minutes)
        self.duration=dt.timedelta(minutes=30)
        
        # MyObservation object from the SCU modules
        self.myObs=None

        # output time formats
        self.isoTimeFmt="%Y-%m-%dT%H:%M:%S%z"
        self.FilenameTimeFmt="%Y%m%d-%H%M"
        self.PointingTimeFmt="%Y/%m/%d %H:%M:%S"

        # backend commands for pulsar observing
        self.backend_cmd=None
        
        return

    # destructor
    def __del__(self):
        return

    ##### some useful utilities ####

    def float2str(self,x):
        if x==None:return "None"
        return str(self.floatPrintFmt % x)

    ################################

    # default directory for saving stats files
    def DataDir(self,datadir=None):
        if isinstance(datadir,str):
            if os.path.isfile(datadir):
                print "Selection of Data Directory: this is an existing file: ",datadir
                print "Please give a valid directory.  Using the default instead."
                datadir=None
            else:
                if os.path.exists(datadir):
                    self.datadir=datadir
                    return datadir
        # possible data directories on Flitwick, SCU, Linnaeus
        dirs=[os.environ['HOME']+'/data/embrace',
              '/tmp/data',
              '/data/obs',
              '/data/embrace/data'] 
        for datadir in dirs:
            if os.path.exists(datadir): 
                self.datadir=datadir
                #print "using data directory: "+datadir
                return datadir

        print "using the current working directory for the data directory"
        datadir=os.getcwd()
        self.datadir=datadir
        return datadir


    # return a bandwith value which is a whole number of channel widths
    # the input argument is a bandwidth close to what we want
    def Bandwidth(self,bw=None):
        if bw==None: return self.bandwidth
        if not (isinstance(bw,int) or isinstance(bw,float)):
            print "WARNING: must give an int or float for bandwidth."
            print "bandwidth unchanged: ",self.bandwidth
            return self.bandwidth

        self.nbeamlets=int(bw/self.chanwidth)
        trueBW=self.nbeamlets*self.chanwidth
        self.bandwidth=trueBW
        print 'Observation Functions: assigning nBeamlets=',self.nbeamlets,\
        ' and Bandwidth=',self.bandwidth,' MHz'
        return trueBW

    # return the number of beamlets
    def nBeamlets(self,nbeamlets=None):
        if nbeamlets==None: return self.nbeamlets
        if not isinstance(nbeamlets,int):
            print "WARNING: must give an integer for number of beamlets"
            print "number of beamlets remains unchanged: ",self.nbeamlets
            return self.nbeamlets

        self.nbeamlets=nbeamlets
        self.bandwidth=self.nbeamlets*self.chanwidth
        return self.nbeamlets

    # return number of created digital beams
    def nDigitalBeams(self):
        return self.ndigbeams

    # check if the given centre frequency is acceptable as an RF
    # return True if okay, and False otherwise
    # some frequency choices produce mixing products in the RF band
    # see the document by Philippe Picard: OLcomb.txt
    # input argument is frequency in MHz
    def RFokay(self):
        RFavoid=[[0875.0,0887.5],
                 [0887.5,0912.5],
                 [0935.7,0950.0],
                 [0978.6,0992.9],
                 [0985.0,0995.0],
                 [1033.3,1066.7],
                 [1066.7,1083.3],
                 [1077.8,1088.9],
                 [1116.7,1133.3],
                 [1133.3,1166.7],   
                 [1193.8,1206.3],
                 [1231.3,1243.7],
                 [1250.0,1270.0],
                 [1270.0,1280.0],
                 [1310.0,1330.0],
                 [1342.9,1357.1],
                 [1385.7,1400.0],
                 [1394.4,1405.6],
                 [1475.0,1525.0]]

        # give beamsizes corresponding to the requested centre frequency
        self.log("  Beam sizes for RF centre "+self.float2str(self.RFcentre)+" MHz")
        self.log("  Tile    beam size: "+self.float2str(self.tileBeamSize())+" degrees")
        self.log("  Tileset beam size: "+self.float2str(self.tilesetBeamSize())+" degrees")
        self.log("  Station beam size: "+self.float2str(self.stationBeamSize())+" degrees")

        for span in RFavoid:
            #print span
            if self.RFcentre > span[0] and self.RFcentre < span[1]:
                print '*******************************************************'
                print '*************** WARNING: BAD RF CHOICE ****************'
                difflower=self.RFcentre-span[0]
                diffupper=span[1]-self.RFcentre
                if diffupper > difflower:
                    print '*** try using a frequency less than ',span[0],' MHz ******'
                else:
                    print '*** try using a frequency greater than ',span[1],' MHz ***'
                print '*******************************************************'
                return False
    
        return True

    # make a filename with a date and time (usually the obs. start time)
    def FitsFilename(self,
                     rootname=None, # root name used to build a filename
                     datatype=None, # ssx, bsx, or csx 
                     usetime=None   # a datetime object (usually the start time)
                     ):
        if rootname!=None:
            # use only the first given name, if several in xephem format separated by pipe
            # replace blank spaces
            # and replace calibration source name if we're loading from file
            # and replace DEFAULT name with emptysky, in case we do that before the main observation
            # and remove (PRNnn) from GPS satellite names
            rootname=str(rootname).split('|')[0]
            rootname=rootname.replace('(','').replace(')','')
            self.filerootname=rootname.replace(' ','').replace('DEFAULT--','').replace('DEFAULT','emptysky')
        if datatype==None:
            datatype='unknown-stats'
        if usetime!=None:
            self.filenametime=usetime


        filename=self.filerootname\
            +'_Beam'+self.beam\
            +'_'+datatype\
            +'_'+time.strftime(self.FilenameTimeFmt,self.filenametime.timetuple())

        filexists=os.path.isfile(self.datadir+'/'+filename+'.fits')
        filename0=filename
        n=1
        while filexists and (n<10000):
            filename=filename0+'__'+str("%04d" % n) # n with leading zeros
            n+=1
            filexists=os.path.isfile(self.datadir+'/'+filename+'.fits')
            

        # if we've made 9999 files with the same root name,  this will fail
        filename+='.fits'

        return filename


    # when doing an observation from pointings in a text file
    # there will be some parameters defined there that will be
    # useful in the python script.  Read them...
    def ReadParametersFromText(self,filename,type='rf'):
        if not os.path.exists(filename):
            print "file does not exist: ",filename
            return

        cenfreq=None
        calfreq=None
        # read the file
        txtfile=open(filename,'r')
        for line in txtfile:
            parmstr=line.rsplit('=')
            if parmstr[0]=='fc':
                cenfreq=eval(parmstr[1])
            if parmstr[0]=='fcal':
                calfreq=eval(parmstr[1])
            if cenfreq!=None and calfreq!=None:
                break

        if isinstance(cenfreq,int) or isinstance(cenfreq,float):
            print "found centre frequency: ",cenfreq," MHz"
            self.RFcentre=cenfreq
            if not self.RFokay(): return False
        else:
            print "did not find a centre frequency defined in text file: ",\
                filename

        if isinstance(calfreq,int) or isinstance(calfreq,float):
            print "found calibration frequency: ",calfreq," MHz"
            self.RFcalfreq=calfreq
        else:
            print "did not find a calibration frequency defined in text file: ",\
                filename

        return True


    # return the RA, Dec, AZ, ALT, of a source pointing
    # it could be a source defined in the radio.edb catalog
    # or it could be "ahead" or "behind" the given source
    # try to calculate the position of the source at a future/past time
    # which corresponds to a beam width (by default), distance given in degrees
    def Pointing(self,src,isAstroSrc,tpointing,distance=None):
        pointing={}
        pointing['visible']=True

        # default off-throw
        # if it's the Sun, then go right outside the RF beam (2x RF beam= ~14 degrees)
        #                  unless distance is specified
        if distance==None:
            if src.name=="Sun":distance=2*7
            else: distance=3.0 # default, 2 station beams distance


        # by default, this is not an OFF calculation
        delta=dt.timedelta(minutes=0)
        # if this is an off source, compute the off position
        if self.offsource!=None:
            mins=3.0
            if self.offsource=="ahead": delta=-dt.timedelta(minutes=mins)
            if self.offsource=="behind": delta= dt.timedelta(minutes=mins)

            # estimate the current source movement rate
            self.embraceNancay.date=tpointing
            src.compute(self.embraceNancay)
            az1=math.degrees(float(src.az))
            alt1=math.degrees(float(src.alt))

            self.embraceNancay.date=tpointing+delta
            src.compute(self.embraceNancay)
            az2=math.degrees(float(src.az))
            alt2=math.degrees(float(src.alt))

            # possible bug here: Wed 04 Sep 2013 12:18:17 CEST
            # angularSeparation should be called with arguments in order: alt,az not az,alt
            # with angles given in radians, but the return is in degrees (see "great programming" comment above)
            angDist=self.angularSeparation(alt1,az1,alt2,az2)
            skyrate=math.fabs(angDist/mins)
            #self.log("angular distance traveled in 6 minutes: "+self.float2str(angDist)+" degrees")
            #self.log("calculated sky rate: "+self.float2str(skyrate)+" degrees/minute")
            # compute the number of minutes corresponding to the desired distance
            mins=distance/skyrate
            
            if self.offsource=="ahead":  delta=-dt.timedelta(minutes=mins)
            if self.offsource=="behind": delta= dt.timedelta(minutes=mins)


        self.embraceNancay.date=tpointing+delta
        src.compute(self.embraceNancay)
        az=math.degrees(float(src.az))
        alt=math.degrees(float(src.alt))
        ra=math.degrees(float(src.a_ra))
        dec=math.degrees(float(src.a_dec))
        if src.alt<self.embraceNancay.horizon:
            comment='!WARNING: '+src.name+' is not visible at the following pointing!'
            self.log(comment)
            pointing['visible']=False
        pointing['alt']=alt
        pointing['az']=az
        pointing['ra']=ra
        pointing['dec']=dec
        return pointing

    # translate nicknames for satellites and sources
    # default is for "source" stored values for calibration
    def source_fullname(self,nick):
        if nick==None: return None

        if nick.upper()=="STORED"\
                or nick.upper()=="DEFAULT":
            return 'DEFAULT'

        if nick.upper()=='GPSF1'\
                or nick.upper()=='F1':
            return 'GPS BIIF-1'

        if nick.upper()=='AFRISTAR':
            return nick.upper()

        # check for gps nicks
        match=re.search('RM([0-9]*)',nick.upper())
        if match and match.group(1)!='':
            return 'GPS BIIRM-'+match.group(1)

        match=re.search('^([FAR])([0-9]*)',nick.upper())
        if match and match.group(2)!='':
            return 'GPS BII'+match.group(1)+'-'+match.group(2)

        if nick.upper().find('JAMES')>=0:
            return "NAVSTAR63"
        if nick.upper().find('BOND')>=0:
            return "NAVSTAR63"

        if nick.upper()=="SUN":
            return "Sun"

        if nick.upper()=="B1133":
            return "PSR B1133+16"

        if nick.upper()=="B0329":
            return "PSR B0329+54"

        if nick.upper()=="B0950":
            return "PSR B0950+08"
        
        # if no matches, just return the nick
        return nick

    # add TLE to FITS file comments
    # this is so we can have the TLE valid for the date of the observation
    # for later use in data processing
    def writeTLE2FITS(self,tle):
        self.addFITScomment('Two Line Element used for satellite orbit determination:')
        for line in tle:
            self.addFITScomment(line.replace('\n','').replace('\r',''))
        return None


    # download TLE data for satellites and try to find the requested satellite
    # GPS BII...   carrier is 1227.6MHz
    # GPS BIIF-1/2 carrier is 1176.45MHz
    # GIOVE-A/B    carriers 1176.45, 1207.14, 1278.75
    def FindSat(self,source=None):
        if source==None:satname=self.satname
        else: satname=source

        # special case:  Afristar
        #if self.source_fullname(self.source)=='AFRISTAR':self.satname='Afristar'

        # interpret possible nicknames
        satname=self.source_fullname(satname)
        if satname==None:
            print 'no calibration source given'
            return None

        # check if we want to use stored calibration parameters from file
        if satname=='DEFAULT' or os.path.exists(satname):
            return self.readCalibration(satname)
            
        tleurls=['http://celestrak.com/NORAD/elements/gps-ops.txt',
                 'http://celestrak.com/NORAD/elements/galileo.txt',
                 'http://celestrak.com/NORAD/elements/molniya.txt',
                 'http://celestrak.com/NORAD/elements/geo.txt',
                 'http://celestrak.com/NORAD/elements/beidou.txt',
                 'http://www.tle.info/data/gps-ops.txt']



        self.log('requested source: '+satname)
        # calibration frequency in MHz
        if self.RFcalfreq==None:
            if satname.find('GPS BIIF')==0:
                self.RFcalfreq=1176.45
            elif satname.find('NAVSTAR63')==0:
                self.RFcalfreq=1176.45
            elif satname.find('GIOVE')==0:
                self.RFcalfreq=1176.45
            elif satname.find('Sun')==0:
                self.RFcalfreq=1420.40575177
            elif satname.find('AFRISTAR')==0:
                #self.RFcalfreq=1449.0
                self.RFcalfreq=1489.0
            else:
                self.RFcalfreq=1227.6

        # RF centre frequency in MHz
        if satname=='AFRISTAR':self.assignRFcentre(1474.5)
        cenfreq=self.RFcentre
        if cenfreq==None:cenfreq=self.RFcalfreq
        self.assignRFcentre(cenfreq)

        # maybe we want to use the Sun as a calibrator
        if satname=='Sun':
            self.satname='Sun'
            self.embraceNancay.date=dt.datetime.utcnow()
            self.sateph=eph.Sun(self.embraceNancay)
            return satname

        if satname.find('NAVSTAR63')==0:
            self.RFcalfreq=1176.45
            tle=self.tle_navstar63()
            self.sateph=eph.readtle(tle[0],tle[1],tle[2])
            self.satname=satname
            self.writeTLE2FITS(tle)
            return satname


        # Afristar is geostationary with az=155.10;alt=32.55
        # but we can also get it from the TLE
        if satname=="AFRISTAR":
            self.satname="Afristar"
            self.log("using Afristar as a calibrator")
            return satname

        for tleurl in tleurls:
            self.log("reading satellite TLE data from "+tleurl)
            tlehandle=url.urlopen(tleurl)
            tletxt=tlehandle.readlines()

            n=0
            for line in tletxt:
                i=line.find(satname)
                if i>=0:
                    tle0=tletxt[n]
                    tle1=tletxt[n+1]
                    tle2=tletxt[n+2]
                    self.sateph=eph.readtle(tle0,tle1,tle2)
                    self.satname=satname
                    self.log("satellite: "+self.sateph.name)
                    self.writeTLE2FITS([tle0,tle1,tle2])
                    return satname
                n+=1

                if n>=len(tletxt)-2: break

        
        self.log("satellite not found in the TLE sources")
        self.satname=None
        return None

    # get Navstar63 TLE.  
    # It's not in celestrak, and has to be extracted from the html 
    def tle_navstar63(self):
        satwww=url.urlopen("http://www.n2yo.com/satellite/?s=34661")
        sathtml=satwww.read()

        tlehtmlbeg=sathtml.find("Two Line Element Set")

        tlebeg=sathtml.find("<pre>",tlehtmlbeg)
        tlebeg=sathtml.find(" ",tlebeg)-1
        tleend=sathtml.find("</pre>",tlehtmlbeg)

        tle='NAVSTAR63\n'+sathtml[tlebeg:tleend].replace('\x00','')
        tle_list=tle.split('\n')
        return tle_list

    # the time necessary to complete a RF calibration
    def RFcalDuration(self):
        rfcaldelta=dt.timedelta(seconds=self.rfcaltime)
        return rfcaldelta

    # the time necessary to complete a digital calibration
    def digcalDuration(self):
        # digital calibration takes approximately 1 second per beamlet (say 2.0 to be very conservative)
        # Christophe T. tells me 1 second per beamlet is enough, plus 10 seconds on top of it all
        digcaltime=dt.timedelta(seconds=10.0+1.1*self.nBeamlets())
        return digcaltime

    # current location of the source
    # I noticed these few lines of code were used in multiple places
    # note: no error checking for validity of srcEph, etc
    # use whatever date is already assigned to embraceNancay
    def currentLocation(self,srcEph):
        location={} # return parameters in a dictionary

        srcEph.compute(self.embraceNancay)
        az=math.degrees(float(srcEph.az))
        alt=math.degrees(float(srcEph.alt))
        ra=math.degrees(float(srcEph.a_ra))
        dec=math.degrees(float(srcEph.a_dec))
        location['az']=az
        location['alt']=alt
        location['RA']=ra
        location['dec']=dec
 	self.log(srcEph.name+" RA and dec in degrees: "+str('%.3f' % ra)+" "+str('%.3f' % dec))
        self.log("at "+isodate(self.embraceNancay.date.datetime())+" "+srcEph.name
                 +" is at: az="+str('%.3f' % az)+" alt="+str('%.3f' % alt))
        
        return location


    # calibrate on a satellite
    # this method originally developped in gpsf1cal.py
    def satcal(self,
               driftscan=True,
               driftdate=None,
               yoffset=None,
               now=False,
               digcal=True):

        # the time now in a datetime structure, in UT
        dtnow=dt.datetime.utcnow()
        earliest_startTime=dtnow+self.minimum_setupdelay
        self.log("The time is now "+isodate(dtnow))
        self.embraceNancay.date=dtnow

        # default off pointing (in minutes for a satellite)
        if yoffset==None:yoffset=self.drift_yoffset

        # if no driftscan, reset driftduration (possibly used in the main scripts)
        if not driftscan: self.caldriftduration=dt.timedelta(minutes=0)

        # if obsdate for the calibration sequence
        if self.caldate != None:
            if self.caldate < earliest_startTime:
                self.log('WARNING: given observation date is in the past!  adjusting.')
                obsdate=earliest_startTime
                now=True
            else:
                nowdelta=abs(tot_seconds(self.caldate-dtnow))
                if nowdelta < tot_seconds(self.minimum_setupdelay):
                    self.log('WARNING: given observation date does not allow enough setup time.  adjusting.')
                    obsdate=earliest_startTime
                    now=True
                else:
                    obsdate=self.caldate
        else:
            self.caldate=earliest_startTime
            obsdate=earliest_startTime
        nowdelta=abs(tot_seconds(obsdate-dtnow))

        # check if we're using stored values instead of doing the calibration
        if self.satname=='DEFAULT' or os.path.exists(self.satname):
            return self.assignCalibration(obsdate)
        
        # dictionary for return values
        satcalParms={}
        satcalParms['satname']=self.satname

        isSat=True
        # a satellite must be previously selected using the FindSat() method
        # or the Sun has been selected
        if self.satname==None: 
            self.log("please select a satellite using the FindSat() method")
            return None
        if self.satname=="Sun":
            isSat=False
            self.sateph=eph.Sun(self.embraceNancay)

        # verify that the chosen RF centre frequency is okay
        if not self.RFokay(): return None

        # timestep in seconds
        tstep=dt.timedelta(seconds=self.calibration_stepsize)
        halfstep=dt.timedelta(seconds=0.5*self.calibration_stepsize)
        
        # time to allow for the RF calibration in seconds
        rfcaldelta=self.RFcalDuration()
        
        # number of RF pointings to generate during digital beam calibration
        digcaltime=self.digcalDuration()
        npointings=int(tot_seconds(digcaltime)/self.calibration_stepsize)+1

        # pyEphem returns a tuple when a location calculates a source
        # see: http://rhodesmill.org/pyephem/quick.html
        # 0  Rise time
        # 1  Rise azimuth
        # 2  Transit time
        # 3  Transit altitude
        # 4  Set time
        # 5  Set azimuth


        # give the current position of the calibrator
        location=self.currentLocation(self.sateph)

        self.log("The given observation time is "+isodate(obsdate))

        # give rise and set times etc of the satellite for the requested date
        isAstroSrc=not isSat
        riseTime,transitTime,setTime=self.next_transit(self.sateph,isAstroSrc,obsdate)
        if riseTime==None:return None

        # check if we're already past transit
        # if so, do the calibration run as soon as possible
        # if we insist on doing it 'now', do it.
        # but if the source is not visible "now" set the obsdate to riseTime
        if now:            
            tstart=obsdate
            # check if source is visible at that time
            self.embraceNancay.date=tstart
            location=self.currentLocation(self.sateph)    
            if location['alt'] < self.embraceNancay.horizon:
                tstart=riseTime
                comment="observation will begin at calibrator rise time: "+isodate(tstart)
                self.log(comment)
            else:
                self.log("observation will begin at "+isodate(tstart)+" as requested")            
        elif transitTime < obsdate:
            tstart=obsdate
            self.log("requested observation time is later than transit.")
            self.log("We'll do a calibration run as soon as possible, ie. at "+isodate(tstart))
        else:
            # start a bit before transit time to do the calibration through transit
            # see note above for modification date Wed Nov 16 17:41:32 CET 2011
            startdelta=dt.timedelta(seconds=0.5*npointings*self.calibration_stepsize)
            # convert pyEphem transit time to a py datetime object
            tstart=transitTime-startdelta
            # make sure it's far enough in the future
            if tstart < earliest_startTime:
                tstart=earliest_startTime
                self.log("WARNING: transit is very soon.  calibration will begin as soon as possible, ie. at "+isodate(tstart))
            else:
                self.log("calibration will begin a little before next transit:  "+isodate(tstart))


        # pyEphem stores dates in days since noon 1899 December 31
        # see 
        # http://www.astro.caltech.edu/palomar/60inch/dfox/pyephem-manual.html#ComputingWithDates
        # http://rhodesmill.org/pyephem/date.html


        # header stuff for the pointing files
        # note that existing files with the same name will be overwritten
        satcalStart=tstart
        rootname=self.satname.replace(' ','').replace('(','').replace(')','')
        rfFilename=rootname+"_RF-"+self.beam+"_"+tstart.strftime(self.FilenameTimeFmt)+".txt"
        digFilename=rootname+"_digital-"+self.beam+"_"+tstart.strftime(self.FilenameTimeFmt)+".txt"
        rffile=open(rfFilename,'w')
        digfile=open(digFilename,'w')

        # beginning stuff for the RF pointing file
        rffile.write("! this is RF calibration pointing file: "+rfFilename)
        rffile.write("\ncoord=azel")
        rffile.write("\nfc="+str(self.RFcentre))
        rffile.write("\nfcal="+str(self.RFcalfreq))
        rffile.write("\n!\ntileset_control=false")
        #rffile.write("\ntileset_activetiles=on/off/off/off")
        rffile.write("\ntype_cal=ssx")

        # beginning stuff for the digital beam pointing file
        digfile.write("! this is digital calibration pointing file: "+digFilename)
        digfile.write("\ncoord=azel")
        digfile.write("\ntype_cal=csx")

        # the first RF pointing remains for the duration of the RF calibration

        # following Henrik: get pointings half an interval later so that
        # the source drifts within the beam during a pointing

        CmdTime=tstart
        comment='RF calibration beginning at '+isodate(CmdTime)
        self.log(comment)
        self.addFITScomment(comment)

        tpointing=CmdTime+halfstep
        SatPointingStatus=self.SatPointing(CmdTime,tpointing,rffile)
        rffile.write("\n! After the end of RF calibration, follow the source during the digital calibration")

        CmdTime=tstart+rfcaldelta+tstep
        tpointing=CmdTime+halfstep
        if digcal:
            comment='digital calibration beginning at '+isodate(CmdTime)
            self.log(comment)
            self.addFITScomment(comment)
        # Mon Sep 19 13:40:07 CEST 2011
        # here is the error!!! I didn't update the pointing for the digital calibration
        # Fri Sep 23 14:34:31 CEST 2011
        # now this is replaced by SatPointing().  
        SatPointingStatus=SatPointingStatus and self.SatPointing(CmdTime,tpointing,digfile)
        # Tue 27 Aug 2013 16:36:22 CEST
        # now this is below in the loop, together with RF pointing
        rffile.write("\n\n! listing "+str(npointings)+" steps at intervals of "+str(tstep))
        rffile.write("\ntype_cal=current")
        CmdTime=tstep+tstart+rfcaldelta
        tpointing=CmdTime+halfstep
        SatPointingStatus=SatPointingStatus and self.SatPointing(CmdTime,tpointing,rffile)

        # Tue 27 Aug 2013 16:25:55 CEST
        #   it seems the digital pointing also must be updated.
        #   This is what the LCU uses to calculate the weights
        digfile.write("\n\n! listing "+str(npointings-1)+" steps at intervals of "+str(tstep))
        digfile.write("\ntype_cal=current")

        for npoint in range(1,npointings):
            CmdTime=(npoint+1)*tstep+tstart+rfcaldelta
            tpointing=CmdTime+halfstep
            SatPointingStatus=SatPointingStatus\
                and self.SatPointing(CmdTime,tpointing,rffile)\
                and self.SatPointing(CmdTime,tpointing,digfile)
        CmdTime+=tstep
        rffile.write("\n! digital calibration should be finished now.")
        digfile.write("\n! end of digital calibration expected at "+CmdTime.strftime(self.PointingTimeFmt))


        EndTime=CmdTime+tstep
        satcalParms['satcalEnd']=EndTime
        if driftscan:
            satdriftParms=self.SatDrift(CmdTime+tstep,rffile,digfile,yoffset,
                                        driftdate=driftdate)
            EndTime=satdriftParms['satdriftEnd']
            satcalParms['driftdate']=satdriftParms['driftdate']
            satcalParms['driftdate_y']=satdriftParms['driftdate_y']
        else:
            satcalParms['driftdate']=None
            satcalParms['driftdate_y']=None


        rffile.write("\n")
        rffile.close()
        digfile.write("\n")
        digfile.close()

        self.log("generated pointings for "+self.satname+" calibration")
        if driftscan:
            self.log("including a drift scan on "+self.satname)
        self.log("RF pointings are in file: "+rfFilename)
        self.log("digital beam pointings are in file: "+digFilename)

        if not digcal: digFilename=None
        if not self.readPointingFiles(rfFilename,digFilename): return None
        #if not SatPointingStatus: EndTime=None

        satcalParms['PointingStatus']=SatPointingStatus
        satcalParms['satcalStart']=satcalStart
        satcalParms['EndTime']=EndTime
        return satcalParms

    def SatPointing(self,CmdTime,tpointing,filehandle):
        codret=True
        self.embraceNancay.date=tpointing
        self.sateph.compute(self.embraceNancay)
        az=self.rad2deg*float(self.sateph.az)
        alt=self.rad2deg*float(self.sateph.alt)
        if self.sateph.alt<self.embraceNancay.horizon:
            comment='!WARNING: '+self.sateph.name+' is not visible at the following pointing!'
            self.log(comment)
            filehandle.write('\n'+comment)
            codret=False
        filehandle.write("\nstart="+CmdTime.strftime(self.PointingTimeFmt)\
                             +", angle1="+self.float2str(az)\
                             +", angle2="+self.float2str(alt))
        return codret

    def SatDrift(self,CmdTime,rffile,digfile,yoffset,driftdate=None):
        # dictionary for return values
        satdriftParms={}

        # do a drift scan for "duration" mins on the satellite
        duration=self.caldriftduration

        # get position for start + 1/2 x duration minutes and point there
        satdriftStart=CmdTime
        comment='drift scan beginning at '+isodate(CmdTime)
        self.log(comment)
        self.addFITScomment(comment)

        # end of the drift scan is used later by data acquisition.  It must be in local time
        # since 2011-11-18, all times are UT
        satdriftEnd=CmdTime+duration

        # pointing by default is midway through the duration, unless specified
        if driftdate==None:
            tdrift=CmdTime+duration/2
        else:
            tdrift=str2dt(driftdate)

        self.embraceNancay.date=tdrift
        self.sateph.compute(self.embraceNancay)
        az=self.rad2deg*float(self.sateph.az)
        alt=self.rad2deg*float(self.sateph.alt)
        rffile.write("\n\n! do a drift scan for "+str(tot_seconds(duration)/60.0)+" minutes.")
        comment="! peak should be at "+isodate(tdrift)
        rffile.write("\n"+comment)
        self.log(comment)
        self.addFITScomment(comment)
        #rffile.write("\ntype_cal=current")
        rffile.write("\nstart="+CmdTime.strftime(self.PointingTimeFmt)\
                         +", angle1="+self.float2str(az)\
                         +", angle2="+self.float2str(alt))
        rffile.write("\n! End drift scan")

        # for the digital beam, offset the y direction to see the peak a bit later
        # the pointing offset in relative time, in seconds,
        digfile.write("\n\n! do a drift scan for "+str(tot_seconds(duration)/60.0)+" minutes.")
        comment="! peak should be at "+isodate(tdrift)
        digfile.write("\n"+comment)
        digfile.write("\ntype_cal="+self.digbeam_caltype) # PRenaud's AddSubscanFromFile() requires a type_cal keyword!
        digfile.write("\nstart="+CmdTime.strftime(self.PointingTimeFmt)\
                          +", angle1="+self.float2str(az)\
                          +", angle2="+self.float2str(alt))

        # for the second direction of the digital beam
        # yoffset is given in minutes.
        ypeakdelta=None
        if isinstance(yoffset,float) or isinstance(yoffset,int):
            ypeakdelta=dt.timedelta(seconds=yoffset*60)
            tdrift_y=tdrift+ypeakdelta
            self.embraceNancay.date=tdrift_y
            self.sateph.compute(self.embraceNancay)
            az_y=self.rad2deg*float(self.sateph.az)
            alt_y=self.rad2deg*float(self.sateph.alt)
            digfile.write(", angle3="+self.float2str(az_y)\
                              +", angle4="+self.float2str(alt_y))
            comment="! the y-direction should have a peak at "+isodate(tdrift_y)
            self.log(comment)
            self.addFITScomment(comment)
            digfile.write("\n"+comment)

        digfile.write("\n! End drift scan")
        satdriftParms['satdriftStart']=satdriftStart
        satdriftParms['satdriftEnd']=satdriftEnd
        satdriftParms['driftdate']=tdrift
        satdriftParms['driftdate_y']=tdrift_y
        return satdriftParms


    def Scriptname(self,scriptname,submittime=None):
        if submittime==None:submittime=dt.datetime.utcnow()
        self.scriptname=scriptname
        irootname=scriptname.rfind(os.sep)+1
        rootname=scriptname[irootname:len(scriptname)]
        self.submitlogname=rootname.replace(".py","_"+submittime.strftime(self.FilenameTimeFmt)+".submitlog")

        # check if the script is modern enough
        (mode, ino, dev, nlink, uid, gid, size, atime, mtime, ctime) = os.stat(sys.argv[0])
        localoffset=time.timezone
        dtlastmod=dt.datetime.fromtimestamp(mtime+localoffset)
        dtmodern=dt.datetime(2013, 1,11,11,16,50) # date in UTC when satcal changed to return a dictionary
        dtmodern=dt.datetime(2013,11,15,15,24,43) # date in UTC when obsfunc object for each RF beam
        if dtlastmod<dtmodern:
            self.log('ERROR:  script is old and will not run since changes on '+isodate(dtmodern))
            return False


        self.log("-------------------------------------------------------")
        self.log("--- OBSERVATION SUBMITTED "+isodate(submittime))
        self.log("--- script name:  "+os.path.realpath(scriptname))
        self.log('--- script last modified: '+isodate(dtlastmod))
        # record the arguments given on the command line
        s=""
        for arg in sys.argv:
            s+=" "+arg
        self.log("--- command line arguments: "+s)
        self.log("--- this file: "+os.path.realpath(self.submitlogname))
        # show the name of the submit log in the FITS file
        self.addFITScomment('log of submission: '+os.path.realpath(self.submitlogname))
        return True

    # log messages to the screen and to a file during submission of the observation
    def log(self,comment,master=False):
        filename=self.submitlogname
        handle=open(filename,'a')
        if master or self.beam not in ['A','B']:
            beam_str='*'
        else:
            beam_str=self.beam
        handle.write('\n'+beam_str+'| '+comment)
        handle.close()
        print beam_str+'| '+comment
        return
    
    # add a comment to the FITS file
    def addFITScomment(self,comment):
        if not self.isSet_MyObservation():return False
        self.myObs.ObsConfig.AddComment(self.beam+': '+comment)
        return True

    # setup the statistics acquisition phase of the observation
    def Acquire(self,obstag,AcqStarts,AcqEnds,statsTypes):
        print "\ndefining acquisition for Beam "+self.beam
        if not self.isSet_MyObservation():return False

        if len(obstag)!=len(AcqStarts):
            print "\nproblem!  you need as many AcqStarts as obstags"
            print '\nobstags:'
            for t in obstag: print t
            print '\nAcqStarts:'
            for t in AcqStarts: print isodate(t)
            return False
        if len(AcqEnds)!=len(AcqStarts):
            print "\nproblem!  you need as many AcqStarts as AcqEnds"
            print '\nAcqStarts:'
            for t in AcqStarts: print isodate(t)
            print '\nAcqEnds:'
            for t in AcqEnds: print isodate(t)
            return False

        for nAcq in range(len(obstag)):
            for statsType in statsTypes:

		self.log("AcqStartTime: "+isodate(AcqStarts[nAcq]))
		self.log("AcqStopTime: "+isodate(AcqEnds[nAcq]))
		acqfilename=self.datadir+"/"\
		    +self.FitsFilename(obstag[nAcq],statsType,AcqStarts[nAcq])
		self.log("Acqfile="+acqfilename)
		AcqPhase=self.myObs.AcqPhases.add(AcqStarts[nAcq],
                                                  AcqEnds[nAcq],
                                                  statsType,
                                                  "fits",
                                                  filename=acqfilename,
                                                  beam=self.beam)
		if(AcqPhase == None):
                    self.log("Error creating acqphase at "\
                                 +isodate(AcqStarts[nAcq])+" to "+isodate(AcqEnds[nAcq]))
                    return False

        return True

    # parse arguments input into the observation script
    # note that since this OBSinit.py script is taken with execfile
    # the variables here will live into the observation script
    # and sys.argv refers to the calling script
    #
    # this is not implemented yet...
    # it's a bit complicated because we have to set all the internal variable accordingly
    # and then modify all the observation scripts... is it worth the trouble?
    def parseargs(self):
        return

    # read an xephem style source catalog
    def ReadSourceCatalog(self,filename=None):
        self.sourcecat={}
        if filename==None:
            filename=FullPathname('radio.edb')

        # check if catalog exists, otherwise define a couple of sources manually
        if filename==None or not os.path.exists(filename):
            self.log("Cannot find source catalog: "+filename)
            self.sourcecat['CasA']=eph.readdb('CasA,f|N|SN,23:23:24,58:48:54,0,2000,1762')
            self.sourcecat['CasA Offset']=eph.readdb('CasA Offset,f|N|SN,23:23:24,61:48:54,0,2000,1762')
            return

        handle=open(filename,'r')
        for line in handle.readlines():
            if line.find("#")==0:
                continue
            name=line.split(',')[0].split('|')[0]
            self.sourcecat[name]=eph.readdb(line)
        handle.close()
        print "using source catalog: "+filename
        return

    # next_transit for an astro source
    # used in next_transit() below, to avoid deep nesting
    def next_transit_astro(self,srcEph,date):
        self.embraceNancay.date=date
        self.log('finding next transit after: '+isodate(self.embraceNancay.date.datetime()))
        try:
            src_transit=self.embraceNancay.next_transit(srcEph).datetime()
            src_transit_alt=srcEph.alt*self.rad2deg
            self.log(srcEph.name+" transits at "+isodate(src_transit)
                     +" at alt="+str(src_transit_alt))
            
        except eph.NeverUpError:
            self.log(srcEph.name+" is never visible!")
            return None

        # ensure the transit time is *after* the requested date
        if src_transit < date: 
            self.log("ERROR in next_transit:  transit time earlier than requested date")
            return None
        return src_transit

    # next_rise for an astro source
    def next_rise_astro(self,srcEph,date):
        self.embraceNancay.date=deepcopy(date)
        self.log('finding next rise after: '+isodate(self.embraceNancay.date.datetime()))
        try:
            src_rise=self.embraceNancay.next_rising(srcEph).datetime()
            src_rise_az=math.degrees(srcEph.az)
            self.log(srcEph.name+" rises at "+isodate(src_rise)
                     +" at az="+str(src_rise_az))

        except eph.AlwaysUpError:
            self.log(srcEph.name+" is always up.  Assigning requested date for rise time")
            src_rise=date+dt.timedelta(minutes=1)

        except eph.NeverUpError:
            self.log(srcEph.name+" never rises above the horizon!")
            return None

        # ensure the rise time is *after* the requested date
        if src_rise < date - dt.timedelta(minutes=1): # add margin, because datetime is imprecise (?)
            self.log("ERROR in next_rise:  rise time earlier than requested date")
            print '*** DEBUG: src_rise: ',isodate(src_rise),' requested date: ',isodate(date)
            return None

        return src_rise

    # next_setting for an astro source
    def next_setting_astro(self,srcEph,date):
        self.embraceNancay.date=date
        self.log('finding next setting after: '+isodate(self.embraceNancay.date.datetime()))
        try:
            src_set=self.embraceNancay.next_setting(srcEph,start=date).datetime()
            src_set_az=srcEph.az*self.rad2deg
            self.log(srcEph.name+" sets at "+isodate(src_set)
                     +" at az="+str(src_set_az))

        except eph.AlwaysUpError:
            self.log(srcEph.name+" is always up.  Assigning tomorrow for the setting time")
            src_set=date+dt.timedelta(days=1)

        except eph.NeverUpError:
            self.log(srcEph.name+" never rises above the horizon!")
            return None

        # ensure the setting time is *after* the requested date
        if src_set < date: 
            self.log("ERROR in next_setting:  setting time earlier than requested date")
            return None

        return src_set

    # next_transit used in SourceTrack and in satcal
    def next_transit(self,srcEph,isAstroSrc,date=None):
        dtnow=dt.datetime.utcnow()
        if date==None: date=dtnow
        self.embraceNancay.date=date
        #srcEph.compute(self.embraceNancay)

        # rise, set, and transit times for an astro source
        if isAstroSrc:
            src_transit=self.next_transit_astro(srcEph,date)
            if src_transit==None: return None,None,None

            src_rise=self.next_rise_astro(srcEph,date)
            if src_rise==None: return None,None,None

            src_set=self.next_setting_astro(srcEph,date)
            if src_set==None: return None,None,None

            return src_rise,src_transit,src_set


        # rise, set, and transit times for a satellite
        try:
            satstats=self.embraceNancay.next_pass(srcEph)
        except:
            errmsg=traceback.format_exc()
            self.log(srcEph.name+": error.  "+errmsg)
            self.log("Assigning tomorrow for the setting time")
            src_rise=dtnow
            src_transit=dtnow
            src_set=dtnow+dt.timedelta(days=1)
            return src_rise,src_transit,src_set
        
        if satstats[0]==None:
            self.log(srcEph.name+" WARNING: no rise/set/transit times returned from next_pass()")
                # assume bogus transit time for testing
            src_rise=dtnow
            src_rise_az=0.0
            src_transit=dtnow
            src_transit_alt=0.0
            src_set=dtnow
            src_set_az=0.0
        else:
            src_rise=satstats[0].datetime()
            src_rise_az=self.rad2deg*satstats[1]
            src_transit=satstats[2].datetime()
            src_transit_alt=self.rad2deg*satstats[3]
            src_set=satstats[4].datetime()
            src_set_az=self.rad2deg*satstats[5]
            
        self.log(""+srcEph.name+" rises at "+isodate(src_rise)
                 +" at az="+str(src_rise_az))
        self.log(srcEph.name+" transits at "+isodate(src_transit)
                 +" at alt="+str(src_transit_alt))
        self.log(srcEph.name+" sets at "+isodate(src_set)
                 +" at az="+str(src_set_az))
        
        return src_rise,src_transit,src_set


    # calculate angular separation between two points on the sky
    # algo given by Henrik 2012-07-05
    # angles can be alt-az, or RA-dec
    # give angles in radians! but get output in degrees.  is that good programming or what.
    def angularSeparation(self,theta1,phi1,theta2,phi2):
        cos_angle=math.sin(theta1) * math.sin(theta2)\
            + math.cos(theta1) * math.cos(theta2) * math.cos( phi1-phi2 ) 
        try:
            return math.degrees(math.acos( cos_angle ))
        except:
            print "ERROR computing angular separation: arc cos of "+str(" %10.7f" % cos_angle)
            if math.fabs(cos_angle-1.0)>0.001:return None
            print "assigning zero to angular separation"
            return 0.0

        return

    # verify that a given source is okay
    # this is used in SourceTrack()
    # return a dictionary with info, or None if a problem encountered
    def verifySource(self,source,StartTime,OffBeam,transit=False,elevation=None,now=False):
        srcParms={}
        if self.sourcecat==None: self.ReadSourceCatalog()

        # hold onto the horizon value in case we change it and then restore it
        true_horizon=self.embraceNancay.horizon

        if elevation!=None:
            if transit:
                self.log('WARNING: start time requested at elevation '+str('%.2f degrees' % elevation))
                self.log('         and also requesting start time at transit.  Elevation request takes precedence')
                transit=False
            if elevation > 90.:
                self.log('start time requested at elevation>90 !')
                return None
            # convert elevation angle to radians
            self.embraceNancay.horizon=math.radians(elevation) # rise time will be at the required elevation
        #print 'horizon: ',self.embraceNancay.horizon

        # convert StartTime to a datetime structure if necessary
        StartTime=str2dt(StartTime)

        # if StartTime given is None, set it to now
        if StartTime==None: StartTime=dt.datetime.utcnow()

        # set date to check if source is visible at StartTime
        self.embraceNancay.date=StartTime

        # convert nickname
        source=self.source_fullname(source)

        # check if we know the source
        if source=="Sun":
            srcEph=eph.Sun(self.embraceNancay)
            isAstroSrc=True
        elif source=="Afristar":
            # Afristar:  geostationary az=155.10;alt=32.55
            isAstrSrc=False
        elif source in self.sourcecat:
            srcEph=self.sourcecat[source]
            isAstroSrc=True
        else:
            isAstroSrc=False
            # check if it's a satellite
            satname=self.FindSat(source)
            if satname==None:
                self.log("Not a known satellite: "+source)
                return None
            srcEph=self.sateph

	# find the RA and dec (to be given in degrees)
	# and the azimuth-elevation at the start time
        location=self.currentLocation(srcEph)
        src_ra=location['RA']
        src_dec=location['dec']

        # check for an offset beam
        RFpointing_ra=None
        if isAstroSrc:
            if OffBeam==None or OffBeam=='ahead' or OffBeam=='behind':
                off_dec=src_dec
                if OffBeam=='ahead':
                    off_ra=src_ra-3
                    RFpointing_ra=src_ra-1.5
                elif OffBeam=='behind':
                    off_ra=src_ra+3
                    RFpointing_ra=src_ra+1.5
                else:
                    off_ra=src_ra
                    RFpointing_ra=src_ra

                # convert RA degrees to hours
                ra=off_ra*24/360.
                # create a source line for pyephem
                line='OFFSOURCE,f,'+str(ra)+','+str(off_dec)+',0,2000'
                offEph=eph.readdb(line)                
            else:
                if not OffBeam in self.sourcecat:
                    self.log("ERROR: Offset beam not defined in the catalog.")
                    return None
                offEph=self.sourcecat[OffBeam]
            # define the time difference between src and offsrc for a driftscan
            # take the difference between transit times
            src_rise,src_transit,src_set=self.next_transit(srcEph,isAstroSrc,StartTime)
            off_rise,off_transit,off_set=self.next_transit(offEph,isAstroSrc,StartTime)
            self.drift_yoffset=tot_seconds(off_transit-src_transit)/60.
            
        else:
            offEph=srcEph

	# rise, set, and transit times
	src_rise,src_transit,src_set=self.next_transit(srcEph,isAstroSrc,StartTime)
	if src_transit==None:return None

        # restore horizon value
        self.embraceNancay.horizon=true_horizon
        #print 'horizon: ',self.embraceNancay.horizon

        # interpret the options for StartTime
        if now and transit:
            self.log('WARNING: requesting start time at transit and at a given time.')
            self.log('         precedence given to time request')
            transit=False
        if transit:
            StartTime=src_transit
        elif now: # check for visibility
            self.embraceNancay.date=StartTime
            srcEph.compute(self.embraceNancay)
            # add 2 degree margin to horizon to compensate for PyEphem imprecision
            if float(srcEph.alt) < (float(self.embraceNancay.horizon)-math.radians(2.0)): 
                self.log('WARNING: '+srcEph.name+' not visible at requested time: '+isodate(self.embraceNancay.date.datetime()))
                self.log('         doing calibration at rise time: '+isodate(src_rise))
                self.log('\nsrcEph.alt: '+str(srcEph.alt)+'\nhorizon: '+str(self.embraceNancay.horizon))
                self.log('\nsrcEph.alt: '+str(float(srcEph.alt))+'\nhorizon: '+str(float(self.embraceNancay.horizon)+math.radians(2.0)))

                StartTime=src_rise
        else:
            StartTime=src_rise


        srcParms['srcEph']=srcEph
        srcParms['offEph']=offEph
        srcParms['offBeam keyword']=OffBeam
        srcParms['RFpointing RA']=RFpointing_ra
        srcParms['RFpointing dec']=src_dec
        srcParms['isAstroSrc']=isAstroSrc
        srcParms['StartTime']=StartTime
        srcParms['start at transit']=transit
        self.srcParms=srcParms
        return srcParms

    # open text files for RF and digital beam pointings
    # this is used in SourceTrack()
    # NOTE NOTE NOTE:  must remove "type_cal" if we want to use assignCalibration() 
    def openPointingFiles(self,source,StartTime):
        rootname=source.replace(' ','').replace('(','').replace(')','')
        rfFilename=rootname+"_RF-"+self.beam+"_"+StartTime.strftime(self.FilenameTimeFmt)+".txt"
        digFilename=rootname+"_digital-"+self.beam+"_"+StartTime.strftime(self.FilenameTimeFmt)+".txt"
        rffile=open(rfFilename,'w')
        digfile=open(digFilename,'w')

        # a file to verify the angular distance of the offset beam
        offsetFilename=rootname+"_digital-"+self.beam+"_"+StartTime.strftime(self.FilenameTimeFmt)+".offset.txt"
        offsetfile=open(offsetFilename,'w')

        # beginning stuff for the RF pointing file
        rffile.write('\n! This is RF tracking for '+source+'.  Filename: '+rfFilename)
        rffile.write('\n! It was generated by '+sys.argv[0]+' on '+isodate(dt.datetime.utcnow()))
        rffile.write("\ncoord=azel")
        rffile.write("\nfc="+str(self.RFcentre))
        rffile.write("\nfcal="+str(self.RFcalfreq))
        rffile.write("\n!\ntileset_control=false")
        #rffile.write("\ntileset_activetiles=on/off/off/off")
        #rffile.write("\ntype_cal=current")

        # beginning stuff for the digital beam pointing file
        digfile.write('\n! This is digital beam tracking for '+source+'.  Filename: '+digFilename)
        digfile.write('\n! It was generated by '+sys.argv[0]+' on '+isodate(dt.datetime.utcnow()))
        digfile.write("\ncoord=azel")
        digfile.write("\ntype_cal="+self.digbeam_caltype) # PRenaud's AddSubscanFromFile() requires a type_cal keyword!

        pointingFiles={}
        pointingFiles['rffile']=rffile
        pointingFiles['rfFilename']=rfFilename
        pointingFiles['digfile']=digfile
        pointingFiles['digFilename']=digFilename
        pointingFiles['offsetfile']=offsetfile
        return pointingFiles

    # track a source by generating and reading a pointing file
    # return the EndTime when the last pointing is done
    # do Digital Calibration if requested
    #  MyObservation is a TObservation object
    #  source should be something known from the radio.edb catalog
    #  StartTime is a datetime object and is expected in UT
    #  OffBeam is a "source" from the radio.edb catalog.  Define appropriate off positions there.
    #  duration=None means track the source while visible, otherwise give a timedelta object
    #  calibration_interval is either None or a timedelta object
    #  calibration_interval=None means no calibration
    #  calibration_interval=0 calibrate only at the beginning
    #  calibration_interval=timedelta : do a calibration every timedelta
    #  Note that only digital calibration is done here.  It is assumed the RF calibration has been done.
    # NOTE NOTE NOTE:  must remove "type_cal" if we want to use this with assignCalibration()
    def SourceTrack(self,StartTime,
                    OffBeam_distance=None,
                    calibration_interval=None,
                    digtrack=True):
        srcParms=self.verifySource(self.source,StartTime,self.offsource,now=True)
        if srcParms==None: return None,None
        srcEph=srcParms['srcEph']
        offEph=srcParms['offEph']
        isAstroSrc=srcParms['isAstroSrc']
        StartTime=srcParms['StartTime']

        # find the RA and dec (to be given in degrees)
        # and the elevation at the time requested
        location=self.currentLocation(srcEph)
        src_alt=location['alt']

        offPointing=self.Pointing(offEph,isAstroSrc,StartTime,distance=OffBeam_distance)
        self.log("offset beam RA and dec in degrees: "+
                 self.float2str(offPointing['ra'])+
                 " "+self.float2str(offPointing['dec']))

        # rise, set, and transit times
        src_rise,src_transit,src_set=self.next_transit(srcEph,isAstroSrc,StartTime)


        CmdTime=StartTime
        comment='tracking by AZEL beginning at '+isodate(CmdTime)
        self.log(comment)
        self.addFITScomment(comment)

        # duration of the observation, if none given, go until source sets
        if self.duration==None: 
            endobs=src_set
            self.duration=src_set-StartTime
        else:
            endobs=StartTime+self.duration

        # open text files for RF and digital beam pointings
        pointingFiles=self.openPointingFiles(srcEph.name,StartTime)

        # start off with a digital calibration if requested
        digcaltime=self.digcalDuration()
        calibrating=False
        newcal=False
        endCal=None
        if calibration_interval==None:
            #pointingFiles['digfile'].write("\ntype_cal=current")
            pass
        else:
            calibrating=True
            newcal=True
            endCal=CmdTime+digcaltime
            if isinstance(calibration_interval,dt.timedelta):
                nextCal=CmdTime+calibration_interval


        # generate the list of pointings
        # command time is given in UT in the pointing files

        # flag for the first time through the loop
        # this is needed for initial digital pointing, in case we don't want digital beam tracking
        firstFlag=True 

        ########### not implemented #################################
        # NOTE NOTE NOTE:  isinstance(digtrack,int) returns True for digtrack boolean!!!!!
        # find digtrack_threshold from the digtrack keyword
        #if isinstance(digtrack,float):
        #    digtrack_threshold=digtrack
        #    digtrack=False

        if not digtrack: self.log("--- No Digital Pointing ---")

        # do RF and Digital pointing in separate loops
        # to allow for different update times for repointing
        pointingStartTime=CmdTime


        # RF pointing
        # step sizes for RF pointing
        dtstepsize=dt.timedelta(seconds=self.RFstepsize) # update pointing every stepsize
        halfstep=dt.timedelta(seconds=0.5*self.RFstepsize) # point a half timestep later
        while CmdTime<=endobs:
            tpointing=CmdTime+halfstep
            
            self.embraceNancay.date=tpointing
            srcEph.compute(self.embraceNancay)
            az=self.rad2deg*float(srcEph.az)
            alt=self.rad2deg*float(srcEph.alt)
            pointingFiles['rffile'].write("\nstart="+CmdTime.strftime(self.PointingTimeFmt)\
			     +", angle1="+self.float2str(az)\
			     +", angle2="+self.float2str(alt))

            CmdTime+=dtstepsize

        # close the RF pointing files, and read them later.
        pointingFiles['rffile'].write('\n')
        pointingFiles['rffile'].close()

        # digital beam pointing
        # step sizes for RF pointing
        dtstepsize=dt.timedelta(seconds=self.stepsize) # update pointing every stepsize
        halfstep=dt.timedelta(seconds=0.5*self.stepsize) # point a half timestep later
        CmdTime=pointingStartTime
        while CmdTime<=endobs:
            tpointing=CmdTime+halfstep
            
            self.embraceNancay.date=tpointing
            srcEph.compute(self.embraceNancay)
            az1=float(srcEph.az)   # to use later for calculating sky angle
            alt1=float(srcEph.alt) # to use later for calculating sky angle
            az=self.rad2deg*float(srcEph.az)
            alt=self.rad2deg*float(srcEph.alt)

            if newcal:
                pointingFiles['digfile'].write("\ntype_cal=csx")               
                pointingFiles['digfile'].write("\nstart="+CmdTime.strftime(self.PointingTimeFmt)\
                                  +", angle1="+self.float2str(az)\
                                  +", angle2="+self.float2str(alt))
                newcal=False

            # track with the digital beam
            if not calibrating:
                ### eventually, replace distance=None with an option from the command line
                offpointing=self.Pointing(offEph,isAstroSrc,tpointing,distance=None)
                offset_az=offpointing['az']
                offset_alt=offpointing['alt']

                # az1 and alt1 are given above
                az2=math.radians(float(offset_az))
                alt2=math.radians(float(offset_alt))
                offset_angular_separation=self.angularSeparation(alt1,az1,alt2,az2)
                if offset_angular_separation==None:return None,None

                # if we're not tracking with the digital beam
                # check if we're so far away that we really do have to update the dig beam pointing
                # say, 3x the 3dB RF beam radius
                updateDigpointing=digtrack
                if not digtrack:
                    if firstFlag:
                        firstFlag=False
                        updateDigpointing=True
                        az_prev=az
                        alt_prev=alt
                    else:
                        updateDigpointing=False
                        angdist=self.angularSeparation(alt_prev,az_prev,alt,az)
                        if angdist/self.tilesetBeamSize() > 3: 
                            updateDigpointing=True
                            az_prev=az
                            alt_prev=alt
                    
                if updateDigpointing:
                    pointingFiles['digfile'].write("\nstart="+CmdTime.strftime(self.PointingTimeFmt)\
                                      +", angle1="+self.float2str(az)\
                                      +", angle2="+self.float2str(alt)\
                                      +", angle3="+self.float2str(offset_az)\
                                      +", angle4="+self.float2str(offset_alt))
                    pointingFiles['offsetfile'].write("\nstart="+CmdTime.strftime(self.PointingTimeFmt)\
                                         +", angle1="+self.float2str(az)\
                                         +", angle2="+self.float2str(alt)\
                                         +", angle3="+self.float2str(offset_az)\
                                         +", angle4="+self.float2str(offset_alt)\
                                         +", offset separation="+self.float2str(offset_angular_separation))
                

            CmdTime+=dtstepsize
            if calibrating:
                if CmdTime>=endCal:
                    calibrating=False
                    pointingFiles['digfile'].write("\ntype_cal=current")
                    if isinstance(calibration_interval,dt.timedelta):
                        nextCal=CmdTime+calibration_interval
                        endCal=nextCal+digcaltime
            else:
                if isinstance(calibration_interval,dt.timedelta):
                    if CmdTime>=nextCal:
                        calibrating=True
                        newcal=True

        # close the files, and then read them
        pointingFiles['digfile'].write('\n')
        pointingFiles['digfile'].close()
        pointingFiles['offsetfile'].write('\n')
        pointingFiles['offsetfile'].close()

        if not self.readPointingFiles(pointingFiles['rfFilename'],pointingFiles['digFilename']):
            return None,None
        
        EndTime=CmdTime+dtstepsize

        # print the end time for tracking, and also put it in the FITS file
        comment='tracking by AZEL ending at '+isodate(EndTime)
        self.log(comment)
        self.addFITScomment(comment)

        return StartTime,EndTime

    # drift scan (adapted from driftscan.py, last mod: Mon Dec 23 08:45:44 CET 2013)
    ### see also SatDrift() above, which uses the method of generating a pointing text file
    def driftscan(self,StartTime=None,transit=False,digbeam=0,now=False):
        driftparms={} # return dictionary with parameters, endTime, etc

        RFBeam=self.getRFBeam()

        digbeam_index=digbeam
        driftparms['digbeam_index']=digbeam
        DigBeam,codret=RFBeam.GetDigitalBeam(digbeam_index)
        if codret<0:
            self.log('ERROR: There is no digital beam '+str("#%02i" % digbeam_index)+' defined for this beam')
            return None

	# duration of the observation, if none given, default 24 hours
	if self.duration==None: self.duration=dt.timedelta(minutes=24*60.)

	# verify the source, and get basic parameters.  Note if StartTime==None, it gets set to now
	srcParms=self.verifySource(self.source,StartTime,self.offsource,now=now)
	if srcParms==None: return None
        StartTime=srcParms['StartTime']
        transit=srcParms['start at transit'] # verifySource() resolves conflicts between "now" and "transit"
	endobs=StartTime+self.duration

	srcEph=srcParms['srcEph']
        driftparms['srcEph']=srcEph
	isAstroSrc=srcParms['isAstroSrc']
        offsrcEph=srcParms['offEph'] # verifySource() finds offEph

	# find the RA and dec (to be given in degrees)
	# and the azimuth-elevation at the start time
        location=self.currentLocation(srcEph)

	# rise, set, and transit times
	src_rise,src_transit,src_set=self.next_transit(srcEph,isAstroSrc,StartTime)
	if src_transit==None:return None

        # try to get around the bug in pyephem... it returns a transit time which is earlier than StartTime
        if src_transit < StartTime:
            src_rise,src_transit,src_set=self.next_transit(srcEph,isAstroSrc,src_set+dt.timedelta(minutes=10))
            

        """
        print '*** DEBUG:  transit is ',transit
        print '*** DEBUG:  StartTime ',isodate(StartTime)
        print '*** DEBUG:  endobs ',isodate(endobs)
        print '*** DEBUG:  transit time ',isodate(src_transit)
        """

	src_peaktime=src_transit
        comment='drift scan around next transit'
	if src_transit > endobs:
            if transit:
                StartTime=src_transit-self.duration/2
                endobs=StartTime+self.duration
            else:
                comment='requested duration is not long enough to point at transit\n'\
                    +'    pointing for driftscan at mid-duration'
                src_peaktime=StartTime+self.duration/2            

        self.log(comment)
        driftparms['StartTime']=StartTime
        driftparms['xpeak']=src_peaktime
        driftparms['enddrift']=endobs

	# azel at transit
	self.embraceNancay.date=src_peaktime
	srcEph.compute(self.embraceNancay)
	src_az=math.degrees(srcEph.az)
	src_alt=math.degrees(srcEph.alt)
	src_ra=math.degrees(srcEph.a_ra)
	src_dec=math.degrees(srcEph.a_dec)
        driftparms['az']=src_az
        driftparms['alt']=src_alt
        driftparms['ra']=src_ra
        driftparms['dec']=src_dec

	CmdTime=StartTime
	comment='pointing by AZEL beginning at '+isodate(CmdTime)
	self.log(comment)
	self.addFITScomment(comment)
	comment='  az='+str(src_az)+' degrees, alt='+str(src_alt)+' degrees'
	self.log(comment)
	comment="! peak should be at "+isodate(src_peaktime)
	self.log(comment)
	self.addFITScomment(comment)

	# RF beam pointing
	#RFBeam.SetCal('current')
	#DigBeam.SetCal('current')
	RFBeam.AddSubscan(CmdTime,'AZEL',src_az,src_alt)

	# offsource if requested
        offsrcEph=srcParms['offEph']
	if srcParms['offBeam keyword']==None:
		offsrcEph.compute(self.embraceNancay)
		off_az=math.degrees(offsrcEph.az)
		off_alt=math.degrees(offsrcEph.alt)
	
	else: # by default use an offset 3 deg away in RA if it's an astro source
            if isAstroSrc:
		off_dec=src_dec

		# to calculate when the source passes through the off position
		# set off_ra on the opposite side, and calculate transit
		off_ra=src_ra+3

		# convert RA degrees to hours
		ra=off_ra*24/360.
		# create a source line for pyephem
		line='OFFSOURCE,f,'+str(ra)+','+str(off_dec)+',0,2000'
		offsrcEph=eph.readdb(line)
		offsrcEph.compute(self.embraceNancay)
	
		# indicate transit
		off_rise,off_transit,off_set=self.next_transit(offsrcEph,isAstroSrc,StartTime)
                driftparms['ypeak']=off_transit
            else:
                yoffset=self.drift_yoffset
                ypeakdelta=dt.timedelta(seconds=yoffset*60)
                tdrift_y=src_peaktime+ypeakdelta
                driftparms['ypeak']=tdrift_y

                    
            self.embraceNancay.date=driftparms['ypeak']
            offsrcEph.compute(self.embraceNancay)
            off_az=math.degrees(float(offsrcEph.az))
            off_alt=math.degrees(float(offsrcEph.alt))
            comment="! the y-direction should have a peak at "+isodate(driftparms['ypeak'])
            self.log(comment)
            self.addFITScomment(comment)
                

        driftparms['off az']=off_az
        driftparms['off alt']=off_alt
        DigBeam.AddSubscan(CmdTime,'AZEL',src_az,src_alt,off_az,off_alt)
        return driftparms


    #### RF calibration
    ##### this is different from satcal().  only RF calibration, no digital calibration
    def RFcal(self,StartTime=None,transit=False,elevation=None,now=False):
        rfcalparms={} # return dictionary with parameters, endTime, etc

        # default StartTime is caldate possibly given on the command line
        if StartTime==None:StartTime=self.caldate

	# verify the source, and get basic parameters.  Note if StartTime==None, it gets set to now
	srcParms=self.verifySource(self.source,StartTime,self.offsource,transit,elevation,now)
	if srcParms==None: return None

        StartTime=srcParms['StartTime']
        rfcalparms['StartTime']=StartTime

	srcEph=srcParms['srcEph']
        rfcalparms['srcEph']=srcEph

    	rfcaldelta=self.RFcalDuration()
	comment='RF calibration beginning at '+isodate(StartTime)
	self.log(comment)
	self.addFITScomment(comment)

        RFBeam=self.getRFBeam()
	RFBeam.SetCal('ssx')

        # number of pointings to generate during RF beam calibration
        npointings=int(tot_seconds(rfcaldelta)/self.calibration_stepsize)+1

        #### but maybe we shouldn't be repointing during RF cal?

        # loop for az-el pointings
        # timestep
        # text file for checking pointing (pointing also appears in FITS files)
        pointingFiles=self.openPointingFiles(self.source_fullname(self.source),StartTime)
        tstep=dt.timedelta(seconds=self.calibration_stepsize)
        halfstep=dt.timedelta(seconds=0.5*self.calibration_stepsize)
        CmdTime=StartTime
        for n in range(npointings):
            tpointing=CmdTime+halfstep
            self.embraceNancay.date=tpointing
            srcEph.compute(self.embraceNancay)
            az =math.degrees(float(srcEph.az))
            alt=math.degrees(float(srcEph.alt))
            if srcEph.alt<self.embraceNancay.horizon:
                comment='!WARNING: '+srcEph.name+' is not visible at this pointing:  (az,el)='+str('(%.2f, %.2f)' % (az,alt))
                self.log(comment)
            if n==1: RFBeam.SetCal('current')
            RFBeam.AddSubscan(CmdTime,'AZEL',az,alt)
            pointingFiles['rffile'].write("\nstart="+CmdTime.strftime(self.PointingTimeFmt)\
                                                  +", angle1="+self.float2str(az)\
                                                  +", angle2="+self.float2str(alt))
            CmdTime+=tstep

        # close pointing text files
        for key in ['rffile','digfile','offsetfile']:
            pointingFiles[key].write('\n! this file generated for checking only.  pointings are not read from here\n')
            pointingFiles[key].close()

        # ensure next observation uses this calibration
        RFBeam.SetCal('current')
        rfcalparms['endCal']=CmdTime
        return rfcalparms



    ##### digital calibration 
    ##### this is different from satcal().  only digital calibration, no RF calibration
    ##### by default, do it at rise time.  
    #####     elevation should be from horizon to 90 in degrees
    def digcal(self,StartTime=None,transit=False,elevation=None,now=False,digbeam=0):
        digparms={} # return dictionary with parameters, endCal, etc

        # time necessary for a digital calibration
        digcaltime=self.digcalDuration()

        # number of pointings to generate during digital beam calibration
        npointings=int(tot_seconds(digcaltime)/self.calibration_stepsize)+1

        RFBeam=self.getRFBeam()
	RFBeam.SetCenterFreq(self.RFcentre)
	RFBeam.SetCal('current')

        digbeam_index=digbeam
        digparms['digbeam_index']=digbeam
        DigBeam,codret=RFBeam.GetDigitalBeam(digbeam_index)
        if codret<0:
            self.log('ERROR: There is no digital beam '+str("#%02i" % digbeam_index)+' defined for this beam')
            return None

	# verify the source, and get basic parameters.  Note if StartTime==None, it gets set to now
	srcParms=self.verifySource(self.source,StartTime,self.offsource,transit,elevation,now)
	if srcParms==None: return None

	isAstroSrc=srcParms['isAstroSrc']

        if srcParms['start at transit']:
            StartTime=srcParms['StartTime']-digcaltime/2
        else:
            StartTime=srcParms['StartTime']

	srcEph=srcParms['srcEph']
        digparms['srcEph']=srcEph

        endobs=StartTime+digcaltime

        comment='digital calibration beginning at '+isodate(StartTime)
        self.log(comment)
        self.addFITScomment(comment)
        digparms['StartTime']=StartTime

	CmdTime=StartTime

	# RF beam pointing
	DigBeam.SetCal('csx')

        # text file for checking pointing (pointing also appears in FITS files)
        pointingFiles=self.openPointingFiles(self.source_fullname(self.source),StartTime)

        # loop for az-el pointings
        # timestep
        tstep=dt.timedelta(seconds=self.calibration_stepsize)
        halfstep=dt.timedelta(seconds=0.5*self.calibration_stepsize)
        for n in range(npointings):
            tpointing=CmdTime+halfstep
            self.embraceNancay.date=tpointing
            srcEph.compute(self.embraceNancay)
            az =math.degrees(float(srcEph.az))
            alt=math.degrees(float(srcEph.alt))
            if srcEph.alt<self.embraceNancay.horizon:
                comment='!WARNING: '+srcEph.name+' is not visible at this pointing:  (az,el)='+str('(%.2f, %.2f)' % (az,alt))
                self.log(comment)

            RFBeam.AddSubscan(CmdTime,'AZEL',az,alt)
            if n==1:DigBeam.SetCal('current') # after first pointing, don't ask for more calibrations!
            DigBeam.AddSubscan(CmdTime,'AZEL',az,alt,az,alt)
            pointingFiles['digfile'].write("\nstart="+CmdTime.strftime(self.PointingTimeFmt)\
                                                  +", angle1="+self.float2str(az)\
                                                  +", angle2="+self.float2str(alt)\
                                                  +", angle3="+self.float2str(az)\
                                                  +", angle4="+self.float2str(alt))
            CmdTime+=tstep

        # close pointing text files
        for key in ['rffile','digfile','offsetfile']:
            pointingFiles[key].write('\n! this file generated for checking only.  pointings are not read from here\n')
            pointingFiles[key].close()

        # ensure next observation uses this calibration
        DigBeam.SetCal('current')
        digparms['endCal']=CmdTime
        return digparms
    ##### end digital calibration


    # assign the MyObservation object
    # once that is done, we can make a comment in the FITS file
    def setMyObservation(self,MyObservation):
        self.myObs=MyObservation
        return True

    # verify that MyObservation has been set
    def isSet_MyObservation(self):
        if self.myObs==None:
            # if it's the dailyobs script, return quietly
            if os.path.basename(self.scriptname)=='dailyobs.py':
                return False
            self.log("ERROR: you must set the MyObservation object"+
                     "\nusing method setMyObservation(<MyObservation Object>)")
            return False
        return True

    # get the RF Beam object 
    def getRFBeam(self):
        if not self.isSet_MyObservation():return None
	if self.beam=='A': RFBeam=self.myObs.GetRFbeamA()
	elif self.beam=='B': RFBeam=self.myObs.GetRFbeamB()
        else: return None
        return RFBeam

    # assign the RF centre frequency
    def assignRFcentre(self,cenfreq):
        if not self.isSet_MyObservation():return False
        self.RFcentre=cenfreq
        if cenfreq==None:return None
        RFBeam=self.getRFBeam()
        self.log("RF centre frequency: "+str(self.RFcentre)+" MHz")
	RFBeam.SetCenterFreq(self.RFcentre)
        if not self.RFokay(): return None
        return True
                    

    # create an RF Beam... it doesn't really create the RF beam
    def makeRFBeam(self):
        if not self.isSet_MyObservation():return None
        self.log(isodate(dt.datetime.utcnow())+' : creating RF Beam')
        
        RFBeam=self.getRFBeam()
        if RFBeam==None:return None

        self.log("RF centre frequency: "+str(self.RFcentre)+" MHz")
        self.log("RF calibration frequency: "+str(self.RFcalfreq)+" MHz")
        if not self.RFokay(): return None
	RFBeam.SetCenterFreq(self.RFcentre)
        RFBeam.SetFcal(self.RFcalfreq)
        self.log(isodate(dt.datetime.utcnow())+' : RF Beam  ... DONE')
        return RFBeam

    # create a digital beam
    def makeDigitalBeam(self,freq=None,nbeamlets=None):
        if not self.isSet_MyObservation():return None
        self.log(isodate(dt.datetime.utcnow())+" : creating digital beam")

        # default number of beamlets is all the beamlets
        if nbeamlets==None:nbeamlets=self.nBeamlets()
        self.log('number of beamlets for this digital beam: '+str(nbeamlets))
        
        # bandwidth
        bandwidth=nbeamlets*self.chanwidth

        # default frequency of the digital beam
        # digital beams are centred at the RF calibration frequency
        if freq==None: freq=self.RFcalfreq 
        StartFreq=freq-0.5*bandwidth

        # get the RF beam from MyObservation
        RFBeam=self.getRFBeam()

        DigBeam=RFBeam.CreateDigitalBeam(StartFreq,bandwidth)
        #self.log('DEBUG: calling printFrequencyRange() from makeDigitalBeamL()')
        digbeam_index=self.ndigbeams
        if not self.printFrequencyRange(digbeam_index): return None
        
        # increment the total number of digital beams created
        self.ndigbeams+=1
        self.log(isodate(dt.datetime.utcnow())+' : digital beam ... DONE')
        return DigBeam # return the digital beam

    # print out to screen and logfile the frequency range 
    def printFrequencyRange(self,digbeam_index=0):
        if not self.isSet_MyObservation():return False
            
        # get the RF and Digital beams from MyObservation
        RFBeam=self.getRFBeam()
        DigBeam,codret=RFBeam.GetDigitalBeam(digbeam_index+1)
        if codret<0:
            self.log('ERROR: There is no digital beam '+str("#%02i" % digbeam_index)+' defined for this beam ')
            return False

        ssmap=DigBeam.GetSSmap()
        ssmap_list=ssmap.GetDescr()
        if ssmap_list==None:
            self.log("ERROR! no subband select map!")
            self.log("  RFcalfreq="+str(self.RFcalfreq))
            self.log("  RFcentre="+str(self.RFcentre))
            return False
        self.log("SSMAP\n"+str(ssmap_list))
        self.log("frequency band: ",)
        subband0=self.RFcentre-(self.chanwidth*256)
        beamlet=ssmap_list[0]
        subbandNumber=eval(beamlet.split(':')[1])
        freq_lower=subband0+subbandNumber*self.chanwidth
        beamlet=ssmap_list[len(ssmap_list)-1]
        subbandNumber=eval(beamlet.split(':')[1])
        freq_upper=subband0+subbandNumber*self.chanwidth
        self.log(str(freq_lower)+" to "+str(freq_upper)+" MHz")

        freqRange={}
        freqRange['freq_lower']=freq_lower
        freqRange['freq_upper']=freq_upper
        return freqRange

    # read the previously generated RF and digital beam pointing files
    # it's possible to read only the RF file, by giving None as the digFilename
    def readPointingFiles(self,rfFilename,digFilename,digbeam_index=None):
        if not self.isSet_MyObservation():return False
        if not os.path.exists(rfFilename):
            self.log("ERROR!  cannot find file: "+rfFilename)
            return False
        if digFilename!=None and not os.path.exists(digFilename):
            self.log("ERROR!  cannot find file: "+digFilename)
            return False                     

        # get the RF and Digital beams from MyObservation
        RFBeam=self.getRFBeam()

        # if no digbeam specified, run through all of them
        if digbeam_index==None:
            digbeam_index_list=[i+1 for i in range(RFBeam.GetTotalDigitalBeam())]
        else:
            digbeam_index_list=[digbeam_index]

        print digbeam_index_list
        for digbeam_index in digbeam_index_list:
            DigBeam,codret=RFBeam.GetDigitalBeam(digbeam_index)
            if codret<0:
                self.log('ERROR: There is no digital beam '+str("#%02i" % digbeam_index)+' defined for this beam')
                return False

            self.log("reading RF beam text file: "+rfFilename)
            RFBeam.AddSubscanFromFile(rfFilename)
            if digFilename!=None:
                self.log("reading digital beam text file: "+digFilename)
                DigBeam.AddSubscanFromFile(digFilename)

        return True

    # beam size of the fully beamformed array (smallest beamsize)
    # each hexboard has 2 x 6 antennas (on the 45 deg diagonal)
    # in the 8x8 EMBRACE array, there is not a line of Vivaldi exactly on the diagonal
    # width: 8.4m
    def stationBeamSize(self):
        wvln=self.speedolight/self.RFcentre
        return math.degrees(wvln/self.arrayWidth)

    def tilesetBeamSize(self):
        wvln=self.speedolight/self.RFcentre
        return math.degrees(wvln/(self.arrayWidth/4))

    def tileBeamSize(self):
        wvln=self.speedolight/self.RFcentre
        return math.degrees(wvln/(self.arrayWidth/8))
        

    # assign an observation title
    def ObsTitle(self):
	if self.source==None:
            comment='No source specified for Beam-'+self.beam\
                +': please select a source with the option --'+self.beam+'=<source name>'
            self.log(comment)
            return None
        
	ObsTitle='|'+self.beam+': '+self.source_fullname(self.source)+'|'
        return ObsTitle


    # initialize MyObservation
    def initialize(self,ObsTitle):
        if not self.isSet_MyObservation():return False
        self.myObs.ObsConfig.SetTitle(ObsTitle)
        ScanAId,ScanBId=self.myObs.configure()
        if(ScanAId <0 or ScanBId <0): 
            self.log("ScanAId,ScanBId: "+str(ScanAId)+" "+str(ScanBId),master=True)
            self.log("Observation init failed . Exit!!",master=True)
            return False
        return True


    # submit the observation, recording the times
    def submit(self):
        if not self.isSet_MyObservation():return False
        self.log("submit Observation "+isodate(dt.datetime.utcnow()),master=True)
        self.myObs.submit() 
        self.log("Observation Submitted "+isodate(dt.datetime.utcnow()),master=True)
        return True

    # method to read RF cal parameters for sending to LCU
    def readCalibration(self,calfile):
        self.satname=calfile
        

        if calfile=='DEFAULT':
            self.RFcalfreq=default_RFcalfreq[self.beam]
            self.calTable=calTable[self.beam]
            comment='using default RF calibration parameters'
        else: # read from file, it's a path that has already been verified in FindSat()
            # file format is a single line with output as given by CalParmsCapturer
            ifile=open(calfile,'r')
            line=ifile.readline()
            metadat=line.split()[0:3]
            self.RFcalfreq=eval(metadat[0])/1e6
            az=eval(metadat[1])
            el=eval(metadat[2])
            list_start=line.find('[')
            self.calTable=eval(line[list_start:])
            ifile.close()
            comment='using RF calibration parameters from file: '+calfile

        if self.RFcentre==None: self.RFcentre=self.RFcalfreq
        # verify that the chosen RF centre frequency is okay
        if not self.RFokay(): return None

        # no driftscan
        self.caldriftduration=dt.timedelta(minutes=0)

        # add FITS comment
        self.log(comment)
        self.addFITScomment(comment)
        self.log(str(self.calTable))
            
        return calfile

    # method to fill the Calibration Table in the RFBeam object
    # calibration takes time even though we're just filling a table with numbers
    # I don't know why.  anywayz, add 3 minutes
    # see observation logs for 2014-08-13,14
    def assignCalibration(self,obsdate):
        if not self.isSet_MyObservation():return None
        calSetupDelta=self.minimum_setupdelay

        # dictionary for return values
        satcalParms={}
        satcalParms['PointingStatus']=True
        satcalParms['satcalStart']=obsdate
        satcalParms['satcalEnd']=obsdate+calSetupDelta
        satcalParms['EndTime']=obsdate+calSetupDelta
        satcalParms['driftdate']=obsdate
        satcalParms['satname']='CalFromFile'

        RFBeam=self.makeRFBeam()
        
        RFBeam.SetCal('scu')
        RFBeam.BeamCal.CalTable.SetValues(self.calTable)

        # for the digital beams, the parameters are read from file on the LCU
        # Taff says tell the LCU to use "none" (2014-05-13)
        digbeam_index_list=[i+1 for i in range(RFBeam.GetTotalDigitalBeam())]
        for digbeam_index in digbeam_index_list:
            DigBeam,codret=RFBeam.GetDigitalBeam(digbeam_index)
            if codret<0:
                self.log('ERROR: There is no digital beam '+str("#%02i" % digbeam_index)+' defined for this beam')
                return None
            DigBeam.SetCal("none")
            self.digbeam_caltype="none"

        return satcalParms


    # ARTEMIS xml template for pulsar observing
    def PulsarXML(self,sourcename,RAJ,DecJ,nIterations,topFreq):
        if self.beam=='B':
            port="2001"
        else:
            port="2000"

        if sourcename=="Crab":
            mycommon="mycommon_crab.xml"
        elif sourcename=="B0950":
            mycommon="mycommon_B0950.xml"
        else:
            mycommon="mycommon.xml"

        secsPerIteration=128*16*5.12e-6
        duration_str=str('%.1f' % (secsPerIteration*nIterations/60.))+' mins'

        xml='<?xml version="1.0" encoding="UTF-8"?>\n'\
            +'<!DOCTYPE pelican>\n'\
            +'<configuration version="1.0">\n'\
            +'  <pipeline>\n'\
            +'    <clients>\n'\
            +'      <PelicanServerClient>\n'\
            +'        <server host="127.0.0.1" port="'+port+'"/>\n'\
            +'        <data type="LofarTimeStream1" adapter="AdapterTimeSeriesDataSet"/>\n'\
            +'      </PelicanServerClient>\n'\
            +'    </clients>\n'\
            +'    <adapters>\n'\
            +'      <AdapterTimeSeriesDataSet>\n'\
            +'        <subbandsPerPacket value="61"/> <!-- 31 or 61 or 62 -->\n'\
            +'        <import file="/home/artemis/EMBRACE/'+mycommon+'"/>\n'\
            +'        <!--   -->\n'\
            +'        <fixedSizePackets value="false" />\n'\
            +'      </AdapterTimeSeriesDataSet>\n'\
            +'    </adapters>\n'\
            +'    <pipelineConfig>\n'\
            +'         <EmbracePipeline>\n'\
            +'             <totalIterations value="'+str(nIterations)+'" /><!-- '+duration_str+' -->\n'\
            +'         </EmbracePipeline>\n'\
            +'    </pipelineConfig>\n'\
            +'    <modules>\n'\
            +'      <PPFChanneliser>\n'\
            +'        <import file="/home/artemis/EMBRACE/'+mycommon+'"/>\n'\
            +'        <!--   -->\n'\
            +'        <processingThreads value="4" />\n'\
            +'        <filter nTaps="8" filterWindow="kaiser"/>\n'\
            +'      </PPFChanneliser>\n'\
            +'      <StokesGenerator>\n'\
            +'      </StokesGenerator>\n'\
            +'\n'\
            +'      <RFI_Clipper active="false" channelRejectionRMS="6"\n'\
            +'                   spectrumRejectionRMS="6.0">\n'\
            +'        <zeroDMing active="false" />\n'\
            +'        <BandPassData file="/data/Commissioning/Useful/BandPass/flat.bp" />\n'\
            +'        <Band matching="true" />\n'\
            +'        <History maximum="10000" />\n'\
            +'      </RFI_Clipper>\n'\
            +'\n'\
            +'      <StokesIntegrator>\n'\
            +'        <import file="/home/artemis/EMBRACE/'+mycommon+'"/>\n'\
            +'      </StokesIntegrator>\n'\
            +'    </modules>\n'\
            +'\n'\
            +'    <output>\n'\
            +'\n'\
            +'      <dataStreams>\n'\
            +'        <stream name="SpectrumDataSetStokes" listeners="EmbraceFBWriter"/>\n'\
            +'      </dataStreams>\n'\
            +'\n'\
            +'      <streamers>\n'\
            +'        <EmbraceFBWriter active="true" writeHeader="true">\n'\
            +'	  <import file="/home/artemis/EMBRACE/'+mycommon+'"/>\n'\
            +'          <dataBits value="32"/>\n'\
            +'          <scale max="1000" min="0"/>\n'\
            +'          <topSubbandIndex value="286"/>\n'\
            +'          <LBA_0_or_HBA_1 value="1" />\n'\
            +'          <fch1 value="'+str('%.7f' % topFreq)+'" />\n'\
            +'          <subbandsPerPacket value="61"/>\n'\
            +'          <file filepath="/Data/incoming/'+sourcename+'_'+self.beam+'" />\n'\
            +'          <params telescope="EMBRACE" nPolsToWrite="1"/>\n'\
            +'          <RAJX value="'+RAJ+'" />\n'\
            +'          <DecJX value="'+DecJ+'"/>\n'\
            +'          <RAJY value="'+RAJ+'" />\n'\
            +'          <DecJY value="'+DecJ+'"/>\n'\
            +'          <TelescopeID value="3"/>\n'\
            +'          <MachineID value="9"/>\n'\
            +'        </EmbraceFBWriter>\n'\
            +'      </streamers>\n'\
            +'      \n'\
            +'    </output>\n'\
            +'\n'\
            +'  </pipeline>\n'\
            +'</configuration>\n'

        return xml


    # method to write an xml file for use by ARTEMIS on Andante
    # we need the frequency end, the integration time,
    def mkPulsarXML(self,source,duration):
        if source.upper().find('B0329')>=0:
            RAJ="033259.3"
            DecJ="543443.5"
            sourcename="B0329"
        elif source.upper().find('B1133')>=0:
            RAJ="113603.2"
            DecJ="155104.4"
            sourcename="B1133"
        elif source.upper().find('CRAB')>=0:
            RAJ="053432.0"
            DecJ="220052.1"
            sourcename="Crab"
        elif source.upper().find('B0950')>=0:
            RAJ="095309.3"
            DecJ="075535.8"
            sourcename="B0950"
        else:
            self.log('ERROR: unknown pulsar')
            return None

        # number of iterations
        # 1 chunk = 128 packets
        # 1 packet = 16 time slices
        # 1 time slice = 5.12 usec
        secsPerIteration=128*16*5.12e-6
        int_secs=tot_seconds(duration)
        nIterations=int(math.floor(int_secs / secsPerIteration))


        # top frequency subband
        # self.log('DEBUG: calling printFrequencyRange() from mkPulsarXML()')
        freqRange=self.printFrequencyRange()
        topFreq=freqRange['freq_upper']

        xml=self.PulsarXML(sourcename,RAJ,DecJ,nIterations,topFreq)

        xmlfilename='Beam'+self.beam+'_'+sourcename+'_1.xml'
        fhandle=open(xmlfilename,'w')
        fhandle.write(xml)
        fhandle.close()

        return xmlfilename

    # method to generate the shell script for running ARTEMIS
    def mkRunArtemis(self,xmlfilename,source=None):
        if self.beam=='B':
            cores="4,5,6,7"
            servername="serverEmbrace2"
            configfile='BeamB_Embrace.xml'
        else:
            cores="0,1,2,3"
            servername="serverEmbrace"
            configfile='BeamA_Embrace.xml'
        
        if source==None:
            configfile='Beam'+self.beam+'_Embrace.xml'
        else:
            configfile='Beam'+self.beam+'_'+source.replace(' ','').replace('PSR','')+'_Embrace.xml'
        

        if source==None:
            runfilename='runARTEMIS_Beam'+self.beam+'_'+self.source.replace(' ','')+'.sh'
        else:
            runfilename='runARTEMIS_Beam'+self.beam+'_'+source.replace(' ','')+'.sh'

        shellcmds='#/bin/sh\n'\
            +'# $Id: '+runfilename+'\n'\
            +'# $created: '+isodate(dt.datetime.utcnow())+'\n'\
            +'# $auth: '+sys.argv[0]+' on '+socket.gethostname()+'\n'\
            +'# Automatically generated.  Do not edit.  It will be overwritten.\n'\
            +"# this script is invoked by the 'at' command.  try 'at -c <job number>' for details\n"\
            +'cd /home/artemis/EMBRACE\n'\
            +'schedtool -a '+cores+' -e embraceBFPipelineStream1 --config='+xmlfilename+' &\n'\
            +'sleep 3\n'\
            +servername+' --config='+configfile+' &\n'\
            +'# '+runfilename+'\n'

    
        fhandle=open(runfilename,'w')
        fhandle.write(shellcmds)
        fhandle.close()

        return runfilename

    # define backend commands for pulsar observing
    def mkBackendCommands(self,startTime,endTime):
        backend_cmd=[]
        source_fullname=self.srcParms['srcEph'].name
        if not source_fullname[0:3]=='PSR':return None
        
	cmd="ssh __BACKEND__ '/home/artemis/remote_cmd at "\
	     +(startTime-dt.timedelta(minutes=2)).strftime('%H:%M %Y-%m-%d')\
	     +" -f /home/artemis/EMBRACE/killobs_Beam"+self.beam+".sh'"
	backend_cmd.append(cmd)

	xmlfilename=self.mkPulsarXML(source_fullname,endTime-startTime)
	cmd="scp -p "+xmlfilename+" __BACKEND__:EMBRACE"
	backend_cmd.append(cmd)


        runfilename=self.mkRunArtemis(xmlfilename,source_fullname)
	cmd="scp -p "+runfilename+" __BACKEND__:EMBRACE"
	backend_cmd.append(cmd)

	cmd="ssh __BACKEND__ '/home/artemis/remote_cmd at "\
	     +startTime.strftime('%H:%M %Y-%m-%d')\
	     +" -f /home/artemis/EMBRACE/"+runfilename+"'"
	backend_cmd.append(cmd)

	# kill acquisition after the observation is finished
        # this is necessary otherwise Andante keeps writing to disk
	cmd="ssh __BACKEND__ '/home/artemis/remote_cmd at "\
	     +endTime.strftime('%H:%M %Y-%m-%d')\
	     +" -f /home/artemis/EMBRACE/killobs_Beam"+self.beam+".sh'"
	backend_cmd.append(cmd)
        self.backend_cmd=backend_cmd
        return backend_cmd
            
    
    # submit jobs to backend (Andante, Borsen,...)
    def submitAtBackend(self,cmd_list=None,backend='andante'):
        from SCU.PackageTools.ModuleShareObjects import LCUconnect__var
        if not self.srcParms['srcEph'].name[0:3]=='PSR': return (None,None)

        if cmd_list==None: cmd_list=self.backend_cmd
        
        self.log('sending commands to '+backend+':')
        (out,err)=(None,None)

        ##### for testing ############################################
        ### backend='borsen'
        ### LCUconnect__var=True
        ##############################################################

        # force command list to be a list, in case only one command is submitted
        # otherwise the "for" loop will go through letter by letter
        if type(cmd_list)==str: cmd_list=[cmd_list]

	for generic_cmd in cmd_list:
            cmd=generic_cmd.replace('__BACKEND__',backend)
            self.log(cmd)
            if LCUconnect__var:
                # split off the command for the at queue, and the command to submit it...
                cmd_parts=cmd.split("'")
                submit_cmd=cmd_parts[0].split()
                if len(cmd_parts)>1:
                    submit_cmd.append(cmd_parts[1])
                submitproc=subprocess.Popen(submit_cmd,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                (out,err)=submitproc.communicate()
                if err!=None:
                    for line in err.split('\n'):
                        if line.find('warning')==0 or line.find('Warning')==0:
                            print(line) # show warning without logging it
                        elif line!='':
                            self.log(line)
                if out!=None:
                    for line in out.split('\n'):
                        if line!='': self.log(out)

        return (out,err)

    def teststuff(self):
        from SCU.PackageTools.ModuleShareObjects\
            import LCUip__var,LCUconnect__var
        try:
            print 'TEST: LCUconnect__var=',LCUconnect__var
        except:
            print 'TEST: LCUconnect__var is undefined'

        try:
            print 'TEST: LCUip__var=',LCUip__var
        except:
            print 'TEST: LCUip__var is undefined'

        return
