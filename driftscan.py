#! /usr/bin/env python
"""
$Id: driftscan.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Wed 12 Jun 2013 14:41:06 CEST

started from template:  SourceTrace_AZEL.py (last mod: Wed Jun 12 10:29:27 CEST 2013)

very long drift scan to test stability

$modified: Thu 04 Jul 2013 15:15:16 CEST
  debugging endless loop which occurs because of next_pass bug in pyephem
  see https://bugs.launchpad.net/pyephem/+bug/572402

$modified: Fri 09 Aug 2013 18:16:01 CEST
  fixed bug:  specifying start date on the command line works

$modified: Mon 12 Aug 2013 12:16:01 CEST
  fixed bug: driftscan was always pointing at transit position, even for driftscan which doesn't cover transit time
  
$modified: Thu 05 Sep 2013 23:24:31 CEST
  obsfunc.source renamed to obsfunc.sourcecat, and no need to initialize here

$modified: Tue 15 Oct 2013 13:57:44 CEST
  RF beam creation is done in satorchi_OBSinit.py
  
$modified: Mon 23 Dec 2013 08:20:45 CET
  updated to conform to changes in ObsFunctions.  Separate obsfunc object for each beam.

$modified: Wed 16 Apr 2014 12:57:31 CEST
  yoffset for cal drift scan is passed via obsfunc for each beam.

$modified: Mon 11 Aug 2014 16:37:40 CEST
  compatibility with using calibration parameters from file
  removing calls to SetCal().  This is done during the calibration segment
  calling SetCal() here interferes with using cal parameters from file

$modified: Mon 18 Aug 2014 14:50:22 CEST
  cleaning up, and looking for bug for proper behaviour with drift scan of satellites

$modified: Thu 27 Aug 2015 07:28:20 CEST
  bug fix: peak time of off position

$modified: Mon 02 Jan 2017 08:42:20 CET
  added pulsar observing

$modified: Sun 08 Jan 2017 18:46:22 CET
  clean up for pulsar commands (changes to ObsFunctions)
  
"""
helptxt='drift scan of an astronomical source'
import sys,os,math
import datetime as dt
import ephem as eph

# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================
	
# === one Observation title for both beams ============================
# source name given on the command line, there is no default
ObsTitle=''
for beam in beams:
	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	comment="cal on "+obsfunc[beam].satname+", and drift scan "+ObsTitle
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc[beam].initialize(ObsTitle):quit()
# =====================================================================

beam_driftdate=None
src_peaktime=None
backend_cmd=[]
for beam in beams:
	obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
	AcqStarts=[]
	AcqEnds=[]
	# ======== get RFBeam and DigBeam objects ======================
	RFBeam=obsfunc[beam].makeRFBeam()
	if RFBeam==None:quit()
	DigBeam=obsfunc[beam].makeDigitalBeam()
	if DigBeam==None: quit()
	# ==============================================================

	satcalParms=obsfunc[beam].satcal(digcal=digcal,
					 driftscan=do_satdrift,
					 driftdate=beam_driftdate,
					 now=now)
	if satcalParms==None: quit()
	if not satcalParms['PointingStatus']:
		obsfunc[beam].log("\nCould not complete the satellite calibration sequence")
		quit()
	obstag.append(satcalParms['satname'].replace(' ',''))
	beam_caldate=satcalParms['satcalEnd']
	beam_driftdate=satcalParms['driftdate']
	calphaseEnd  =satcalParms['satcalEnd']+obsfunc[beam].caldriftduration
	satcalStart=satcalParms['satcalStart']

	# but one minute earlier
	AcqStarts.append(satcalStart-dt.timedelta(minutes=1))

	comment='calibration phase ends at '+isodate(calphaseEnd)
	obsfunc[beam].log(comment)

# ========= astronomical source pointing =======
	if obsfunc[beam].obsdate==None:
		StartTime=calphaseEnd	
		obstag[0]+='--'+obsfunc[beam].source_fullname(source)
	else:
		if obsfunc[beam].obsdate > calphaseEnd:
			obsfunc[beam].log('using given start time: '+isodate(obsfunc[beam].obsdate))
			StartTime=obsfunc[beam].obsdate
			obstag.append(obsfunc[beam].source_fullname(source))
			AcqEnds.append(calphaseEnd)
			AcqStarts.append(StartTime)
		else:
			StartTime=calphaseEnd
			obstag[0]+='--'+obsfunc[beam].source_fullname(source)

	# duration of the observation, if none given, default 24 hours
	if obsfunc[beam].duration==None: obsfunc[beam].duration=dt.timedelta(minutes=24*60.)
	endobs=StartTime+obsfunc[beam].duration
	AcqEnds.append(endobs)


	# find pointing for next transit
	srcParms=obsfunc[beam].verifySource(obsfunc[beam].source,StartTime,obsfunc[beam].offsource)
	if srcParms==None: quit()

	srcEph=srcParms['srcEph']
	offEph=srcParms['offEph']
	isAstroSrc=srcParms['isAstroSrc']

	# rise, set, and transit times
	src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,isAstroSrc,StartTime)
	if src_transit==None:quit()

	src_peaktime=src_transit
	transit_obs=True
	if src_transit > endobs:
		comment='requested duration is not long enough to point at transit\n'\
		    +'pointing for driftscan at mid-duration'
		obsfunc[beam].log(comment)
		src_peaktime=StartTime+obsfunc[beam].duration/2
		transit_obs=False


	# azel at peak time
	obsfunc[beam].embraceNancay.date=src_peaktime
	location=obsfunc[beam].currentLocation(srcEph)
	src_az=location['az']
	src_alt=location['alt']
	src_ra=location['RA']
	src_dec=location['dec']

	CmdTime=StartTime
	comment='pointing by AZEL beginning at '+isodate(CmdTime)
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
	comment='  az='+str(src_az)+' degrees, alt='+str(src_alt)+' degrees'
	obsfunc[beam].log(comment)
	comment="! peak should be at "+isodate(src_peaktime)
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)

        ## indicate further transits (NOTE: this only works if pointing at meridian transit!)
	next_transit=src_transit
	next_set=src_set
	next_rise=src_rise
	while transit_obs and next_transit < endobs:
		comment='! peak should be at '+isodate(next_transit)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)
		date=next_rise+obsfunc[beam].onesid
		next_rise,next_transit,next_set=obsfunc[beam].next_transit(srcEph,isAstroSrc,date)

		# hack to get around pyephem bug on next_pass : see https://bugs.launchpad.net/pyephem/+bug/572402
		if next_transit < date: break


	# RF beam tracking
	RFBeam.AddSubscan(CmdTime,'AZEL',src_az,src_alt)

	# indicate further transits
	if transit_obs:
		off_rise,off_transit,off_set=obsfunc[beam].next_transit(offEph,isAstroSrc,StartTime)
		next_transit=off_transit
		next_rise=off_rise
                # azel at off-pointing peak time
                obsfunc[beam].embraceNancay.date=off_transit
                offlocation=obsfunc[beam].currentLocation(srcEph)
	else: # off position defined a few time minutes later
		off_peaktime=src_peaktime+dt.timedelta(minutes=obsfunc[beam].drift_yoffset)
		comment='! the y-direction should have a peak at '+isodate(off_peaktime)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)
		obsfunc[beam].embraceNancay.date=off_peaktime
		offlocation=obsfunc[beam].currentLocation(srcEph)
        
	while transit_obs and next_transit < endobs:
		comment='! the y-direction should have a peak at '+isodate(next_transit)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)

		date=next_rise+obsfunc[beam].onesid
		next_rise,next_transit,next_set=obsfunc[beam].next_transit(offEph,isAstroSrc,date)

		obsfunc[beam].log("\n"+offEph.name+" rises at "+isodate(next_rise))
		obsfunc[beam].log(offEph.name+" transits at "+isodate(next_transit))
		obsfunc[beam].log(offEph.name+" sets at "+isodate(next_set))
		# hack to get around pyephem bug on next_pass : see https://bugs.launchpad.net/pyephem/+bug/572402
		if next_transit < date: break



	off_az=offlocation['az']
	off_alt=offlocation['alt']
        comment='off beam pointing:  az='+str(off_az)+' degrees, alt='+str(off_alt)+' degrees'
	obsfunc[beam].log(comment)

	if digtrack:
		comment=" with normal digital beam pointing"
		DigBeam.AddSubscan(CmdTime,'AZEL',src_az,src_alt,off_az,off_alt)
	else:
		comment="*** No digital beam pointing ***"
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# === end pointing configuration ==================

# === Define  STATISTICS ACQUISITION phases ============================
	if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
		print "\nObservation NOT submitted\n"
		quit()
# ======================================================================

# === Define commands for pulsar observing on Andante or Borsen
        psrAcqStart=AcqStarts[-1]+dt.timedelta(minutes=2)
	psrAcqEnd=AcqEnds[-1]-dt.timedelta(minutes=2)

        obsfunc[beam].mkBackendCommands(psrAcqStart,psrAcqEnd)

# === finally, submit the observation for both beams ===========
submit_EMBRACE_observation(obsfunc)


