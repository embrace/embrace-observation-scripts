#! /usr/bin/env python
"""
$Id: SourceTrack_AZEL.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Wed Dec  7 16:21:54 CET 2011
  originally from CasA_tracking.py

Track a source after an initial RF and digital calibration on satellite
The source can be given as a command line argument (default: B0329+54)
The off-source beam can be given as an command line argument (default: None)
The calibrating source (satellite) can be given as a command line argument
  default: GPS BIIF-1

Tracking is done by AZEL computed by PyEphem

command line arguments are processed in satorchi_OBSinit.py

$modified: Wed Dec  7 22:32:16 CET 2011
  new optional command line arguments:
   caldate: when to start satellite calibration
   obsdate: now refers to time to start astro observation

$modified: Fri Dec  9 12:24:38 CET 2011
  renamed from SourceTrack.py to SourceTrack_AZEL.py
  now that J2000 is working, we probably don't need this.

$modified: Tue Apr 24 17:54:38 CEST 2012
  this is still good for the Sun and planets

  duration can now be given on the command line

$modified: Mon May 14 15:53:52 CEST 2012
  option to track with RF beam only.  digbeam tracking updated only when really outside the beam
  (see ObsFunctions.py)

$modified: Wed May 23 12:24:21 CEST 2012
 digital calibration is an option given on the command line.  default True.

$modified: Tue Jul  3 10:40:22 CEST 2012
 statsTypes is given on the command line.  default ssx,bsx

$modified: Tue 24 Jul 2012 17:03:38 CEST
 digital beams are centred at the RF calibration frequency
 instead of the RF centre frequency

$modified: Wed 25 Jul 2012 11:55:29 CEST
  using obsfunc.makeDigitalBeam()

$modified: Thu 23 Aug 2012 12:24:06 CEST
  using obsfunc.makeRFBeam()

$modified: Fri 24 Aug 2012 18:27:05 CEST
  temporary change.  remember to change back!
  satellite drift scan for 30 mins

$modified: Wed 21 Nov 2012 14:16:33 CET
  initialization changed to work for anyone

$modified: Tue 27 Nov 2012 15:44:30 CET
  allowing selection of beam-A or beam-B on the command line
  use argument:  --beams='A,B' or --beams='A' or --beams='B'
  this probably won't work for --beams=A,B :  
    I have to add time between RF cals (see eg. satdrift_AB.py)

$modified: Mon 21 Jan 2013 15:36:22 CET
  update for new version of satcal()

$modified: Tue 22 Jan 2013 09:19:17 CET
  removed rfcaldelta etc, which is done in satcal() and returned in satcalParms

$modified: Fri 25 Jan 2013 16:56:01 CET
  acquisition gets source_fullname() so we don't use the nick for the
  filename

$modified: Fri 15 Feb 2013 14:23:41 CET
  using FullPathname() to find the init file

$modified: Thu 21 Feb 2013 18:30:25 CET
  added help text

$modified: Fri 08 Mar 2013 10:10:28 CET
  do not submit if cal sequence not completed because of cal source visibility

$modified: Tue 11 Jun 2013 16:15:35 CEST
  removed calculation of bandwidth, which is already done as part of the 
  argument parsing (see satorchi_OBSinit.py, and ObsFunctions.py)
  
$modified: Wed 12 Jun 2013 10:27:27 CEST
  added driftduration option.  duration of drift scan during calibration phase

$modified: Mon 08 Jul 2013 14:13:15 CEST
  removed variable "driftduration" which originally had the command line argument value from --driftduration=...
  the default is defined in ObsFunctions.py in variable caldriftduration

$modified: Mon 26 Aug 2013 12:40:04 CEST
  str2dt() and tot_seconds() are in datefunctions.py
  no longer a method of class ObsFunctions

$modified: Tue 01 Oct 2013 13:59:57 CEST
  RFbeams created in satorchi_OBSinit.py

$modified: Tue 19 Nov 2013 12:30:09 CET
  modifications for changes to ObsFunctions and satorchi_OBSinit
  Beam-A and Beam-B each have its own obsfunc

$modified: Wed 16 Apr 2014 12:55:32 CEST
  yoffset for cal drift scan is passed via obsfunc for each beam.

$modified: Fri 13 Mar 2015 08:17:10 CET
  modifications for using saved cal tables (assignment of satname)

$modified: Sat 07 Jan 2017 00:31:47 CET
  adding pulsar backend commands

$modified: Sun 08 Jan 2017 18:40:07 CET
  clean up for pulsar commands (changes to ObsFunctions)

"""
helptxt='Track by AZ-EL a source after an initial RF and digital calibration on a calibration source'
import sys,os
import datetime as dt
import ephem as eph
from datefunctions import *
# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================
			
# === Observation title is applied for both beams =====================
# source name given on the command line, there is no default
ObsTitle=''
for beam in beams:
	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	comment="cal on "+obsfunc[beam].satname+", and tracking "+ObsTitle
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc[beam].initialize(ObsTitle):quit()
# =====================================================================


# ===== configure each RF Beam ========================================
backend_cmd=[]
FirstAcquisition=None
satcalParms={}
for beam in beams:
	# ======== get RFBeam and DigBeam objects ======================
	RFBeam=obsfunc[beam].makeRFBeam()
	if RFBeam==None:quit()
	DigBeam=obsfunc[beam].makeDigitalBeam()
	if DigBeam==None: quit()
	# ==============================================================

	# reinitialize obstag, AcqStarts and AcqEnds for each beam
	obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
	AcqStarts=[]
	AcqEnds=[]

	# phase calibration generate the pointings for satellite calibration with driftscan
	# satcal also chooses the frequencies, if not specifically given
	# note that obsfunc.FindSat() must have been previously called to set the satname
	satcalParms=obsfunc[beam].satcal(digcal=digcal,
					 driftscan=do_satdrift,
					 now=now)
	if satcalParms==None: quit()
	if not satcalParms['PointingStatus']:
		obsfunc[beam].log("\nCould not complete the satellite calibration sequence")
		quit()

	obstag.append(satcalParms['satname'].replace(' ',''))
	satcalStart=satcalParms['satcalStart']
	satcalEnd=satcalParms['EndTime']

	# calibration and drift data acquisition times
	if FirstAcquisition==None:FirstAcquisition=satcalStart-dt.timedelta(minutes=1)
	AcqStarts.append(satcalStart-dt.timedelta(minutes=1))
	AcqEnds.append(satcalEnd)
# ===== end phase calibration ==========================================



# ========= source pointing ============================================
	StartTime=satcalEnd
        source_fullname=obsfunc[beam].source_fullname(obsfunc[beam].source)
	if obsfunc[beam].obsdate!=None and obsfunc[beam].obsdate>satcalEnd:
		StartTime=obsfunc[beam].obsdate

	if not digtrack:
		obsfunc[beam].log("no digital beam tracking")

	StartTime,endTracking=obsfunc[beam].SourceTrack(StartTime,
							calibration_interval=None,
							digtrack=digtrack)
	if StartTime==None:quit()


	trackingStartTime=StartTime

	# acquisition begins using StartTime returned by SourceTrack()

	# duration of tracking.  Default None=while visible
	# if duration given on the command line, 
	# it is converted to a timedelta by the parser in satorchi_OBSinit.py
	if obsfunc[beam].duration==None:
		endobs=endTracking
	else:
		endobs=StartTime+obsfunc[beam].duration

	# acquisition times
	# if the main observation follows directly from the calibration
	# make a single acquisition phase for both
	if StartTime-satcalEnd < dt.timedelta(minutes=30):
		AcqEnds[0]=endobs
		obstag[0]+="--"+source_fullname
	else:
		AcqStarts.append(StartTime)
		AcqEnds.append(endobs)
		obstag.append(source_fullname)
# === end pointing configuration ==================

# === Define  STATISTICS ACQUISITION phases ============================
	# align the acquistion times of the two beams, if they are within a few minutes of one another
	delta=AcqStarts[0] - FirstAcquisition
	if delta>dt.timedelta(minutes=0) and delta<dt.timedelta(minutes=45): AcqStarts[0]=FirstAcquisition
	if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
		print "\nObservation NOT submitted\n"
		quit()
# ======================================================================
# === Define commands for pulsar observing on Andante or Borsen

        psrAcqStart=trackingStartTime+dt.timedelta(minutes=2)
	psrAcqEnd=endobs-dt.timedelta(minutes=2)
        obsfunc[beam].mkBackendCommands(psrAcqStart,psrAcqEnd)

# === finally, submit the observation for both beams ===========
submit_EMBRACE_observation(obsfunc)

