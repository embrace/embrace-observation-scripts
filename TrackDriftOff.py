#!/usr/bin/env python
"""
$Id: TrackDriftOff.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Fri 13 Mar 2015 08:39:42 CET
$license: GPLv3 or later, see https://www.gnu.org/licenses/gpl-3.0.txt

copied from SourceTrack_AZEL.py (last mod: Fri 13 Mar 08:18:13 CET 2015)

Track by AZEL, do a drift scan on the source, and then go to an RF off position

"""
helptxt="Track by AZEL, do a drift scan on the source, and then go to an RF off position"

import sys,os
import datetime as dt
import ephem as eph
from datefunctions import *
# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================
			
# === Observation title is applied for both beams =====================
# source name given on the command line, there is no default
ObsTitle=''
for beam in beams:
	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	comment="cal on "+obsfunc[beam].satname+", and tracking "+ObsTitle
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc[beam].initialize(ObsTitle):quit()
# =====================================================================


# ===== configure each RF Beam ========================================
FirstAcquisition=None
satcalParms={}
for beam in beams:
	# ======== get RFBeam and DigBeam objects ======================
	RFBeam=obsfunc[beam].makeRFBeam()
	if RFBeam==None:quit()
	DigBeam=obsfunc[beam].makeDigitalBeam()
	if DigBeam==None: quit()
	# ==============================================================

	# reinitialize obstag, AcqStarts and AcqEnds for each beam
	obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
	AcqStarts=[]
	AcqEnds=[]

	# phase calibration generate the pointings for satellite calibration with driftscan
	# satcal also chooses the frequencies, if not specifically given
	# note that obsfunc.FindSat() must have been previously called to set the satname
	satcalParms=obsfunc[beam].satcal(digcal=digcal,
					 driftscan=do_satdrift,
					 now=now)
	if satcalParms==None: quit()
	if not satcalParms['PointingStatus']:
		obsfunc[beam].log("\nCould not complete the satellite calibration sequence")
		quit()

	obstag.append(satcalParms['satname'].replace(' ',''))
	satcalStart=satcalParms['satcalStart']
	satcalEnd=satcalParms['EndTime']

	# calibration and drift data acquisition times
	if FirstAcquisition==None:FirstAcquisition=satcalStart-dt.timedelta(minutes=1)
	AcqStarts.append(satcalStart-dt.timedelta(minutes=1))
	AcqEnds.append(satcalEnd)
# ===== end phase calibration ==========================================



# ========= source tracking ============================================
	StartTime=satcalEnd
	if obsfunc[beam].obsdate!=None and obsfunc[beam].obsdate>satcalEnd:
		StartTime=obsfunc[beam].obsdate

	if not digtrack:
		obsfunc[beam].log("no digital beam tracking")

	StartTime,endTracking=obsfunc[beam].SourceTrack(StartTime,
							calibration_interval=None)
	if StartTime==None:quit()


	# acquisition begins using StartTime returned by SourceTrack()

	# duration of tracking.  Default None=while visible
	# if duration given on the command line, 
	# it is converted to a timedelta by the parser in satorchi_OBSinit.py
	if obsfunc[beam].duration==None:
		endobs=endTracking
	else:
		endobs=StartTime+obsfunc[beam].duration

	# acquisition times
	# if the main observation follows directly from the calibration
	# make a single acquisition phase for both
	if StartTime-satcalEnd < dt.timedelta(minutes=30):
		AcqEnds[0]=endobs
		obstag[0]+="--"+obsfunc[beam].source_fullname(obsfunc[beam].source)
	else:
		AcqStarts.append(StartTime)
		AcqEnds.append(endobs)
		obstag.append(obsfunc[beam].source_fullname(obsfunc[beam].source))
# === end tracking configuration =======================================

# === do a drift scan on the same source ===============================
        StartTime=endobs
        obsfunc[beam].duration=dt.timedelta(minutes=30)
        driftparms=obsfunc[beam].driftscan(StartTime=endobs,transit=False,digbeam=0,now=True)
        endobs=driftparms['enddrift']       
# === end drift scan ===================================================

# === go to an OFF position for a while ================================
        CmdTime=endobs
        az=driftparms['az']+180 # swing around in azimuth
        if az>360:az-=360
        alt=driftparms['alt']
        
        off_az=driftparms['off az']+180 # swing around in azimuth
        if off_az>360:off_az-=360
        off_alt=driftparms['off alt']
        DigBeam.AddSubscan(CmdTime,'AZEL',az,alt,off_az,off_alt)
        comment='RF OFF position starting at '+isodate(CmdTime)
        obsfunc[beam].log(comment)
        obsfunc[beam].addFITScomment(comment)
        endobs=CmdTime+dt.timedelta(minutes=20)
# === end OFF position =================================================


# === Define  STATISTICS ACQUISITION phases ============================
	# align the acquistion times of the two beams, if they are within a few minutes of one another
	delta=AcqStarts[0] - FirstAcquisition
	if delta>dt.timedelta(minutes=0) and delta<dt.timedelta(minutes=45): AcqStarts[0]=FirstAcquisition
        # assign the end of observation time
        AcqEnds[-1]=endobs
	if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
		print "\nObservation NOT submitted\n"
		quit()
# ======================================================================

# === finally, submit the observation ===========
obsfunc[beam].submit()

