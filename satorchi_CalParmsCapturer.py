#! /usr/bin/env python
"""
$Id: satorchi_CalParmsCapturer.py
$original-Id: ReadPointings.py by Patrice Renaud
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Thu Oct 27 14:47:14 CEST 2011
  copied from ReadPointings.py by Patrice Renaud
  original path: /home/scuadmin/software/SCU-develop/SCU/Tests/Observation/ReadPointings.py
  last modification date was: Wed Sep  7 14:55:26 CEST 2011  

DESCRIPTION:
record the phase parameters used for pointing

$modified: Thu 10 Jan 2013 14:07:31 CET
  allow for use with Beam-B by command line argument (default, Beam-A)
  usage:  python satorchi_CalParmsCapturer.py --beam=B

$modified: Fri 15 Feb 2013 11:43:38 CET
  using FullPathname() to make this usable by any user

$modified: Tue 18 Mar 2014 10:52:15 CET
  eliminating call to SCUconf.py

$modified: Tue 29 Apr 2014 16:35:28 CEST
  fixing bug introduced when SCUconf.py was eliminated

"""
import os,sys
import datetime as dt
from ObsFunctions import FullPathname

interruptText="\nHit 'ctl-alt-space' key to stop reading statistics"
interruptKey='\x80'
timeFmt="%Y-%m-%d %H:%M:%S"

# check if there are only zeros in the table
def isZero_table(table):
    for val in table:
        if val!=0: return False
    return True

# import SCU things
from SCU.PackageFiles.ModuleLogFiles\
    import TLogBeamPointing,TLogCmd
from SCU.PackageTools import ModuleShareObjects
from SCU.PackageTools.ModuleShareObjects\
    import SCUlogfile__obj
ModuleShareObjects.SCUlogfile__obj=TLogCmd()
if ModuleShareObjects.SCUlogfile__obj==None:
    print "ERROR:  SCUlogfile__obj is not defined"
    quit()
from SCU.PackageObservation.ModuleSCANClient\
    import TSCANClient
from SCU.PackageTools.ModuleTools\
    import WaitUntilTime,IsTimeArrived,PrintOver,StartTimer,KeyPressed,KeyInit,KeyEnd

# Connect to the LCU?
#   True if connect to LCU
#   False for no LCU connection (LCU not available, no LCU, etc for testing)
# the following script sets the LCUconnect__var, 
initfile=FullPathname('ConnectLCU.py')
if initfile==None:quit()
execfile(initfile)

# Max RSP boards per backend
try:
    if isinstance(TotalRSPboards,int):
        print "overriding default number of RSPboards: ",TotalRSPboards
except:
    TotalRSPboards=2

# select tilesets (16 tilesets connected since 1 july 2011)
tilesets='0:15'

# parse command line arguments
beam=None
for arg in sys.argv:
    if arg==sys.argv[0]:continue

    if arg.find("--beam=")==0:
        beam=arg.split('=')[1].upper()
        continue

# assign Beam
if beam==None: beam='A'
if beam=='A':
    beam_index=0
else:
    beam='B'
    beam_index=1

print 'Calibration parameters to be recorded for Beam-'+beam
    

SCAN=TSCANClient(TotalRSPboards,
                 backend_number=beam_index,
                 lcuconnect=LCUconnect__var,
                 lcuip=LCUip__var) #Virtual access to LCU
if(not(LCUconnect__var)):
    print 'Test: No connection to LCU'
if(SCAN.connected == False):   
    print '- Unable to Connect to LCU'
    sys.exit()
print "Total RSPboards: ", TotalRSPboards

print 'Subscribe to RFBeam pointings'
status=SCAN.SubscribeRFBeamPointings()
print 'Answer:'
answer=SCAN.GetAnswer()  
if(answer <> None):
    answer_hex= answer.GetMessageAsHex()
    print answer_hex
if(status !=0):
    SCAN.UnSubscribeRFBeamPointings()
    print 'Error subscribe RFBeam. Status= ',status
    sys.exit()

#open log file to store beam updates
LogFile=TLogBeamPointing()

#Read updates sent by LCU
RFcalParmsFile='RFcalibrationParameters_Beam-'+beam+'.txt'
KeyInit()
while not KeyPressed(interruptKey):
    print "\n*** Reading LCU Updates ***"
    print interruptText
    UpdateAnswer=SCAN.UpdateBeamPointings()
    print "Update time:", dt.datetime.now()
    BeamSteering=UpdateAnswer.GetBeamSteering()
    CalType=BeamSteering.BeamCal.GetType()
    BeamTimestamp=BeamSteering.GetTimeStamp()
    print "LCU BeamTimeStamp:", BeamTimestamp
    print "BeamSteering config:\n",BeamSteering.GetConfigAsDesc()
    print "Cal type: ",CalType
    CalTable=UpdateAnswer.GetCalTable()
    print "CalTable",CalTable.table
    print 

    angle0_dir1=BeamSteering.GetAngle0(1)
    angle1_dir1=BeamSteering.GetAngle1(1)
    freq=BeamSteering.GetFreq()

    # log the parameters if we're doing an RF calibration
    if CalType=='ssx' and not isZero_table(CalTable.table): 
        # using Patrice Renaud's log file format
        LogFile.add(BeamSteering)
        # using my log file format
        # freq angle1 angle2 cal-table=nrcu x 4 values (currently nrcu=16)
        ofile=open(RFcalParmsFile,'a')
        ofile.write(str("%10.0f " % freq)\
                        +str("%7.5f " % angle0_dir1)\
                        +str("%7.5f " % angle1_dir1)\
                        +str(CalTable.table)\
                        +'\n')
        ofile.close()
	    
print "Unsubscribe Update"	
SCAN.UnSubscribeRFBeamPointings()



