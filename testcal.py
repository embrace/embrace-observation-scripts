"""
$Id: testcal.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Mon 07 Apr 2014 13:00:53 CEST

test digcal() method in ObsFunctions

$modified: Tue 08 Apr 2014 10:29:24 CEST
  putting in multiple digcal()
  this problem looks like the old oscillating calibration which was apparently
  solved in 2011.  see sat-Track-and-Cal.py

$modified: Thu 10 Apr 2014 23:56:13 CEST
  make sure the two calibration sequences don't interfere with one another.

$modified: Tue 15 Apr 2014 11:52:51 CEST
  replacing satcal() with RFcal() and digcal()

$modified: Tue 15 Apr 2014 17:15:26 CEST
  bug fix:  need to specify "now" for subsequent calibration runs, and drift scan

$modified: Wed 16 Apr 2014 17:01:14 CEST
  if starting immediately, add 3 minutes to allow for configuration setup

$modified: Tue 22 Apr 2014 15:09:30 CEST
  removing multiple digital calibration... this was done to test csx subband sequencing
  now just do one more digital calibration and a drift.

$modified: Tue 22 Apr 2014 17:38:36 CEST
  use original satcal() for the first calibration rather than the separate methods RFcal() and digcal()

$modified: Wed 30 Apr 2014 11:39:43 CEST
  calibration source name now returned by satcal()
    in case we calibrate from file, this prevents the bug of a bad fits file name
  driftscan after the satcal() and then another digcal and driftscan

"""
helptxt="calibrate and then do another digital calibration using the digcal() method in ObsFunctions"

import datetime as dt
import ephem as eph
import math
from datefunctions import *
# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================

# === Observation title is applied for both beams =====================
ObsTitle=''
for beam in beams:
	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	comment="cal on "+obsfunc[beam].satname+", and another digital calibration "+ObsTitle
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc[beam].initialize(ObsTitle):quit()
# =====================================================================

# generate the pointings for satellite calibration with driftscan
# calibrate at next transit for each calibrator
FirstAcquisition=None
endFirstCalSequence=None
for beam in beams:

	# ======== get RFBeam and DigBeam objects ======================
	RFBeam=obsfunc[beam].makeRFBeam()
	if RFBeam==None:quit()
	DigBeam=obsfunc[beam].makeDigitalBeam()
	if DigBeam==None: quit()
	# ==============================================================

	# reinitialize obstag, AcqStarts and AcqEnds for each beam
	obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
	AcqStarts=[]
	AcqEnds=[]

	# make sure second beam calibration is done after first beam calibration
	# so as not to interfere with each other when tiles are switched off/on
	if endFirstCalSequence!=None:obsfunc[beam].caldate=endFirstCalSequence

	# ==== if we want to begin "immediately" add 3 minutes to allow the setup to be configured
	StartTime=obsfunc[beam].caldate
	if now and StartTime==None:		
		StartTime=dt.datetime.utcnow()+dt.timedelta(minutes=3)

	"""
	# ==== RF cal and dig cal using separate ObsFunctions methods
	rfcalparms=obsfunc[beam].RFcal(StartTime,elevation=50,now=now)
	if rfcalparms==None:quit()
	satcalStart=rfcalparms['StartTime']
	CmdTime=rfcalparms['endCal']
	digparms=obsfunc[beam].digcal(CmdTime,now=True)
	if digparms==None:quit()
	if endFirstCalSequence==None:endFirstCalSequence=digparms['endCal']
	CmdTime=digparms['endCal']

	# ==== and a driftscan
	driftparms=obsfunc[beam].driftscan(CmdTime,transit=False,now=True)
	if driftparms==None:quit()

	satcalEnd=driftparms['enddrift']
	"""

	# ==== RF cal and dig cal using the original satcal() in ObsFunctions
	satcalParms=obsfunc[beam].satcal(driftscan=do_satdrift,
					 now=now)
	if satcalParms==None:quit()
	CmdTime=satcalParms['EndTime']
	satcalStart=satcalParms['satcalStart']
	satcalEnd=satcalParms['EndTime']
	obsfunc[beam].log('end of calibration sequence: '+isodate(satcalEnd))
	obstag.append(satcalParms['satname'].replace(' ',''))


	# calibration and drift data acquisition times
	if FirstAcquisition==None:FirstAcquisition=satcalStart-dt.timedelta(minutes=1)
	AcqStarts.append(satcalStart-dt.timedelta(minutes=1))

	CmdTime=satcalEnd
	endobs=satcalEnd

	"""
	for count_digcals in range(3):
	# ==== now do a digital calibration
		digparms=obsfunc[beam].digcal(CmdTime,now=True)
		if digparms==None:quit()

	# ==== wait a minute and do another one	
		CmdTime=digparms['endCal']+dt.timedelta(minutes=1)
		digparms=obsfunc[beam].digcal(CmdTime,now=True)
		if digparms==None:quit()
		CmdTime=digparms['endCal']+dt.timedelta(minutes=1)

	# ==== now do another full cal, including RF cal.  check if we get proper behaviour
	rfcalparms=obsfunc[beam].RFcal(CmdTime,now=True)
	CmdTime=rfcalparms['endCal']
	digparms=obsfunc[beam].digcal(CmdTime,now=True)
	CmdTime=digparms['endCal']
	"""
	# ==== and another driftscan
	driftparms=obsfunc[beam].driftscan(CmdTime,transit=False,now=True)
	if driftparms==None:quit()
	CmdTime=driftparms['enddrift']

	# ==== do another digital calibration
	digparms=obsfunc[beam].digcal(CmdTime,now=True)
	if digparms==None:quit()
	CmdTime=digparms['endCal']

	# ==== and another driftscan
	driftparms=obsfunc[beam].driftscan(CmdTime,transit=False,now=True)
	if driftparms==None:quit()
	CmdTime=driftparms['enddrift']

	AcqEnds.append(driftparms['enddrift'])
	endobs=driftparms['enddrift']

# ===== end phase calibration ==========================================
	# === statistics acquisition
	if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
		print "\nObservation NOT submitted\n"
		quit()
# === end loop over RF Beams ==================================
		

# === finally, submit the observation for both beams ===========
obsfunc[beam].submit()
