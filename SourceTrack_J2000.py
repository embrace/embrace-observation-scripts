#! /usr/bin/env python
"""
$Id: SourceTrack_J2000.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Fri Dec  9 12:26:37 CET 2011
  originally from SourceTrack.py

Track a source after an initial RF and digital calibration on satellite
The source can be given as a command line argument (default: B0329+54)
The off-source beam can be given as a command line argument 
  default: 3 degrees away in RA
The calibrating source (satellite) can be given as a command line argument
  default: GPS BIIF-1

Tracking is done by J2000 computed by the LCU using casacore

command line arguments are processed in satorchi_OBSinit.py

$modified: Wed Apr 25 10:21:23 CEST 2012
 default source: "PSR B0329+54" changed from "B0329+54"

 exceptions for AlwaysUpError for circumpolar sources

$modified: Thu Apr 26 10:38:02 CEST 2012
 if astro obs follows on directly from calibration
 then do only one acquisition phase for the whole thing.
 don't split into separate files

$modified: Fri Apr 27 11:02:20 CEST 2012
 fixed bug with end acquisition when duration=None

$modified: Thu May 17 19:18:40 CEST 2012
 allow for no digital beam tracking (testing using beamctl independently)

$modified: Wed May 23 12:24:21 CEST 2012
 digital calibration is an option given on the command line.  default True.

$modified: Tue Jul  3 10:40:22 CEST 2012
 statsTypes is given on the command line.  default ssx,bsx

$modified: Mon 23 Jul 2012 17:31:21 CEST
  using ObsFunctions.next_transit() rather than calculating here

$modified: Tue 24 Jul 2012 17:05:22 CEST
  digital beams are centred at the RF calibration frequency
  instead of the RF centre frequency

$modified: Wed 25 Jul 2012 11:43:12 CEST
  using obsfunc to configure digital beam

$modified: Wed 22 Aug 2012 13:34:15 CEST
  using obsfunc to configure RF beam

$modified: Thu 06 Sep 2012 16:04:59 CEST
  default offset beam is 3 degree away in RA

  added RF off pointing after satellite calibration, if there's time

$modified: Wed 21 Nov 2012 10:58:30 CET
  trying to make these scripts usable by anyone...
  PYTHONPATH should include my scripts directory
  changing execfile to import for OBSinit... no, this is not the way to go.

$modified: Wed 16 Jan 2013 10:23:07 CET
  updating for new version of satcal()

$modified: Thu 07 Feb 2013 12:18:28 CET
  starting to make this for use with both Beam-A and Beam-B
  for now, Beam-A and Beam-B track the same source
  
  using new submit() method from ObsFunctions()  

$modified: Tue 12 Feb 2013 11:53:54 CET
  fixed bug:  Beam-B wasn't calibrated properly.  
              satcal has to be called with beam='B' 
              in the loop

$modified: Sat 16 Feb 2013 07:27:34 CET
  possible bug:  need obsdate=beam_caldate in loop satcal()
                 (I was using caldate)

$modified: Wed 20 Feb 2013 11:45:42 CET
  fixed bug: when source is already up during calibration phase
             I was giving a pointing time before end of cal phase
             fixed with check if CmdTime<satcalEnd

$modified: Thu 21 Feb 2013 18:29:42 CET
  added help text

$modified: Fri 08 Mar 2013 10:10:28 CET
  do not submit if cal sequence not completed because of cal source visibility

$modified: Tue 11 Jun 2013 16:13:50 CEST
  removed calculation of bandwidth, which is already done as part of the 
  argument parsing (see satorchi_OBSinit.py, and ObsFunctions.py)

$modified: Tue 30 Jul 2013 17:47:54 CEST
  using verifySource() method in ObsFunctions, and deleting similar stuff here

$modified: Thu 15 Aug 2013 15:50:53 CEST
  some rearrangement:  verify_source() moved earlier.
  if pulsar observing, send command to Andante to begin

$modified: Mon Aug 19 17:11:34 CEST 2013
  for pulsar observing, create XML and send it to Andante
  see also ObsFunctions, method mkPulsarXML()

$modified: Tue 20 Aug 2013 15:05:26 CEST
  pulsar acquisition should start a bit later than track start
  there is always some artefact in the data right at the beginning

$modified: Mon 26 Aug 2013 12:40:04 CEST
  str2dt() and tot_seconds() are in datefunctions.py
  no longer a method of class ObsFunctions

$modified: Tue 27 Aug 2013 18:47:13 CEST
  tot_seconds() : is in datefunctions.py, no longer in ObsFunctions.py

$modified: Fri 06 Sep 2013 12:52:22 CEST
  implementing independent pointing for Beam-A and Beam-B

  source becomes obsfunc.source
  no default source.  if none given, quit.
  moving RFBeam creation to satorchi_OBSinit.py

$modified: Wed 16 Oct 2013 11:18:54 CEST
  FirstAcquisition: ensuring Beam-A and Beam-B acquisitions start at the same time.

$modified: Fri 15 Nov 2013 15:54:05 CET
  changes towards independence of Beam-A and Beam-B:  obsfunc object for each RF beam    

$modified: Sun 17 Nov 2013 08:19:47 CET
  continuing towards independence of Beam-A and Beam-B:
  caldate, obsdate different for each beam

$modified: Mon 18 Nov 2013 13:43:41 CET
  removed beam_caldate which had been introduced to align the RF beam calibration times

$modified: Tue 19 Nov 2013 10:36:22 CET
  mkPulsarXML() : argument is now duration rather than startTime and endTime

$modified: Mon 23 Dec 2013 08:30:02 CET
  minor change for initialize: obsfunc[beam] instead of obsfunc['A']

$modified: Mon 20 Jan 2014 12:47:41 CET
  bug fix:  duration is no longer a global variable. must use obsfunc[beam].duration

$modified: Tue 08 Apr 2014 17:10:09 CEST
  bug fix:  killobs replaced with beam specific killobs so as to avoid killing the other beam which may be running
  using ObsFunctions.submitAtBackend() for launching pulsar acquisition

$modified: Wed 16 Apr 2014 12:55:32 CEST
  yoffset for cal drift scan is passed via obsfunc for each beam.

$modified: Fri 20 Jun 2014 14:18:50 CEST
  generic backend for pulsar acquisition.
  psr runfilename for different pulsars

$modified: Thu 10 Jul 2014 14:54:47 CEST
  mkRunArtemis should have the source name to choose the correct xml files

$modified: Wed 23 Jul 2014 15:43:13 CEST
  bug fix: need to call makeRFBeam() rather than getRFBeam()

$modified: Thu 31 Jul 2014 13:29:14 CEST
  removed cal type declaration.  this is done immediately after the calibration and is not necessary here
  also, it interferes with using saved cal parameters. i.e. caltype='none'

$modified: Thu 14 Aug 2014 14:47:49 CEST
  minor change:  using isodate()

$modified: Wed 24 Feb 2016 06:30:24 CET
  stop ARTEMIS acquisition after the end of the observation

$modified: Sun 08 Jan 2017 18:44:50 CET
  clean up for pulsar commands (changes to ObsFunctions)

"""
helptxt='Track an astronomical source after an initial RF and digital calibration on a calibration source'
import sys,os
import datetime as dt
import ephem as eph
import math
from datefunctions import *
# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================

# === Observation title is applied for both beams =====================
# source name given on the command line, there is no default
ObsTitle=''
for beam in beams:
	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	comment="cal on "+obsfunc[beam].satname+", and tracking "+ObsTitle
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc[beam].initialize(ObsTitle):quit()
# =====================================================================

# ===== phase calibration ==============================================
# generate the pointings for satellite calibration with driftscan
# default frequencies are given by obsfunc.FindSat() which was called in satorchi_OBSinit.py
backend_cmd=[]
FirstAcquisition=None
for beam in beams:

	# ======== get RFBeam and DigBeam objects ======================
	RFBeam=obsfunc[beam].makeRFBeam()
	if RFBeam==None:quit()
	DigBeam=obsfunc[beam].makeDigitalBeam()
	if DigBeam==None: quit()
	# ==============================================================

	# reinitialize obstag, AcqStarts and AcqEnds for each beam
	obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
	AcqStarts=[]
	AcqEnds=[]

	satcalParms=obsfunc[beam].satcal(digcal=digcal,
					 driftscan=do_satdrift,
					 now=now)
	if satcalParms==None: quit()
	if not satcalParms['PointingStatus']:
		obsfunc[beam].log("Could not complete the satellite calibration sequence")
		quit()

	obstag.append(satcalParms['satname'].replace(' ',''))
	satcalStart=satcalParms['satcalStart']
	satcalEnd=satcalParms['EndTime']

	# calibration and drift data acquisition times
	if FirstAcquisition==None:FirstAcquisition=satcalStart-dt.timedelta(minutes=1)
	AcqStarts.append(satcalStart-dt.timedelta(minutes=1))
	AcqEnds.append(satcalEnd)
# ===== end phase calibration ==========================================

# ========= astronomical source pointing ================================
	srcParms=obsfunc[beam].verifySource(obsfunc[beam].source,satcalEnd,obsfunc[beam].offsource)
	if srcParms==None: quit()
	srcEph=srcParms['srcEph']
	offsrcEph=srcParms['offEph']

	# for the source tracking
	CmdTime=satcalEnd
	if obsfunc[beam].obsdate!=None and obsfunc[beam].obsdate>satcalEnd:
		CmdTime=obsfunc[beam].obsdate

	src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,True,date=CmdTime)
	if src_rise==None: quit()

	if obsfunc[beam].obsdate==None:CmdTime=src_rise

	# ensure that CmdTime is after the calibration end  time
	if CmdTime<satcalEnd:CmdTime=satcalEnd

	# recalculate setting time to make sure it's after the start time
	src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,True,date=CmdTime)
	if src_rise==None: quit()

	obsfunc[beam].embraceNancay.date=CmdTime
	srcEph.compute(obsfunc[beam].embraceNancay)
	az=math.degrees(srcEph.az)
	alt=math.degrees(srcEph.alt)
	srcEph_ra=math.degrees(srcEph.a_ra)
	srcEph_dec=math.degrees(srcEph.a_dec)

	comment=srcEph.name+"  RA="+str(srcEph_ra)+" dec="+str(srcEph_dec)
	obsfunc[beam].log(comment)
	comment=srcEph.name+" tracking by J2000 at "+isodate(CmdTime)
	trackingStartTime=CmdTime
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
	comment=srcEph.name+" is at  az="+str(az)+" alt="+str(alt)
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)

	# calculate transit for an RF off pointing, if there's time after the sat cal
	obsfunc[beam].embraceNancay.date=src_transit
	srcEph.compute(obsfunc[beam].embraceNancay)
	transit_az=math.degrees(srcEph.az)
	transit_alt=math.degrees(srcEph.alt)


	# RF beam tracking
	RFBeam.AddSubscan(CmdTime,'J2000',srcEph_ra,srcEph_dec)

	# offsource if requested
	if obsfunc[beam].offsource==None: # by default use an offset 3 deg away in RA
		comment='off beam is 3 degrees away in RA'
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)
		offsrcEph_ra=srcEph_ra+3
		offsrcEph_dec=srcEph_dec	
	else:
		offsrcEph.compute(obsfunc[beam].embraceNancay)
		offsrcEph_ra=math.degrees(offsrcEph.a_ra)
		offsrcEph_dec=math.degrees(offsrcEph.a_dec)
	

	if digtrack:
		comment=" with normal digital beam tracking"
		DigBeam.AddSubscan(CmdTime,'J2000',srcEph_ra,srcEph_dec,offsrcEph_ra,offsrcEph_dec)
	else:
		comment="*** No digital beam tracking ***"
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)


	# duration of tracking.  Default None=while visible
	# if duration given on the command line, 
	# it is converted to a timedelta by the parser in satorchi_OBSinit.py
	if obsfunc[beam].duration==None:
		obsfunc[beam].log("setting end acquistion to source setting time: "+isodate(src_set))
		endobs=src_set
	else:
		endobs=CmdTime+obsfunc[beam].duration

	# acquisition times
	# if the main observation follows directly from the calibration
	# make a single acquisition phase for both
	waitTime=CmdTime-satcalEnd
	waitTimeMins=tot_seconds(waitTime)/60.0
	obsfunc[beam].log("time between satellite end tracking and "+srcEph.name+" track start: "+str(waitTimeMins)+" minutes")

	if waitTime < dt.timedelta(minutes=30):
		AcqEnds[0]=endobs
		obstag[0]+="--"+srcEph.name

		# point immediately after calibration to the position where the source rises
		if waitTime > dt.timedelta(minutes=1):
			RFBeam.AddSubscan(satcalEnd,'AZEL',az,alt)
			DigBeam.AddSubscan(satcalEnd,'AZEL',az,alt,az,alt)

	else: #we have time for a blank sky pointing before tracking starts
		AcqStarts.append(CmdTime)
		AcqEnds.append(endobs)
		obstag.append(srcEph.name)

		CmdTime=AcqEnds[0]
		AcqEnds[0]+=dt.timedelta(minutes=30)

		RFBeam.AddSubscan(CmdTime,'AZEL',transit_az,transit_alt)
		DigBeam.AddSubscan(CmdTime,'AZEL',transit_az,transit_alt,transit_az,transit_alt)
		comment='pointing at empty sky for 30 minutes starting at '+isodate(CmdTime)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)


# === end pointing configuration ==================

# === Define  STATISTICS ACQUISITION phases ======

	# align the acquistion times of the two beams, if they are within a few minutes of one another
	delta=AcqStarts[0] - FirstAcquisition
	if delta>dt.timedelta(minutes=0) and delta<dt.timedelta(minutes=45): AcqStarts[0]=FirstAcquisition
	if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
		print "\nObservation NOT submitted\n"
		quit()

# === Define commands for pulsar observing on Andante or Borsen
        psrAcqStart=trackingStartTime+dt.timedelta(minutes=2)
	psrAcqEnd=endobs-dt.timedelta(minutes=2)

        obsfunc[beam].mkBackendCommands(psrAcqStart,psrAcqEnd)

# === finally, submit the observation for both beams ===========
submit_EMBRACE_observation(obsfunc)



