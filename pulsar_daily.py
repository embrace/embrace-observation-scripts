#! /usr/bin/env python
"""
$Id: pulsar_daily.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Mon 18 Nov 2013 16:31:19 CET
  originally from SourceTrack_J2000.py (last mod:Mon Nov 18 16:16:51 CET 2013) 

RF and digital calibration on two different calibrators (Sun, and GPS)
followed by daily observations of B0329+54 at two different frequencies
continue for N days

$modified: Wed 15 Jan 2014 12:51:52 CET
  changing some parameters to test probs with acquisition
  ndays=2 (this was already done some time ago)
  B:cal=f3

$modified: Thu 23 Jan 2014 00:24:46 CET
  ndays=4

$modified: Sun 26 Jan 2014 22:03:06 CET
  acquisition seems to be working, go back to...
  ndays=7

$modified: Thu 06 Feb 2014 22:35:40 CET
  changing sys.argv.append to sys.argv.insert so that command line arguments take precedence by appearing last

$modified: Tue 08 Apr 2014 17:12:31 CEST
  bug fix:  killobs replaced with beam specific killobs so as to avoid killing the other beam which may be running
  using ObsFunctions.submitAtBackend() for launching pulsar acquisition

$modified: Wed 16 Apr 2014 13:25:16 CEST
  yoffset for cal drift scan is passed via obsfunc for each beam.
  
"""
ndays=7 # eventually this could be a command line option
ndays=7

helptxt='RF and digital calibration on two different calibrators (Sun, and GPS)\n'\
    +'followed by daily observations of B0329+54 at two different frequencies.\nContinue for N days.\n'\
    +'Note that many command lines options are overridden here: duration, driftduration, freqs...'

import sys,os
sys.argv.insert(1,'--beams=both')
sys.argv.insert(1,'--source=b0329')
sys.argv.insert(1,'--duration=120')
sys.argv.insert(1,'--driftduration=120')
sys.argv.insert(1,'--A:nohorizon')
sys.argv.insert(1,'--A:cal=Sun')
sys.argv.insert(1,'--A:RFcentre=970')
sys.argv.insert(1,'--A:RFcalfreq=970')
sys.argv.insert(1,'--B:cal=f2')

import datetime as dt
import ephem as eph
import math
from datefunctions import *
# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================

# === Observation title is applied for both beams =====================
ObsTitle=''
for beam in beams:	
	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	comment="cal on "+obsfunc[beam].satname+", and tracking "+ObsTitle
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc[beam].initialize(ObsTitle):quit()
# =====================================================================

# generate the pointings for satellite calibration with driftscan
# calibrate at next transit for each calibrator
backend_cmd=[]
FirstAcquisition=None
didEmptySky={}
didEmptySky['A']=False
didEmptySky['B']=False
for beam in beams:

	# ======== get RFBeam and DigBeam objects ======================
	RFBeam=obsfunc[beam].getRFBeam()
	if RFBeam==None:quit()
	DigBeam=obsfunc[beam].makeDigitalBeam()
	if DigBeam==None: quit()
	# ==============================================================

	# reinitialize obstag, AcqStarts and AcqEnds for each beam
	obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
	obstag.append(obsfunc[beam].satname.replace(' ',''))
	AcqStarts=[]
	AcqEnds=[]

	satcalParms=obsfunc[beam].satcal(digcal=digcal,
					 driftscan=do_satdrift,
					 now=now)
	if satcalParms==None: quit()
	if not satcalParms['PointingStatus']:
		obsfunc[beam].log("Could not complete the satellite calibration sequence")
		quit()

	satcalStart=satcalParms['satcalStart']
	satcalEnd=satcalParms['EndTime']

	# calibration and drift data acquisition times
	if FirstAcquisition==None:FirstAcquisition=satcalStart-dt.timedelta(minutes=1)
	AcqStarts.append(satcalStart-dt.timedelta(minutes=1))
	AcqEnds.append(satcalEnd)
# ===== end phase calibration ==========================================

# ========= astronomical source pointing ================================
	srcParms=obsfunc[beam].verifySource(obsfunc[beam].source,satcalEnd,obsfunc[beam].offsource)
	if srcParms==None: quit()
	srcEph=srcParms['srcEph']
	offsrcEph=srcParms['offEph']

        # === xml file for ARTEMIS
	xmlfilename=obsfunc[beam].mkPulsarXML(srcEph.name,obsfunc[beam].duration-dt.timedelta(minutes=4))
	cmd="scp -p "+xmlfilename+" __BACKEND__:EMBRACE"
	backend_cmd.append(cmd)

	CmdTime=satcalEnd
	for day in range(ndays):
		obsfunc[beam].log(' <><><><><><> Day '+str(day)+' <><><><><><>')

		# track source around transit time
		src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,True,date=CmdTime)
		if src_rise==None: quit()
		CmdTime=src_transit - obsfunc[beam].duration/2

		# ensure that CmdTime is after the calibration end  time
		if CmdTime<satcalEnd:CmdTime=satcalEnd

		# recalculate setting time to make sure it's after the start time
		src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,True,date=CmdTime)
		if src_rise==None: quit()

		obsfunc[beam].embraceNancay.date=CmdTime
		srcEph.compute(obsfunc[beam].embraceNancay)
		az=math.degrees(srcEph.az)
		alt=math.degrees(srcEph.alt)
		srcEph_ra=math.degrees(srcEph.a_ra)
		srcEph_dec=math.degrees(srcEph.a_dec)

		comment=srcEph.name+"  RA="+str(srcEph_ra)+" dec="+str(srcEph_dec)
		obsfunc[beam].log(comment)
		comment=srcEph.name+" tracking by J2000 at "+isodate(CmdTime)
		psrAcqStart=CmdTime+dt.timedelta(minutes=2)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)
		comment=srcEph.name+" is at  az="+str(az)+" alt="+str(alt)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)

		# calculate transit for an RF off pointing, if there's time after the sat cal
		obsfunc[beam].embraceNancay.date=src_transit
		srcEph.compute(obsfunc[beam].embraceNancay)
		transit_az=math.degrees(srcEph.az)
		transit_alt=math.degrees(srcEph.alt)


		# RF beam tracking
		RFBeam.SetCal('current')
		DigBeam.SetCal('current')
		RFBeam.AddSubscan(CmdTime,'J2000',srcEph_ra,srcEph_dec)

		# offsource if requested
		if obsfunc[beam].offsource==None: # by default use an offset 3 deg away in RA
			comment='off beam is 3 degrees away in RA'
			obsfunc[beam].log(comment)
			obsfunc[beam].addFITScomment(comment)
			offsrcEph_ra=srcEph_ra+3
			offsrcEph_dec=srcEph_dec	
		else:
			offsrcEph.compute(obsfunc[beam].embraceNancay)
			offsrcEph_ra=math.degrees(offsrcEph.a_ra)
			offsrcEph_dec=math.degrees(offsrcEph.a_dec)
	
		DigBeam.AddSubscan(CmdTime,'J2000',srcEph_ra,srcEph_dec,offsrcEph_ra,offsrcEph_dec)
		endobs=CmdTime+obsfunc[beam].duration
		psrAcqEnd=endobs-dt.timedelta(minutes=2)

		# acquisition times
		# if the main observation follows directly from the calibration
		# make a single acquisition phase for both
		if not didEmptySky[beam]:
			waitTime=CmdTime-satcalEnd
			waitTimeMins=tot_seconds(waitTime)/60.0
			obsfunc[beam].log("time between satellite end tracking and "+srcEph.name+" track start: "+str(waitTimeMins)+" minutes")
			if waitTime < dt.timedelta(minutes=30):
				AcqEnds[0]=endobs
				obstag[0]+="--"+srcEph.name
				
				# point immediately after calibration to the position where the source rises
				if waitTime > dt.timedelta(minutes=1):
					DigBeam.SetCal('current')
					RFBeam.AddSubscan(satcalEnd,'AZEL',az,alt)
					DigBeam.AddSubscan(satcalEnd,'AZEL',az,alt,az,alt)

			else: #we have time for a blank sky pointing before tracking starts
				AcqStarts.append(CmdTime)
				AcqEnds.append(endobs)
				obstag.append(srcEph.name)
				
				CmdTime=AcqEnds[0]
				AcqEnds[0]+=dt.timedelta(minutes=30)

				RFBeam.SetCal('current')
				DigBeam.SetCal('current')
				RFBeam.AddSubscan(CmdTime,'AZEL',transit_az,transit_alt)
				DigBeam.AddSubscan(CmdTime,'AZEL',transit_az,transit_alt,transit_az,transit_alt)
				comment='pointing at empty sky for 30 minutes starting at '+isodate(CmdTime)
				obsfunc[beam].log(comment)
				obsfunc[beam].addFITScomment(comment)
				didEmptySky[beam]=True
		else:
			AcqStarts.append(CmdTime)
			AcqEnds.append(endobs)
			obstag.append(srcEph.name)



		# === Define commands for pulsar observing on Andante
		if srcEph.name[0:3]=='PSR':

			# compose commands for Andante, send them after job submission
			# kill observations just before beginning
			cmd="ssh __BACKEND__ '/home/artemis/remote_cmd at "\
			    +(psrAcqStart-dt.timedelta(minutes=2)).strftime('%H:%M %Y-%m-%d')\
			    +" -f /home/artemis/EMBRACE/killobs_Beam"+beam+".sh'"
			backend_cmd.append(cmd)

			cmd="ssh __BACKEND__ '/home/artemis/remote_cmd at "\
			    +psrAcqStart.strftime('%H:%M %Y-%m-%d')\
			    +" -f /home/artemis/EMBRACE/run_beam"+beam+"'"
			backend_cmd.append(cmd)


		# === align the acquistion times of the two beams, if they are within a few minutes of one another
		delta=AcqStarts[0] - FirstAcquisition
		if delta>dt.timedelta(minutes=0) and delta<dt.timedelta(minutes=45): AcqStarts[0]=FirstAcquisition

		CmdTime=endobs
# === end loop over days ======================================


	# === statistics acquisition
	if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
		print "\nObservation NOT submitted\n"
		quit()
# === end loop over RF Beams ==================================
		

# === finally, submit the observation for both beams ===========
if obsfunc[beam].submit() and srcEph.name[0:3]=='PSR':
	obsfunc[beam].submitAtBackend(backend_cmd)



