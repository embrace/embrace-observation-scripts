#! /usr/bin/env python
"""
$Id: sat-Track-and-Cal.py
$created: Tue Nov 15 11:54:04 CET 2011
   from satCalTrack.py

Do a calibration on a satellite,
and then track it for a few minutes,
and then redo a full calibration, 
and repeat

This script will be used to help build up a database
of RF pointing parameters.

The RF pointing parameters are saved by a capturer
running at the same time
the capturer script is: satorchi_CalParmsCapturer.py

$modified: Thu Nov 17 11:25:12 CET 2011
  MyObservation is no longer passed as an argument
  it is set in satorchi_OBSinit.py as a class variable in ObsFunctions()

$modified: Fri Nov 18 12:44:35 CET 2011
  all times are in UT.  
  There's no longer a difference between acquisition times and pointing times
  ut2local has been set to zero in ObsFunctions, but I'm cleaning things up here anyway

  adding wait times between calibration phases and tracking
  still trying to track down the weird oscillating behaviour...
  this seems to help!

$modified: Mon Nov 21 10:09:17 CET 2011
  satcal() now returns start and end times, so no longer use obsfunc.satcalStart etc

  5 second wait after calibration doesn't seem to be enough.  try 10 seconds

$modified: Tue Nov 22 10:57:34 CET 2011
  10 seconds doesn't work either
  introducing variable "margin"

$modified: Tue Nov 22 12:09:41 CET 2011
  option to not do digital calibration
  this is to try and track down the problem of the oscillating calibration results

$modified: Wed Nov 23 08:58:26 CET 2011
  duration: half hour
  remember to remove this!

$modified: Wed Nov 23 09:21:43 CET 2011
  new plan, after talking to Christophe T.
  increment the margin after each cycle to see what is the critical wait time
  and if that is really the problem...
  margin is applied only at the end of the digital cal before starting to track
  small constant margin of 20 seconds is used before the next RF calibration
  tracking period is reduced to 3 minutes to have more cycles
  margin is increased by 10 seconds every 4th cycle

$modified: Wed Nov 23 12:43:53 CET 2011
  first acquisition start time is one minute before the pointing start time
  this is because there's a delay of about 30 seconds before acquisition begins
  Patrice R. is looking for the problem

  margin is increased by 30 seconds after 4th cycle

$modified: Mon Dec  5 12:10:48 CET 2011
  It looks like we finally found the problem with the oscillating digital calibration.
  The parameters have to be reset to zero before starting calibration.
  Christophe T. has implemented this.  See observation of GPSBIIA-25_bsx_20111205-1021.fits

  the margin is set to zero

$modified: Tue Dec  6 16:25:39 CET 2011
  do a drift scan before entering the tracking loop
  trying to debug satdriftAcq.py, and using this script to compare
  drift_counter: do a drift on the satellite every Nth time

  and change back again...
  do not do a drift scan before entering the tracking loop

$modified: Fri May  4 11:58:19 CEST 2012
  track for 10 minutes between calibrations (changed from 3 minutes)

  duration is assigned as a command line argument (see satorchi_OBSinit.py)
  by default: None = track while visible

$modified: Sat 16 Feb 2013 06:36:22 CET
  updating for changes to ObsFunctions and satorchi_OBSinit.py
  using FullPathname() to find init file

$modified: Thu 21 Feb 2013 13:24:35 CET
  added helptxt

$modified: Thu Mar  7 16:44:09 CET 2013
  do not submit if cal sequence not completed

"""
helptxt='Do a calibration on a satellite, and then track it for a few minutes,\n'\
    +'and then redo a full calibration, and repeat'

import sys,os
import datetime as dt

# Tue Nov 22 12:26:14 CET 2011: margin=1 minute
# Wed Nov 23 08:54:41 CET 2011: margin=30 seconds
# Mon Dec  5 12:10:48 CET 2011: margin=0 seconds, we found the problem
margin=dt.timedelta(seconds=0)


# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================

# =====  create RF Beams ==============================================
RFBeam={}
for beam in beams:
	RFBeam[beam]=obsfunc.makeRFBeam(beam)
	if RFBeam[beam]==None: quit()
# =====================================================================

# ===== prepare variables for data acquisition ========================
ObsTitle=obsfunc.satname.replace(' ','')
comment="cal on "+obsfunc.satname+", and track"
obsfunc.addFITScomment(comment)
obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
obstag.append(ObsTitle)
AcqStarts=[]
AcqEnds=[]
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc.initialize(ObsTitle):quit()
# =====================================================================

# ==== Define directions, times, obsparams ============================
# Desired number of beamlets per dig.beam bandpass
# ObsFunc also automatically sets the associated bandwidth
obsfunc.nBeamlets(nbeamlets)     
bw=obsfunc.Bandwidth()
# ======================================================================


# ======== Start digital beam config ===================================
DigBeam={}
for beam in beams:
	DigBeam[beam]=obsfunc.makeDigitalBeam(beam=beam)
	if DigBeam[beam]==None: quit()
# ========= End digital beam config ====================================


# ===== track the satellite with periodic recalibration ===============
# start with a calibration, which also determines the start time
# start time is some time after submission, depending on command line options
# Tue Nov 22 12:26:04 CET 2011: temporary option:  no digital calibration in satcal()
# Wed Nov 23 08:49:24 CET 2011: digital cal re-implemented
# Tue Dec  6 16:25:39 CET 2011: do a drift scan before entering the loop
#    see: GPSBIIR-4_bsx_20111206-1530.fits
# Tue Dec  6 17:06:57 CET 2011: do drift scan in the loop, but not before
#    see: GPSBIIR-4_bsx_20111206-1612.fits
beam_caldate=caldate
if beam_caldate==None:beam_caldate=obsdate
satcalParms={}
for beam in beams:
	satcalParms[beam]=obsfunc.satcal(obsdate=beam_caldate,
					 driftscan=False,
					 yoffset=yoffset,
					 now=now,
					 digcal=True,
					 beam=beam)
	if satcalParms[beam]==None: quit()
	if not satcalParms[beam]['PointingStatus']:
		obsfunc.log("\nCould not complete the satellite calibration sequence")
		quit()
	beam_caldate=satcalParms[beam]['satcalEnd']
# acquisition start time is the start of the cal for the first beam
satcalStart=satcalParms[beams[0]]['satcalStart']
# end time is the end of the cal for the last beam
satcalEnd=satcalParms[beams[len(beams)-1]]['EndTime']

maxEndTime=None
if duration!=None:
	maxEndTime=satcalStart+duration
	obsfunc.log("requested end time for observation: "+maxEndTime.isoformat(' ')+" UT")
AcqStarts.append(satcalStart-dt.timedelta(minutes=1)) # determined by satcal() above
StartTime=satcalEnd

# create a counter for incrementing margin after 4 cycles
margin=dt.timedelta(seconds=0)
margin_counter=0

# do a drift scan after every 3rd cycle
drift_counter=0
do_drift=False
drift_interval=20 # do a drift every Nth cycle

while StartTime!=None:
	LastTime=StartTime # hold on to the latest time

	# track the satellite for 10 minutes
	for beam in beams:
		StartTime,endTracking=obsfunc.SourceTrack(obsfunc.satname,
							  StartTime,
							  OffBeam=None,
							  duration=dt.timedelta(minutes=10),
							  calibration_interval=None,
							  beam=beam)
	if StartTime==None:break

	# and do another RF & digital calibration, add a 20 second margin before beginning
	PointingStatus=True
	for beam in beams:
		satcalParms[beam]=obsfunc.satcal(obsdate=endTracking+dt.timedelta(seconds=20),
						 driftscan=do_drift,
						 yoffset=yoffset,
						 now=True,
						 digcal=True,
						 beam=beam)
		PointingStatus=PointingStatus and satcalParms[beam]['PointingStatus']

	StartTime=satcalParms[beams[0]]['satcalStart']
	EndTime=satcalParms[beams[len(beams)-1]]['EndTime']

	if not PointingStatus or (maxEndTime!=None and EndTime>maxEndTime): 
		StartTime=None
		break
	

	drift_counter+=1
	if drift_counter==drift_interval:
		do_drift=True
		drift_counter=0
	else:
		do_drift=False

	margin_counter+=1
	if margin_counter==3:
		margin+=dt.timedelta(seconds=10)
		margin_counter=0
	StartTime=EndTime+margin
# =====================================================================

# ====== assign final time for statistics acquisition==================
if EndTime==None:
	EndTime=endTracking
if EndTime==None:
	EndTime=LastTime
AcqEnds.append(EndTime+dt.timedelta(minutes=5)) # a little extra
# =====================================================================


# === Define  STATISTICS ACQUISITION phases ===========================
for beam in beams:
	if not obsfunc.Acquire(obstag,AcqStarts,AcqEnds,statsTypes,beam=beam):
		print "\nObservation NOT submitted\n"
		quit()
# =====================================================================

# === finally, submit the observation =================================
obsfunc.submit()
# =====================================================================

