#! /usr/bin/python

# B. Censier 26/08/2014
# Read output of dump1090, basestation format, by reading TCP socket (parameters HOST and PORT to be set up properly, see beginning of code)
# Version of dump1090 used: https://github.com/MalcolmRobb/dump1090.git
# Dump1090 launched with 'dump1090 --net --interactive'
# All planes detected stored in a dictionary with plane Hex Id as the key. The latter plane key is linking to a sub-dictionnary storing all the parameters as a list for each parameter key 
# A subset of the parameters are also saved in an ascii file, meant to be loaded in python afterwards, with e.g. M=loadtxt(filename,usecols=range(1,10)) (here zapping the first column, which is a string and not a number)
# The Ctrl+C signal is caight to terminate the script properly (see signal_handler())

import socket
import pyproj
import math
import astropy.coordinates as coord
import curses
import numpy as np
import signal
import datetime as dt
import matplotlib.pyplot as plt
import time
import pytz

global azels, latlons, lms

HOST = 'nan18'#'localhost'#'192.168.0.18'#'localhost'    # The remote host
PORT = 30003              # socket port of remote host 


### GRAPH ###
#plt.ion()
#plt.figure()
#plt.xlim([-1,1])
#plt.ylim([-1,1])
#plt.axis('equal')
#############

print "\n                   ### Hit Ctr+C to stop ### \n"

# open ascii file to save paramters
filename = "data_dump1090_"+str(dt.datetime.now().year)+"_"+str(dt.datetime.now().month)+"_"+str(dt.datetime.now().day)+"_"+str(dt.datetime.now().hour)+str(dt.datetime.now().minute)+str(dt.datetime.now().second)+".txt"
fid = open(filename,"w")
print "### Saving data in file: "+filename+" ###\n"


# time zone info
timezone = pytz.timezone(time.tzname[0])

# Observer (EMBRACE station)
lat = 47.381944 #embrace  ##47.077778 #bourges
lon = 2.199444 #embrace   ##2.410713 #bourges
alt = 0

aa = coord.EarthLocation.from_geodetic(0,0,alt)
xyz = np.array(aa.value) # cartesiant coordinates of observing station, geocentric, origin at base station 

# pyproj project to compute curvilinear distance of plane
g=pyproj.Geod(ellps='GRS80')

# useful(?) constants
earth_radius = 6.371009e6 # (m)


# all the detected planes
tab_all = {}
# planes outside EMBRACE'shorizon
tab_out = {}
# planes inside EMBRACE'shorizon
tab_in = {}


latlons = [] # for storing lat,lon of planes 
azels = [] # for storing az,el of planes 
curv_dists=[] # for storing curv_dist 
lms = [] # for storing l,m coordinates


# function to be launched when user makes Ctrl+C 
def signal_handler(signal, frame):
    	global azels,latlons,lms,fid, s
	azels = array(azels)
	latlons = array(latlons)
	lms = array(lms)
	fid.close()
	s.close()
	print 'You pressed Ctrl-C => Quitting script'
   	sys.exit(0)

# redirect Ctrl+C
signal.signal(signal.SIGINT, signal_handler)

while 1==1:
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

	s.connect((HOST, PORT))

	data = s.recv(65536).split(',')
	#pause(0.01) # for plotting with matplotlib

	Ref = data[4] # plane Id
	lat_temp = data[14]
	lon_temp = data[15]
	alt_temp = data[11]

	if lat_temp != '':
		lat_temp = float(lat_temp)
	else:
		lat_temp=np.nan

	if lon_temp != '':
                lon_temp = float(lon_temp)
        else:
                lon_temp=np.nan
	if alt_temp != '':
		alt_temp = float(alt_temp)*0.305 # convert from feet to meters
	else:
		alt_temp=np.nan


	#tab_all[Ref] = {}
	if not np.isnan(alt_temp) and not np.isnan(lon_temp) and not np.isnan(lat_temp):

		aa,bb,curv_dist=g.inv(lon,lat,lon_temp,lat_temp)

	else:
		curv_dist = np.nan	
	
	aa = coord.EarthLocation.from_geodetic(lon_temp-lon,lat_temp-lat,alt_temp)
	xyz_temp = np.array(aa.value) # cartesian coordinates of plane, geocentric

	xyz_temp2 = xyz_temp - xyz # change cartesian coordinates of plane to an origin at the observing station 
	r_plane = np.sqrt(sum(xyz_temp2**2)) # r coordinates from observing station
 

	# if plane has any current lat,lon and altitude (and is inside horizon?)
	if not np.isnan(lat_temp) and not np.isnan(lon_temp) and not np.isnan(alt_temp):# and el>45:
		
		
		
		# compute el as the inverse tan (!! x is the height axis !!)
		el = np.rad2deg(math.atan(xyz_temp2[0]/np.sqrt(xyz_temp2[1]**2+xyz_temp2[2]**2)))
		
		
		# compute az as inverse tan + put result between 0 and 360deg (!! x is the height axis !!)
		# az between 0 (North) and 360 (north)
		az = -np.rad2deg(math.atan(xyz_temp2[2]/xyz_temp2[1])-np.pi/2) # -pi/2 => 0 az at north

		if xyz_temp2[1]<0:
        		az = az+180


		

		l=np.cos(np.deg2rad(el))*np.cos(np.deg2rad(az))
		m=np.cos(np.deg2rad(el))*np.sin(np.deg2rad(az))
		lms.append(np.array([l,m]))
		

		latlons.append(np.array([lat_temp,lon_temp]))
		azels.append(np.array([az,el]))
		curv_dists.append(curv_dist)
		
		# convert date1(date) and date2(hour) fields into a single datetime object
		plane_year = int(data[6].split('/')[0])
		plane_month = int(data[6].split('/')[1])
		plane_day = int(data[6].split('/')[2])
		plane_hour = int(data[7].split(':')[0])
		plane_minute = int(data[7].split(':')[1])
		plane_second = int(data[7].split(':')[2].split(".")[0])		
		plane_microsecond = int(data[7].split(':')[2].split(".")[1])

		plane_datetime = dt.datetime(plane_year,plane_month,plane_day,plane_hour,plane_minute,plane_second,plane_microsecond,timezone)		
		plane_timestamp = time.mktime(plane_datetime.timetuple())



		

		# store in dictionary according to the template (key,value)
		param_keys = ['curv_dist','date1','date2','timestamp','datetime','callsign','altitude','groundspeed','groundheading','lat','lon','verticalrate','squawk','squawkemergency','ontheground','az','el','l','m','r']
		param_values = [curv_dist,data[6],data[7],plane_timestamp,plane_datetime,data[10],alt_temp,data[12],data[13],lat_temp,lon_temp,data[16],data[17],data[19],data[21],az,el,l,m,r_plane]

		# list of params saved in ascii file
		param_keys_saved = ['timestamp','lon','lat','az','el','l','m','altitude','r']		

		if not tab_all.has_key(Ref) : # no previous detection for the given plane: initialize it to a dictionary of empty lists
			tab_all[Ref] = {}
			for key in param_keys:
				tab_all[Ref][key] = []
		
		ikey = 0
		for key in param_keys:
			tab_all[Ref][key].append(param_values[ikey])  # append each new parameter to the corresponding list in the plane dictionary
			ikey +=1
		
		param_temp=Ref+'\t' #initialize list with plane Id 
		param_temp_disp = param_temp # second string only for inline display
		for key in param_keys_saved:
			param_temp = param_temp+str(tab_all[Ref][key][-1])+'\t' # create string line with all parameters to be saved
			param_temp_disp = param_temp_disp+str("%5.3f"%tab_all[Ref][key][-1])+'\t' # create string line with all parameters to be saved
		param_temp = param_temp+"\n"
		
		print data[6]+" - "+data[7]
		print 'Id\t'+'\t'.join(param_keys_saved)
		print param_temp_disp
		print str(len(latlons))+" detections stored so far.. \n"

		fid.writelines(param_temp)
		

#	plt.hold(True)
	for key in tab_all.keys():
		if tab_all[key] != {}:
			#print "######\n plane Id: "+key+"\n"
			#print tab_all[key]
			#print tab_all[key]['az'],tab_all[key]['el'],"\n"
			l=np.cos(np.deg2rad(tab_all[key]['el']))*np.cos(np.deg2rad(tab_all[key]['az']))
			m=np.cos(np.deg2rad(tab_all[key]['el']))*np.sin(np.deg2rad(tab_all[key]['az']))
#			plt.plot(-m,l,'bo')
#	plt.hold(False)
	#plt.show()

	
	
	s.close()
	#print '\nReceived: ', repr(data)

# From dump1090's net_io.c:
###########################
# Fields 1 to 6 : SBS message type and ICAO address of the aircraft and some other stuff
# Fields 7 & 8 are the current time and date
# Fields 9 & 10 are the current time and date
# Field 11 is the callsign (if we have it)
# Field 12 is the altitude (if we have it) - force to zero if we're on the ground
# Field 13 is the ground Speed (if we have it)
# Field 14 is the ground Heading (if we have it)
# Fields 15 and 16 are the Lat/Lon (if we have it)
# Field 17 is the VerticalRate (if we have it)
# Field 18 is  the Squawk (if we have it)
# Field 19 is the Squawk Changing Alert flag (if we have it)
# Field 20 is the Squawk Emergency flag (if we have it)
# Field 21 is the Squawk Ident flag (if we have it)
# Field 22 is the OnTheGround flag (if we have it)

s.close()

# l,m coordinates
# l=cos(deg2rad(azels[:,1]))*cos(deg2rad(azels[:,0]))
# m=cos(deg2rad(azels[:,1]))*sin(deg2rad(azels[:,0]))


