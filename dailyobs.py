#! /usr/bin/env python
"""
$Id: dailyobs.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Thu 14 Aug 2014 13:31:55 CEST
$license: GPLv3 or later, see https://www.gnu.org/licenses/gpl-3.0.txt

setup observations for daily submission using "at" on the LCU and the SCU
You should have your certificates setup for no-password login to LCU (and SCU if remote)
It's best to run this from the SCU

$modified: Fri 22 Aug 2014 17:38:40 CEST
  argument options for testmode and number of days

$modified: Tue 06 Jan 2015 13:21:29 CET
  argument: action=wrapper script which will be copied to SCU for execution
  argument: source= (not necessarily B0329+54)

$modified: Fri 16 Jan 2015 15:57:07 CET
  make sure first observation is after the present time

$modified: Thu 12 Feb 2015 17:42:52 CET
  add --nohorizon option so we can do the Sun

$modified: Mon 23 Feb 2015 12:59:55 CET
  more margin for first observation
  add Calibration parameters capturer
  bug fix: "test" argument was too all encompassing

$modified: Fri 13 Mar 2015 01:23:04 CET
  add trigger option: rise, transit, or set
  add duration option

$modified: Fri 13 Mar 2015 08:52:14 CET
  changing wait times before commanding (SCUstartTime_delta, etc)
  making it less conservative

$modified: Fri 13 Mar 2015 16:03:30 CET
  bug fix: okay for 1 day.

$modified: Fri 27 Mar 2015 15:18:54 CET
  possible bug fix: extra delay before starting cal parms capturer

$modified: Mon 08 Jun 2015 17:50:29 CEST
  can give a specific date for trigger as well as keywords 'rise', 'set', 'transit'

$modified: Wed 10 Jun 2015 17:41:04 CEST
  bug fix for error handling if given invalid date

$modified: Fri 26 Jun 2015 17:00:37 CEST
  bug fix: exit if unknown argument.
  "date" is accepted as argument (synonym for "trigger")
  bug fix: number of days when trigger is a specific date

$modified: Sun 12 Jul 2015 21:30:21 CEST
  sleep and wakeup tiles

$modified: Mon 20 Jul 2015 14:17:45 CEST
  bug fix:  allow non astro object like Afristar

$modified: Tue 21 Jul 11:30:05 CEST 2015
  options: power up, power down

$modified: Mon 27 Jul 2015 17:13:14 CEST
  at job number is recorded in submitlog

$modified: Mon 31 Aug 2015 16:25:33 CEST
  no need to clear processes before starting LCU and SCU procs.  The kills are in the startup wrapper.

$modified: Thu 21 Jul 2016 11:30:40 CEST
  periodicity option:  period=1 by default (everyday).

$modified: Wed 27 Jul 2016 15:37:33 CEST
  periodicity correction:  starts on first day instead of nth day (where n=period)

$modified: Sun 28 Aug 2016 12:47:32 CEST
  wakeup include powerup, so we no longer send a separate job to the SCU for powerup
  wakeup duration increased by 10 minutes
  sleepmode includes powerdown, so we no longer send a separate job to the SCU for powerdown

$modified: Mon 29 Aug 2016 09:21:11 CEST
  Now it works when invoked from the SCU.  No need to be on a remote station.

$modified: Mon 26 Sep 2016 14:51:50 CEST
  powerupTime_delta : increased to 60 minutes to allow more warm up time

"""
import sys,os,socket,time,subprocess,traceback
import datetime as dt
import ephem as eph
import urllib as url
import math
import re
from datefunctions import *
from ObsFunctions import *

helptxt='\n\nSetup observations for daily submission using "at" on the LCU and the SCU'\
    +'\nYou should have your certificates setup for no-password login to LCU (and SCU if remote)'\
    +"\nIt's best to run this from the SCU"\
    +'\noptions:'\
    +'\n\n--source=<source identifier>'\
    +'\n   source identifier is required'\
    +'\n\n--ndays=<N>'\
    +'\n   number of days to repeat the observation (default is 1 day)'\
    +'\n\n--period=<N>'\
    +'\n   periodicity in integer days (default is period=1 ie. everyday)'\
    +'\n\n--action=<path to action script>'\
    +'\n   action script is required.  Generally, this is a wrapper script\n   calling an observation script with the necessary arguments'\
    +'\n\n--trigger=<rise,transit,set>'\
    +'\n   daily observations can trigger on the time for source rise, transit, or set'\
    +'\n   alternatively, a specific date can be given.  e.g. --date=15:20'\
    +'\n\n--duration=<N>'\
    +'\n   duration of the observation in minutes (default is 120 minutes)'\
    +'\n\n--powerup'\
    +'\n   power up before the observation (default:  do *not* send power up commands)'\
    +'\n\n--powerdown'\
    +'\n   power down after the observation (default: leave everything running)'\
    +'\n'
    
powerupTime_delta=dt.timedelta(minutes=60)

wakeupTime_delta=dt.timedelta(minutes=30)

SCUclearTime_delta=dt.timedelta(minutes=10)
LCUclearTime_delta=dt.timedelta(minutes=9)

LCUstartTime_delta=dt.timedelta(minutes=8)
SCUstartTime_delta=dt.timedelta(minutes=3)
SCUcalparmsStartTime_delta=dt.timedelta(minutes=2)

CmdTime_delta=dt.timedelta(minutes=1)

SCUstopTime_delta=dt.timedelta(minutes=1)
LCUstopTime_delta=dt.timedelta(minutes=2)

sleepTime_delta=dt.timedelta(minutes=3)

powerdownTime_delta=dt.timedelta(minutes=5)

funcobj=ObsFunctions(beam='None')
if not funcobj.Scriptname(sys.argv[0]):quit()

# parse arguments
testmode=False
ndays=1
local_action_script=None
source=None
trigger='TRANSIT'
valid_triggers=['RISE','TRANSIT','SET']
do_powerup=False
do_powerdown=False
do_sleep=False
do_wakeup=False
duration=dt.timedelta(minutes=120)
period=1
for arg in sys.argv:
    #print '** DEBUG ** : argument = ',arg
    if arg==sys.argv[0]:continue

    if arg.find("--help")==0:
        print helptxt
        quit()

    if arg.find("--ndays=")==0:
        ndays_str=arg.split('=')[1]
        ndays=eval(ndays_str)
        continue

    if arg.find("--source=")==0:
        source=arg.split('=')[1]
        continue
    
    if arg.find("--action=")==0:
        wrapper=arg.split('=')[1]
        if os.path.exists(wrapper):
            local_action_script=wrapper
        else:
            local_action_script=None
            funcobj.log('ERROR: action script not found: '+wrapper)
        continue

    if arg.find("--nohorizon")==0:
        funcobj.embraceNancay.horizon=0.0
        continue

    if arg.find("--horizon=")==0:
        horizon=math.radians(eval(arg.split('=')[1]))
        funcobj.embraceNancay.horizon=horizon
        continue

    if arg.find("--trigger=")==0:
        trigger=arg.split('=')[1].upper()
        continue

    if arg.find("--date=")==0:
        trigger=arg.split('=')[1].upper()
        continue

    if arg.find("--duration=")==0:
        duration=dt.timedelta(minutes=eval(arg.split('=')[1]))
        continue
    
    if arg.find("--powerup")==0:
        do_powerup=True
        continue

    if arg.find("--powerdown")==0:
        do_powerdown=True
        continue

    if arg.find("--sleep")==0:
        do_sleep=True
        continue

    if arg.find("--wakeup")==0:
        do_wakeup=True
        continue

    if arg.find("--period=")==0:
        period=eval(arg.split('=')[1])
        continue

    if arg.upper().find("--TEST")==0:
        testmode=True
        continue

    print helptxt
    print 'unknown argument: ',arg
    quit()
    

if source==None:
    funcobj.log('ERROR: must provide a source')
    quit()

if local_action_script==None:
    funcobj.log('ERROR: must provide a valid action script')
    quit()

if trigger not in valid_triggers:
    # check if trigger is a date:
    trigger_arg=trigger
    trigger_date=str2dt(trigger)
    trigger='DATE'
    if trigger_date==None:
        funcobj.log('ERROR: invalid trigger: '+trigger_arg)
        quit()                        
    if trigger_date < dt.datetime.utcnow():
        funcobj.log('ERROR: trigger date is in the past! '+isodate(trigger_date))
        quit()


        
if testmode:
    funcobj.log("RUNNING IN TESTMODE: NOT SUBMITTING JOBS")

netnames=whichNetwork()

# assign start_delta which depends on what type of trigger, and the duration of the observation
if trigger=='RISE' or trigger=='DATE':
    start_delta=dt.timedelta(minutes=0.0)
elif trigger=='TRANSIT':
    start_delta=duration/2
else:
    start_delta=duration

# if doing a power up, then you also have to wake up afterwards
if do_powerup:do_wakeup=True

# Sun 28 Aug 2016 12:46:40 CEST
# we no longer do power up as a separate action.  It is part of the wakeup script on the LCU
do_powerup=False

# Sun 28 Aug 2016 12:56:49 CEST
# we no longer de a separate power down after sleep mode.  sleepmode includes the power down.
if do_powerdown:do_sleep=True
do_powerdown=False

if do_powerup:
    leadTime_delta=powerupTime_delta
elif do_wakeup:
    leadTime_delta=wakeupTime_delta
else:
    leadTime_delta=SCUclearTime_delta

####### 
#action_script=FullPathname('do_B0329.sh')
#if action_script==None:quit()
#action_script="/home/torchinsky/do_B0329.sh"
######

# copy action script to SCU
action_script='obslogs/do_'+source+'_'+dt.datetime.utcnow().strftime('%Y%m%d-%H%M')
if netnames['scu']=="localhost":
    action_script="$HOME/"+action_script
    cmd="cp -pv "+local_action_script+" "+action_script
else:
    cmd="scp -p "+local_action_script+" "+netnames['scu']+":"+action_script
if not testmode: submit_process(cmd) 

dtnow=dt.datetime.utcnow()
ndays_delta=dt.timedelta(days=ndays)
endobs=dtnow+ndays_delta

trigger_times=[]

if trigger=='DATE':
    CmdTime=trigger_date
    endobs=trigger_date+duration+sleepTime_delta
    verify_date=trigger_date
    start_date=trigger_date
    trigger_times=[trigger_date]
else:
    CmdTime=dtnow
    verify_date=dtnow
    start_date=dtnow

if source.upper()!='AFRISTAR':
    srcParms=funcobj.verifySource(source,verify_date,funcobj.offsource)
    if srcParms==None: quit()
    srcEph=srcParms['srcEph']
    offsrcEph=srcParms['offEph']
    
period_counter=0
while trigger!='DATE' and CmdTime < endobs:
    src_rise,src_transit,src_set=funcobj.next_transit(srcEph,True,date=CmdTime)
    if src_rise==None: quit()

    if trigger=='RISE':
        src_trigger=src_rise
    elif trigger=='TRANSIT':
        src_trigger=src_transit
    elif trigger=='SET':
        src_trigger=src_set
    elif trigger=='DATE':
        src_trigger=trigger_date
    else:
        print 'ERROR: invalid trigger.  We should never get here!'
        quit()
    

    StartTime=src_trigger-start_delta-leadTime_delta
    StopTime=src_trigger-start_delta+duration+LCUstopTime_delta
    if StartTime > dtnow and StopTime <= endobs:
        print 'period_counter=',period_counter
        if period_counter == 0:
            trigger_times.append(src_trigger)
        period_counter+=1
        if period_counter >= period:
            period_counter=0

    CmdTime=src_set
    if CmdTime<src_transit:CmdTime=src_transit
    if CmdTime<src_rise:CmdTime=src_rise
    CmdTime+=dt.timedelta(hours=1)

for src_trigger in trigger_times:
    StartTime=src_trigger-start_delta

    leadTime=StartTime-leadTime_delta
    if leadTime <= dtnow:
        funcobj.log('ERROR:  Tile powerup/wakeup or SCU start time is in the past!')
        quit()

    powerupTime=StartTime-powerupTime_delta
    wakeupTime=StartTime-wakeupTime_delta
        
    SCUclearTime=StartTime-SCUclearTime_delta
    LCUclearTime=StartTime-LCUclearTime_delta

    LCUstartTime=StartTime-LCUstartTime_delta
    SCUstartTime=StartTime-SCUstartTime_delta
    SCUcalparmsStartTime=StartTime-SCUcalparmsStartTime_delta

    CmdTime=StartTime-CmdTime_delta

    SCUstopTime=StartTime+duration+SCUstopTime_delta
    LCUstopTime=StartTime+duration+LCUstopTime_delta

    sleepTime=StartTime+duration+sleepTime_delta
    powerdownTime=StartTime+duration+powerdownTime_delta
    
    # setup the "at" jobs
    if do_powerup:
        funcobj.log(isodate(powerupTime)+" power up the tiles")
        if not testmode:
            logtxt=powerupTiles(powerupTime)
            for txt in logtxt:funcobj.log(txt)
            
    if do_wakeup:
        funcobj.log(isodate(wakeupTime)+" wake up the tiles")
        if not testmode:
            logtxt=wakeup(wakeupTime)
            for txt in logtxt:funcobj.log(txt)

    """
    #### clearing stats capturer and cal parms capturer is not necessary as a separate job.
    #### this is done in the startup wrappers
    funcobj.log(isodate(SCUclearTime)+" stop Statistics Capturer")
    if not testmode:
        logtxt=stopStatisticsCapturer(SCUclearTime)
        for txt in logtxt:funcobj.log(txt)
        
    funcobj.log(isodate(SCUclearTime)+" stop Calibration Parameters Capturer")
    if not testmode:
        logtxt=stopCalparmsCapturer(SCUclearTime)
        for txt in logtxt:funcobj.log(txt)
    """
    
    # if we've done the power up sequence, then there's no reason to stop and restart the LCU servers
    if (not do_powerup) and (not do_wakeup):

        """
        #### no need to clear LCU processes.  This is done in the startup wrapper. the following commented out.
        funcobj.log(isodate(LCUclearTime)+" stop LCU processes")
        if not testmode:
            logtxt=stopLCUprocs(LCUclearTime)
            for txt in logtxt:funcobj.log(txt)
        """
            
        funcobj.log(isodate(LCUstartTime)+" start LCU processes")
        if not testmode:
            logtxt=startLCUprocs(LCUstartTime)
            for txt in logtxt:funcobj.log(txt)

    funcobj.log(isodate(SCUstartTime)+" start Statistics Capturer")
    if not testmode:
        logtxt=startStatisticsCapturer(SCUstartTime)
        for txt in logtxt:funcobj.log(txt)

    funcobj.log(isodate(SCUcalparmsStartTime)+" start Calibration Parameters Capturer")
    if not testmode:
        logtxt=startCalparmsCapturer(SCUcalparmsStartTime)
        for txt in logtxt:funcobj.log(txt)
    
    funcobj.log(isodate(CmdTime)+" submit observation")
    if not testmode:
        logtxt=scucommand(at=CmdTime,action=action_script)
        for txt in logtxt:funcobj.log(txt)

    funcobj.log(isodate(SCUstopTime)+" stop Statistics Capturer")
    if not testmode:
        logtxt=stopStatisticsCapturer(SCUstopTime)
        for txt in logtxt:funcobj.log(txt)
        
    funcobj.log(isodate(SCUstopTime)+" stop Calibration Parameters Capturer")
    if not testmode:
        logtxt=stopCalparmsCapturer(SCUstopTime)
        for txt in logtxt:funcobj.log(txt)
        
    if do_sleep:
        funcobj.log(isodate(sleepTime)+" tiles go to sleep mode")
        if not testmode:
            logtxt=shutdownmode(sleepTime)
            for txt in logtxt:funcobj.log(txt)
    elif do_powerdown:
        funcobj.log(isodate(sleepTime)+" tiles commanded to shutdown mode")
        if not testmode:
            logtxt=shutdownmode(sleepTime)
            for txt in logtxt:funcobj.log(txt)
        funcobj.log(isodate(powerdownTime)+" 48V to tiles is switched off")
        if not testmode:
            logtxt=poweroffTiles(powerdownTime)
            for txt in logtxt:funcobj.log(txt)
    else:
        funcobj.log(isodate(LCUstopTime)+" stop LCU processes")
        if not testmode:
            logtxt=stopLCUprocs(LCUstopTime)
            for txt in logtxt:funcobj.log(txt)

