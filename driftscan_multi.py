#! /usr/bin/env python
"""
$Id: driftscan_multi.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Tue 02 Jul 2013 16:58:39 CEST

started from template:  driftscan.py (last mod: Tue Jul  2 16:52:56 CEST 2013)

very long drift scan using multiple digital beams
point in grid (half spacing in elevation, fullspacing in az)

     2y 3y
  1x 2x 3x
     1y 

calibration is done the same for all digital beams.

$modified: Mon 22 Jul 2013 17:23:31 CEST
  totally sidetracked... now it's nearly ready

$modified: Tue 23 Jul 2013 14:55:20 CEST
  still problems with the old bug about next_transit in PyEphem.  see also ObsFunction.py

$modified: Thu 05 Sep 2013 11:55:44 CEST
  this script has been attempted three times without success
  Is it a bug in the LCU? or is it here?
  the calibration seems to work, and the subsequent drift scan, with all beams co-aligned.

  bug fix:  digital beam az el was calculated at the wrong time.
  BE CAREFUL about changes to ephem.date when doing calculations, such as next_transit
  
  using src_peaktime as is done in driftscan.py

$modified: Fri 06 Sep 2013 14:45:11 CEST
  create RF beams is now in satorchi_OBSinit.py

$modified: Sat 07 Sep 2013 18:21:25 CEST
  implementing separate A and B pointing
  increasing to 4 digital beams from 3, per RF beam

$modified: Mon 06 Jan 2014 17:29:22 CET
  modifications for changes to ObsFunctions, in particular, obsfunc object for each beam

$modified: Wed 16 Apr 2014 12:58:51 CEST  
  yoffset for cal drift scan is passed via obsfunc for each beam.

$modified: Fri 16 Jan 2015 15:57:43 CET
  bug: missing createRFBeam()

"""
helptxt='drift scan of an astronomical source with multibeam pointing'


# pointing offsets are RA and dec from the RF centre pointing of each Beam
# so far, these are completely independent.
# eventually, try to line-up A and B if source['A']==source['B']
pointing_offset={}
pointing_offset['A']={}
pointing_offset['B']={}
pointing_offset['A']['X']=[( 3.0, 0.0),
			   ( 1.5, 0.0),
			   ( 0.0, 0.0),
			   (-1.5, 0.0)]
pointing_offset['A']['Y']=[( 0.0,-0.75),
			   ( 0.0, 0.75),
			   ( 1.5,-0.75),
			   ( 1.5, 0.75)]
pointing_offset['B']['X']=[( 3.0, 0.0),
			   ( 1.5, 0.0),
			   ( 0.0, 0.0),
			   (-1.5, 0.0)]
pointing_offset['B']['Y']=[( 0.0,-0.75),
			   ( 0.0, 0.75),
			   ( 1.5,-0.75),
			   ( 1.5, 0.75)]

### Thu 05 Sep 2013 11:54:34 CEST
### for testing, use tiny offsets
#pointing_offset['X']=[(-0.1, 0.0),
#		      ( 0.0, 0.0),
#		      ( 0.1, 0.0)]
#pointing_offset['Y']=[( 0.0,-0.05),
#		      ( 0.0, 0.05),
#		      ( 0.1, 0.05)]

### Wed 14 Jan 2015 17:15:12 CET
### lined up pointing for multi drift scan of CasA everyday
pointing_offset['A']['X']=[( 3.0, 0.0),
			   ( 1.5, 0.0),
			   ( 0.0, 0.0),
			   (-1.5, 0.0)]
pointing_offset['A']['Y']=[( 4.5, 0.0),
			   (-3.0, 0.0),
			   (-4.5, 0.0),
			   (-6.0, 0.0)]
pointing_offset['B']['X']=[( 3.0, 0.0),
			   ( 1.5, 0.0),
			   ( 0.0, 0.0),
			   (-1.5, 0.0)]
pointing_offset['B']['Y']=[( 4.5, 0.0),
			   (-3.0, 0.0),
			   (-4.5, 0.0),
			   (-6.0, 0.0)]


import sys,os
import datetime as dt
import ephem as eph
from math import *
from datefunctions import *

# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================
	
# === one Observation title for both beams ============================
# source name given on the command line, there is no default
ObsTitle=''
for beam in beams:
	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	comment="cal on "+obsfunc[beam].satname+", and drift scan "+ObsTitle
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc[beam].initialize(ObsTitle):quit()
# =====================================================================

beam_driftdate=None
src_peaktime=None
for beam in beams:
	obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
	AcqStarts=[]
	AcqEnds=[]

# ======== make RFBeam and digital beam objects =======================
        RFBeam=obsfunc[beam].makeRFBeam()
        ndigbeams=len(pointing_offset[beam]['X'])

	# divide the number of beamlets evenly between digital beams
	beamlets_per_digbeam=obsfunc[beam].nBeamlets()/ndigbeams
        # give the extra ones to the first digbeam
	remainder=obsfunc[beam].nBeamlets()-ndigbeams*beamlets_per_digbeam
	DigBeam=[]
	for digbeam_index in range(ndigbeams):
		if digbeam_index==0:nbeamlets=beamlets_per_digbeam+remainder
		else: nbeamlets=beamlets_per_digbeam
		DigBeam.append(obsfunc[beam].makeDigitalBeam(nbeamlets=nbeamlets))
		if DigBeam[digbeam_index]==None: quit()
# ========= End digital beam config ====================================


	# satellite calibration with driftscan
	# satcal also chooses the frequencies, if not specifically given
	satcalParms=obsfunc[beam].satcal(digcal=digcal,
					 driftscan=do_satdrift,
					 driftdate=beam_driftdate,
					 now=now)
	if satcalParms==None: quit()
	if not satcalParms['PointingStatus']:
		obsfunc[beam].log("\nCould not complete the satellite calibration sequence")
		quit()
	obstag.append(satcalParms['satname'].replace(' ',''))
	beam_caldate=satcalParms['satcalEnd']
	beam_driftdate=satcalParms['driftdate']
	calphaseEnd=satcalParms['satcalEnd']+obsfunc[beam].caldriftduration

	satcalStart=satcalParms['satcalStart']

	# calibration and drift data acquisition times
	AcqStarts.append(satcalStart-dt.timedelta(minutes=1))

	comment='calibration phase for Beam-'+beam+' ends at '+isodate(calphaseEnd)
	obsfunc[beam].log(comment)


# ========= astronomical source pointing =======
	StartTime=calphaseEnd

	# duration of the observation, if none given, default 24 hours
	if duration==None: duration=dt.timedelta(minutes=24*60.)
	endobs=calphaseEnd+duration
	AcqEnds.append(endobs)


	# find pointing for next transit
	srcParms=obsfunc[beam].verifySource(obsfunc[beam].source,StartTime,obsfunc[beam].offsource)
	if srcParms==None: quit()
	obstag[0]+='--'+obsfunc[beam].source_fullname(obsfunc[beam].source)

	srcEph=srcParms['srcEph']
	isAstroSrc=srcParms['isAstroSrc']


	# find the RA and dec (to be given in degrees)
	# and the azimuth-elevation at the start time
	srcEph.compute(obsfunc[beam].embraceNancay)
	src_ra=degrees(srcEph.a_ra)
	src_dec=degrees(srcEph.a_dec)
	obsfunc[beam].log(srcEph.name+" RA and dec in degrees: "+str(src_ra)+" "+str(src_dec))
	obsfunc[beam].log("at "+isodate(obsfunc[beam].embraceNancay.date.datetime())+" "+srcEph.name
		    +" is at: az="+str(degrees(float(srcEph.az)))
		    +" alt="+str(degrees(float(srcEph.alt))))


	# rise, set, and transit times
	src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,isAstroSrc,StartTime)
	if src_transit==None:quit()

	src_peaktime=src_transit
	if src_transit > endobs:
		comment='requested duration is not long enough to point at transit\n'\
		    +'pointing for driftscan at mid-duration'
		obsfunc[beam].log(comment)
		src_peaktime=StartTime+duration/2


	# find pointing az,el
	obsfunc[beam].embraceNancay.date=src_peaktime
	srcEph.compute(obsfunc[beam].embraceNancay)
	comment='computing pointing at time: '+isodate(obsfunc[beam].embraceNancay.date.datetime())
	obsfunc[beam].log(comment)
	src_az=degrees(srcEph.az)
	src_alt=degrees(srcEph.alt)
	src_ra=degrees(srcEph.a_ra)
	src_dec=degrees(srcEph.a_dec)
	comment='  az='+str(src_az)+' alt='+str(src_alt)
	obsfunc[beam].log(comment)

	CmdTime=StartTime
	comment='pointing by AZEL beginning at '+isodate(CmdTime)
	obsfunc[beam].log('\n'+comment)
	obsfunc[beam].addFITScomment(comment)

        ## indicate further transits
	next_transit=src_transit
	next_set=src_set
	while next_transit < endobs:
		comment='! peak should be at '+isodate(next_transit)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)
		date=next_set+dt.timedelta(minutes=1)
		next_rise,next_transit,next_set=obsfunc[beam].next_transit(srcEph,isAstroSrc,date)

		# hack to get around pyephem bug on next_pass :
		# see https://bugs.launchpad.net/pyephem/+bug/572402
		if next_rise==None or next_transit==None or next_set==None: break



	# RF beam tracking
	RFBeam.SetCal('current')
	RFBeam.AddSubscan(CmdTime,'AZEL',src_az,src_alt)
	comment='RF pointing for Beam-'+beam+' at '+isodate(CmdTime)\
	    +': az='+str(src_az)+' alt='+str(src_alt)
	obsfunc[beam].log(comment)
	for digbeam_index in range(ndigbeams):
		DigBeam[digbeam_index].SetCal('current')



	az={}
	alt={}
	obsfunc[beam].embraceNancay.date=src_peaktime
	for digbeam_index in range(ndigbeams):
		for direction in ['X','Y']:
			ra_deg = src_ra  + pointing_offset[beam][direction][digbeam_index][0]
			dec_deg= src_dec + pointing_offset[beam][direction][digbeam_index][1]
			angDist=obsfunc[beam].angularSeparation(radians(src_ra),radians(src_dec),
								radians(ra_deg),radians(dec_deg))
			comment='dig beam '+str(digbeam_index)\
			    +': direction '+direction\
			    +': separation from RF pointing: '+str(angDist)+' degrees'
			obsfunc[beam].log(comment)

			# convert RA degrees to hours
			ra=ra_deg*24/360.
			ra_H=int(ra)
			ra_M=int((ra-ra_H)*60)
			ra_S=60*((ra-ra_H)*60 - ra_M)
			dec_H=int(dec_deg)
			dec_M=int((dec_deg-dec_H)*60)
			dec_S=60*((dec_deg-dec_H)*60 - dec_M)

			# create a source line for pyephem
			line='POINTING,f,'+str(ra_H)+':'+str(ra_M)+':'+str(ra_S)+','\
			    +str(dec_H)+':'+str(dec_M)+':'+str(dec_S)+',0,2000'
			obsfunc[beam].log(line)
			dummyEph=eph.readdb(line)
			dummyEph.compute(obsfunc[beam].embraceNancay)
			comment='computing pointing at time: '+isodate(obsfunc[beam].embraceNancay.date.datetime())	
			obsfunc[beam].log(comment)
			az[direction]=degrees(dummyEph.az)
			alt[direction]=degrees(dummyEph.alt)

		DigBeam[digbeam_index].AddSubscan(CmdTime,'AZEL',
							az['X'],alt['X'],az['Y'],alt['Y'])
		comment='pointing by azel at '+isodate(CmdTime)\
		    +str(': %.3f %.3f %.3f %.3f' % (az['X'],alt['X'],az['Y'],alt['Y']))
		obsfunc[beam].log(comment)

# === end pointing configuration ==================

# === Define  STATISTICS ACQUISITION phases ============================
	if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
		print "\nObservation NOT submitted\n"
		quit()
# ======================================================================

# === finally, submit the observation ===========
obsfunc[beam].submit()

