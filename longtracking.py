#! /usr/bin/env python
"""
$Id: longtracking.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Fri 24 Aug 2012 17:45:11 CEST

Track a source while visible

Henrik's tracking suggestion:
RF pointing at 1 beam ahead of CasA
digital beam X pointing on CasA
digital beam Y pointing one beam ahead of CasA

This way we have symmetric beams for on and off
and we take care of the baseline tracking wobble 
by subtracting the off position which should display the same wobble
(see eg. Anti-CasA tracking)

$modified: Fri 24 Aug 2012 17:47:10 CEST
  copied from CasA_longtracking.py (last mod: Fri Aug 24 17:12:40 CEST 2012)
  removed Tobia mode
  RFpointing and pre- for offsource is generated here rather than reading from catalog

$modified: Sun 26 Aug 2012 12:09:07 CEST
  add RF off pointing for 30 mins after the end of the observation

$modified: Mon Aug 27 13:10:30 CEST 2012
  turned on drift scan during satellite calibration
  added satellite tracking after satellite drift scan
  add half drift scan:  pointing at horizon waiting for src rise

$modified: Wed 29 Aug 2012 15:22:25 CEST
  increase drift scan time so that it begins and ends outside the RF beam
  (10 degree skytrail:  ie. approx 1.5 degree before and after RF beam lobe)

$modified: Thu 30 Aug 2012 18:13:20 CEST
  fixed bug: didn't start acquisition for horizon pointing drift scan

  drift scan starts later, not at transit

$modified: Tue 04 Sep 2012 13:56:17 CEST
  fixed bug:  drift scan was pointing at transit location, but not at transit time
              drift scan pointing at correct location

$modified: Mon 14 Jan 2013 11:45:40 CET
  initialization generalized for any user
  updated for use with new version of satcal() etc

  configuring both beams.  For now, the two beams are following the same source

$modified: Tue 22 Jan 2013 09:18:05 CET
  removed rfcaldelta etc, which is done in satcal() and returned in satcalParms

$modified: Mon 11 Feb 2013 13:44:28 CET
  using obsfunc.initialize() and obsfunc.submit()

$modified: Fri 15 Feb 2013 15:28:22 CET
  using FullPathname()

  removing FindSat() which is now done in satorchi_OBSinit.py

$modified: Sat 16 Feb 2013 07:49:14 CET
  calibration begins with caldate or, if it was not defined, then with obsdate

$modified: Tue 27 Aug 2013 18:46:27 CEST
  tot_seconds() : is no longer in ObsFunctions.py.  it is on datefunctions.py

$modified: Thu 05 Sep 2013 23:24:31 CEST
  obsfunc.source renamed to obsfunc.sourcecat, and no need to initialize here

$modified: Wed 11 Sep 2013 16:34:49 CEST
  RFBeam creation is in satorchi_OBSinit.py

$modified: Wed 16 Apr 2014 13:21:07 CEST
  yoffset for cal drift scan is passed via obsfunc for each beam.
  
"""
helptxt="Track a source while visible\n\n"\
    +"Henrik's tracking suggestion:\n"\
    +"RF pointing at 1 beam ahead of CasA\n"\
    +"digital beam X pointing on CasA\n"\
    +"digital beam Y pointing one beam ahead of CasA\n\n"\
    +"This way we have symmetric beams for on and off\n"\
    +"and we take care of the baseline tracking wobble\n"\
    +"by subtracting the off position which should display the same wobble"

import sys,os
import datetime as dt
import ephem as eph
import math
from datefunctions import *

# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================
		
# ===== setup comments and obstags ====================================
if source==None:source="CasA" 
if not source in obsfunc.sourcecat.keys():
	obsfunc.log('source unknown: '+source)
	quit()
ObsTitle=source 

comments=[]
comments.append("track "+source)
comments.append("cal on "+obsfunc.satname+", and track a bit")
for comment in comments:
	obsfunc.addFITScomment(comment)

obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
obstag.append(obsfunc.satname.replace(' ',''))
# =====================================================================

# create holders for acquisition start and end times
AcqStarts=[]
AcqEnds=[]

# ====== Initialize MyObservation  ====================================
if not obsfunc.initialize(ObsTitle):quit()
# =====================================================================


# ==== Define directions, times, obsparams ============================
# Desired number of beamlets per dig.beam bandpass
# ObsFunc also automatically sets the associated bandwidth
obsfunc.nBeamlets(nbeamlets)     
bw=obsfunc.Bandwidth()
# =====================================================================

# ======== Start digital beam config ===================================
DigBeam={}
for beam in beams:
	DigBeam[beam]=obsfunc.makeDigitalBeam(beam=beam)
	if DigBeam[beam]==None: quit()
# ========= End digital beam config ====================================


# generate the pointings for satellite calibration with driftscan
# satcal also chooses the frequencies, if not specifically given
# note that obsfunc.FindSat() must have been previously called to set the satname

beam_caldate=caldate
if beam_caldate==None:beam_caldate=obsdate
satcalParms={}
for beam in beams:
	satcalParms[beam]=obsfunc.satcal(obsdate=beam_caldate,
					 driftscan=True,
					 digcal=digcal,
					 now=now)
	if satcalParms[beam]==None: quit()
	beam_caldate=satcalParms[beam]['satcalEnd']


# acquisition start time is the start of the cal for the first beam
satcalStart=satcalParms[beams[0]]['satcalStart']
# end time is the end of the cal for the last beam
satcalEnd=satcalParms[beams[len(beams)-1]]['EndTime']

# gps calibration and gps drift data acquisition times
AcqStarts.append(satcalStart-dt.timedelta(minutes=1)) # begin a minute before


# now track the satellite for 15 minutes
for beam in beams:
	StartTime,endTracking=obsfunc.SourceTrack(obsfunc.satname,
						  satcalEnd,
						  OffBeam=None,
						  duration=dt.timedelta(minutes=15),
						  calibration_interval=None,
						  digtrack=True)
	if StartTime==None:quit()
	if endTracking==None:quit()

AcqEnds.append(endTracking)


# ========= astronomical source pointing =======
# for the source tracking,
# calculate the next transit from after the above calibration has been done
StartTime=endTracking+dt.timedelta(seconds=10) # plus some margin

# if obsdate given specifically, than use it, but only if it makes sense
if not obsdate==None:
	if obsdate>StartTime:StartTime=obsdate


# tracking of source
srcEph=obsfunc.sourcecat[source]
rises,transits,sets=obsfunc.next_transit(srcEph,True,date=StartTime)

obsfunc.embraceNancay.date=StartTime
srcEph.compute(obsfunc.embraceNancay)
src_ra=obsfunc.rad2deg*srcEph.a_ra
src_dec=obsfunc.rad2deg*srcEph.a_dec

# check if we can start right away, otherwise, start at rise time
if srcEph.alt<obsfunc.embraceNancay.horizon:
	obsfunc.log("cannot start now: "+str(StartTime)+" "+source+" is not visible: alt="+str(srcEph.alt))
	StartTime=rises

# the duration is until the source sets, unless duration given
if duration==None:
	endobs=sets
else:
	endobs=StartTime+duration


# az/alt will be used for pointing before tracking starts
# to do a drift scan while waiting for source to rise
obsfunc.embraceNancay.date=StartTime
srcEph.compute(obsfunc.embraceNancay)
horizon_alt=obsfunc.rad2deg*srcEph.alt
horizon_az=obsfunc.rad2deg*srcEph.az

# make an offset beam, 3 degrees ahead (two beams)
off_ra=src_ra+3
off_dec=src_dec

# make the RF pointing beam, 1.5 degrees ahead (one beam)
RFpointing_ra=src_ra+1.5
RFpointing_dec=src_dec

# start pointing half hour before
HorizonTime=StartTime-dt.timedelta(minutes=30)
if HorizonTime<endTracking:
	HorizonTime=endTracking

# do a drift scan, through a whole RF beam
skyrate=15.0*math.cos(math.radians(src_dec)) # minutes/degree
# beamsize 7 degrees, start and end outside RF beam: say 10 deg
driftduration=dt.timedelta(minutes=skyrate*10)

## drift scan at transit is no good for Cygnus-A, there's F2 passing then
## driftstart=transits-driftduration/2
# Do drift scan 3/4 through observation
driftstart=3*(endobs-StartTime)/4+StartTime
driftend=driftstart+driftduration
driftPointingTime=driftstart+driftduration/2
obsfunc.embraceNancay.date=driftPointingTime
srcEph.compute(obsfunc.embraceNancay)
drift_alt=obsfunc.rad2deg*srcEph.alt
drift_az=obsfunc.rad2deg*srcEph.az

# the pointing commands
for beam in beams:
	RFBeam[beam].SetCal('current')
	DigBeam[beam].SetCal('current')

if HorizonTime<StartTime:
	comment=source+' drift scan beginning at '+str(HorizonTime)+" UT.  pointing at horizon"
	obsfunc.log(comment)
	obsfunc.log("  az="+str(horizon_az)+" alt="+str(horizon_alt))
	obsfunc.addFITScomment(comment)
        comment="! peak should be at "+str(StartTime)+" UT"
	obsfunc.log(comment)
	obsfunc.addFITScomment(comment)
	for beam in beams:
		RFBeam[beam].AddSubscan(HorizonTime,'AZEL',horizon_az,horizon_alt)
		DigBeam[beam].AddSubscan(HorizonTime,'AZEL',horizon_az,horizon_alt,horizon_az,horizon_alt)

comment=source+" tracking by J2000 at "+str(StartTime)+" UT"
obsfunc.log(comment)
obsfunc.log("  RA="+str(src_ra)+" dec="+str(src_dec))
obsfunc.addFITScomment(comment)
comment="RF tracking one beam ahead of "+source
obsfunc.log(comment)
obsfunc.log("  RA="+str(RFpointing_ra)+" dec="+str(RFpointing_dec))
obsfunc.addFITScomment(comment)
comment="off source is two beams ahead of "+source
obsfunc.log(comment)
obsfunc.log("  RA="+str(off_ra)+" dec="+str(off_dec))
obsfunc.addFITScomment(comment)
for beam in beams:
	RFBeam[beam].AddSubscan(StartTime,'J2000',RFpointing_ra,RFpointing_dec)
	DigBeam[beam].AddSubscan(StartTime,'J2000',src_ra,src_dec,off_ra,off_dec)

#
comment=source+" drift scan beginning at "+str(driftstart)+" UT"
obsfunc.log(comment)
obsfunc.log("  az="+str(drift_az)+" alt="+str(drift_alt))
obsfunc.addFITScomment(comment)
comment="! peak should be at "+str(driftPointingTime)+" UT"
obsfunc.log(comment)
obsfunc.addFITScomment(comment)
for beam in beams:
	RFBeam[beam].AddSubscan(driftstart,'AZEL',drift_az,drift_alt)
	DigBeam[beam].AddSubscan(driftstart,'AZEL',drift_az,drift_alt,drift_az,drift_alt)

comment=source+" tracking by J2000 at "+str(driftend)+" UT"
obsfunc.log(comment)
obsfunc.log("  RA="+str(src_ra)+" dec="+str(src_dec))
obsfunc.addFITScomment(comment)
for beam in beams:
	RFBeam[beam].AddSubscan(driftend,'J2000',RFpointing_ra,RFpointing_dec)
	DigBeam[beam].AddSubscan(driftend,'J2000',src_ra,src_dec,off_ra,off_dec)


# after the source tracking, point to transit location for 30 mins
# for empty sky
CmdTime=endobs
tpointing=transits
obsfunc.embraceNancay.date=transits
srcEph.compute(obsfunc.embraceNancay)
srcTransit_alt=obsfunc.rad2deg*srcEph.alt
srcTransit_az=obsfunc.rad2deg*srcEph.az
for beam in beams:
	RFBeam[beam].SetCal('current')
	DigBeam[beam].SetCal('current')
	RFBeam[beam].AddSubscan(CmdTime,'AZEL',srcTransit_az,srcTransit_alt)
	DigBeam[beam].AddSubscan(CmdTime,'AZEL',srcTransit_az,srcTransit_alt,srcTransit_az,srcTransit_alt)
endobs+=dt.timedelta(minutes=30)
comment='pointing at empty sky for 30 minutes starting at '+str(CmdTime)
obsfunc.log(comment)
obsfunc.addFITScomment(comment)
# === end pointing configuration ==================

# === Define  STATISTICS ACQUISITION phases ======


# if the main observation follows directly from the calibration
# make a single acquisition phase for both
if HorizonTime<StartTime:
	waitTime=HorizonTime-endTracking
else:
	waitTime=StartTime-endTracking
waitTimeMins=tot_seconds(waitTime)/60.0
obsfunc.log("time between satellite end tracking and "+source+" track start: "+str(waitTimeMins)+" minutes")
if waitTime < dt.timedelta(minutes=30):
	AcqEnds[0]=endobs
	obstag[0]+="--"+source
else:
	#we have time for a blank sky pointing before
	CmdTime=AcqEnds[0]
	AcqEnds[0]+=dt.timedelta(minutes=30)
	for beam in beams:
		RFBeam[beam].SetCal('current')
		DigBeam[beam].SetCal('current')
		RFBeam[beam].AddSubscan(CmdTime,'AZEL',srcTransit_az,srcTransit_alt)
		DigBeam[beam].AddSubscan(CmdTime,'AZEL',srcTransit_az,srcTransit_alt,srcTransit_az,srcTransit_alt)
	comment='pointing at empty sky for 30 minutes starting at '+str(CmdTime)
	obsfunc.log(comment)
	obsfunc.addFITScomment(comment)

	if HorizonTime<StartTime:
		AcqStarts.append(HorizonTime)
	else:
		AcqStarts.append(StartTime)
	AcqEnds.append(endobs)
	obstag.append(source)

for beam in beams:
	if not obsfunc.Acquire(obstag,AcqStarts,AcqEnds,statsTypes,beam=beam):
		print "\nObservation NOT submitted\n"
		quit()

# === finally, submit the observation ===========
obsfunc.submit()

