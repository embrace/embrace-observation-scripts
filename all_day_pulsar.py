"""
$Id: all_day_pulsar.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Wed 23 Apr 2014 13:21:38 CEST

high cadence pulsar observation for 24 hours: 10 minutes twice per hour

"""
import datetime as dt
import ephem as eph
import math
from datefunctions import *

helptxt='high cadence pulsar observation for 24 hours: 10 minutes twice per hour'
ndays=1
psr_acquisition_duration=dt.timedelta(minutes=10)

import sys,os
sys.argv.insert(1,'--beams=both')
sys.argv.insert(1,'--duration=120')
sys.argv.insert(1,'--driftduration=120')
sys.argv.insert(1,'--nohorizon')
sys.argv.insert(1,'--A:cal=Sun')
sys.argv.insert(1,'--A:RFcentre=970')
sys.argv.insert(1,'--A:RFcalfreq=970')
sys.argv.insert(1,'--B:cal=f2')
sys.argv.insert(1,'--source=B0329')

# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================

# === Observation title is applied for both beams =====================
ObsTitle='all day pulsar'
for beam in beams:
	obsfunc[beam].source='B0329'
	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	comment="cal on "+obsfunc[beam].satname+", and tracking "+ObsTitle
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc[beam].initialize(ObsTitle):quit()
# =====================================================================

# generate the pointings for satellite calibration with driftscan
# calibrate at next transit for each calibrator
backend_cmd=[]
FirstAcquisition=None
xmlfilename={}
runfilename={}
satcalEnd={}
endobs=None

for beam in beams:
    # initialize obstag, AcqStarts and AcqEnds for each beam
    obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
    AcqStarts=[]
    AcqEnds=[]
    obstag.append(obsfunc[beam].satname.replace(' ',''))

    # ======== get RFBeam and DigBeam objects ======================
    RFBeam=obsfunc[beam].getRFBeam()
    if RFBeam==None:quit()
    DigBeam=obsfunc[beam].makeDigitalBeam()
    if DigBeam==None: quit()
    # ==============================================================
    
    satcalParms=obsfunc[beam].satcal(digcal=digcal,
                                     driftscan=do_satdrift,
                                     now=now)
    if satcalParms==None: quit()
    if not satcalParms['PointingStatus']:
        obsfunc[beam].log("Could not complete the satellite calibration sequence")
        quit()

    satcalStart=satcalParms['satcalStart']
    satcalEnd[beam]=satcalParms['EndTime']

    # calibration and drift data acquisition times
    if FirstAcquisition==None:FirstAcquisition=satcalStart-dt.timedelta(minutes=1)
    AcqStarts.append(satcalStart-dt.timedelta(minutes=1))
    AcqEnds.append(satcalEnd[beam])

    # === align the acquistion times of the two beams, if they are within a few minutes of one another
    delta=AcqStarts[0] - FirstAcquisition
    if delta>dt.timedelta(minutes=0) and delta<dt.timedelta(minutes=45): AcqStarts[0]=FirstAcquisition

    # === statistics acquisition for calibration phase
    if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
        print "\nObservation NOT submitted\n"
        quit()

    # ==== make the same end time for both beams
    if endobs==None:
        endobs=satcalEnd[beam] + dt.timedelta(days=ndays)
    else:
        chk_endobs=satcalEnd[beam] + dt.timedelta(days=ndays)
        if chk_endobs > endobs:
            endobs=chk_endobs
# ===== end phase calibration ==========================================

# ========= astronomical source pointing ================================
for beam in beams:
    # initialize obstag, AcqStarts and AcqEnds for each beam
    obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
    AcqStarts=[]
    AcqEnds=[]

    # the first acquisition begins immediately after calibration
    CmdTime=satcalEnd[beam]

    srcParms=obsfunc[beam].verifySource(obsfunc[beam].source,CmdTime,obsfunc[beam].offsource)
    if srcParms==None: quit()
    srcEph=srcParms['srcEph']
    offsrcEph=srcParms['offEph']

    # === xml file for ARTEMIS: 10 minutes acquistion each time
    xmlfilename=obsfunc[beam].mkPulsarXML(srcEph.name,psr_acquisition_duration)
    cmd="scp -p "+xmlfilename+" __BACKEND__:EMBRACE"
    backend_cmd.append(cmd)

    runfilename=obsfunc[beam].mkRunArtemis(xmlfilename)
    cmd="scp -p "+runfilename+" __BACKEND__:EMBRACE"
    backend_cmd.append(cmd)
    # === end setup pulsar xml files

    # === track source immediately
    src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,True,date=CmdTime)
    if src_rise==None: quit()
			
    obsfunc[beam].embraceNancay.date=CmdTime
    srcEph.compute(obsfunc[beam].embraceNancay)
    az=math.degrees(srcEph.az)
    alt=math.degrees(srcEph.alt)
    srcEph_ra=math.degrees(srcEph.a_ra)
    srcEph_dec=math.degrees(srcEph.a_dec)

    comment=srcEph.name+"  RA="+str(srcEph_ra)+" dec="+str(srcEph_dec)
    obsfunc[beam].log(comment)
    comment=srcEph.name+" tracking by J2000 at "+isodate(CmdTime)
    obsfunc[beam].log(comment)
    obsfunc[beam].addFITScomment(comment)

    # RF beam tracking
    RFBeam.SetCal('current')
    DigBeam.SetCal('current')
    RFBeam.AddSubscan(CmdTime,'J2000',srcEph_ra,srcEph_dec)

    # offsource if requested
    if obsfunc[beam].offsource==None: # by default use an offset 3 deg away in RA
        comment='off beam is 3 degrees away in RA'
        obsfunc[beam].log(comment)
        obsfunc[beam].addFITScomment(comment)
        offsrcEph_ra=srcEph_ra+3
        offsrcEph_dec=srcEph_dec	
    else:
        offsrcEph.compute(obsfunc[beam].embraceNancay)
        offsrcEph_ra=math.degrees(offsrcEph.a_ra)
        offsrcEph_dec=math.degrees(offsrcEph.a_dec)
	
    DigBeam.AddSubscan(CmdTime,'J2000',srcEph_ra,srcEph_dec,offsrcEph_ra,offsrcEph_dec)
    # ==== from now on we are tracking


    # ==== acquisition times: 10 minutes per half hour
    AcqStart=CmdTime
    AcqEnd=CmdTime+psr_acquisition_duration
    while AcqEnd < endobs:
        AcqStarts.append(AcqStart)
        AcqEnds.append(AcqEnd)
        obstag.append(srcEph.name)

        # === Define commands for pulsar observing on __BACKEND__
        # compose commands for __BACKEND__, send them after job submission
        # kill observations just before beginning
        cmd="ssh __BACKEND__ '/home/artemis/remote_cmd at "\
            +(AcqStart-dt.timedelta(minutes=2)).strftime('%H:%M %Y-%m-%d')\
            +" -f /home/artemis/EMBRACE/killobs_Beam"+beam+".sh'"
        backend_cmd.append(cmd)

        cmd="ssh __BACKEND__ '/home/artemis/remote_cmd at "\
            +AcqStart.strftime('%H:%M %Y-%m-%d')\
            +" -f /home/artemis/EMBRACE/"+runfilename+"'"
        backend_cmd.append(cmd)

        AcqStart+=dt.timedelta(minutes=30)
        AcqEnd=AcqStart+psr_acquisition_duration

    # === statistics acquisition
    if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
        print "\nObservation NOT submitted\n"
        quit()

# === end loop over RF Beams ==================================
		

# === finally, submit the observation for both beams ===========
if obsfunc[beam].submit() and srcEph.name[0:3]=='PSR':
	obsfunc[beam].submitAtBackend(backend_cmd)



