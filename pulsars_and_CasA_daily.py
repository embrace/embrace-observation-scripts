#! /usr/bin/env python
"""
$Id: pulsars_and_CasA_daily.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Fri 14 Mar 2014 17:44:42 CET
  originally from pulsars_daily.py, last mod: Wed Feb 19 18:25:14 CET 2014

monitoring of two pulsars each day for a week (or more), with a drift scan of CasA each day

$modified: Wed Mar 19 22:44:35 UTC 2014
  ndays=3

$modified: Sat 22 Mar 2014 19:26:35 CET
  fixed bug:  same run script was used for both pulsars
  ndays=4

$modified: Wed 26 Mar 2014 17:03:09 CET
  ndays=5

$modified: Fri 28 Mar 2014 16:06:38 CET
  ndays=4:  it looks like this is the maximum before the system barfs

$modified: Sun 30 Mar 2014 18:12:46 CEST
  digital calibration on CasA after satcal

$modified: Sat 05 Apr 2014 09:09:06 CEST
  CasA driftscan after digital cal on CasA is now the full duration (2 hrs) instead of 20 mins

$modified: Mon 07 Apr 2014 00:46:30 CEST
  CasA digital cal is not working.  going back to normal calibration while I try to work it out.

$modified: Tue 08 Apr 2014 17:03:31 CEST
  bug fix:  killobs replaced with beam specific killobs so as to avoid killing the other beam which may be running

$modified: Thu 10 Apr 2014 20:12:00 CEST
  removed comment to FITS file for location of source in az,el.  this doesn't make sense.  why did I put it there?

$modified: Wed 16 Apr 2014 13:00:25 CEST
  yoffset for cal drift scan is passed via obsfunc for each beam.

$modified: Thu 17 Apr 2014 19:51:08 CEST
  reintroduced digital calibration on CasA after having debugged the digcal() method in ObsFunctions

$modified: Mon 21 Apr 2014 00:08:53 CEST
  remove digital calibration on CasA... it's not working as we hoped.  
  is it the implementation?  or is it fundamental?

$modified: Tue 22 Apr 2014 22:52:30 CEST
  check if we should start observing immediately after calibration because we just missed transit
  ndays=3

$modified: Thu 05 Jun 2014 14:59:20 CEST
  ndays=4
  removed "nohorizon" for the summer.  The Sun is visible.

$modified: Mon 16 Jun 2014 22:01:58 CEST
  ndays=3: 4 days is too many, and we get the 0-byte problem

$modified: Mon 23 Jun 2014 16:48:27 CEST
  changing pulsars for observing: Crab instead of B1133
  Crab duration is only 30 mins instead of 2 hrs

$modified: Tue 24 Jun 2014 12:02:59 CEST
  Crab duration is only 24 mins

$modified: Thu 10 Jul 2014 14:27:06 CEST
  mkRunArtemis should have source name as input so it can choose the correct xml files

$modified: Fri 11 Jul 2014 18:06:51 CEST
  bug fix:  if obs ended with CasA then there was no submission to pulsar backend.
  always run submitAtBackend().  This script is always pulsar observing!

$modified: Thu 17 Jul 2014 11:47:38 CEST
  bug fix: all pulsar obs were using the same name left over from variable psr in the first loop

"""
ndays=3 # eventually this could be a command line option

helptxt='RF and digital calibration on two different calibrators (Sun, and GPS)\n'\
    +'followed by daily observations of B0329+54 and the Crab pulsar, and a drift scan of CasA,\n'\
    +'at two different frequencies.\nContinue for '+str(ndays)+' days.'

from copy import copy
pulsars=['B0329','PSR Crab']
sources=copy(pulsars)
sources.append('CasA')

import sys,os
sys.argv.insert(1,'--beams=both')
sys.argv.insert(1,'--duration=120')
sys.argv.insert(1,'--driftduration=120')
#sys.argv.insert(1,'--A:nohorizon')
sys.argv.insert(1,'--A:cal=Sun')
sys.argv.insert(1,'--A:RFcentre=970')
sys.argv.insert(1,'--A:RFcalfreq=970')
sys.argv.insert(1,'--B:cal=f2')

import datetime as dt
import ephem as eph
import math
from datefunctions import *

# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================

# === Observation title is applied for both beams =====================
ObsTitle='pulsar daily'
for beam in beams:
	obsfunc[beam].source='pulsars and CasA'
	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	comment="cal on "+obsfunc[beam].satname+", and tracking "+ObsTitle
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc[beam].initialize(ObsTitle):quit()
# =====================================================================

# generate the pointings for satellite calibration with driftscan
# calibrate at next transit for each calibrator
backend_cmd=[]
FirstAcquisition=None
didEmptySky={}
didEmptySky['A']=False
didEmptySky['B']=False
xmlfilename={}
runfilename={}
for beam in beams:

	# ======== get RFBeam and DigBeam objects ======================
	RFBeam=obsfunc[beam].getRFBeam()
	if RFBeam==None:quit()
	DigBeam=obsfunc[beam].makeDigitalBeam()
	if DigBeam==None: quit()
	# ==============================================================

	# reinitialize obstag, AcqStarts and AcqEnds for each beam
	obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
	obstag.append(obsfunc[beam].satname.replace(' ',''))
	AcqStarts=[]
	AcqEnds=[]

	satcalParms=obsfunc[beam].satcal(digcal=digcal,
					 driftscan=do_satdrift,
					 now=now)
	if satcalParms==None: quit()
	if not satcalParms['PointingStatus']:
		obsfunc[beam].log("Could not complete the satellite calibration sequence")
		quit()

	satcalStart=satcalParms['satcalStart']
	satcalEnd=satcalParms['EndTime']

	# calibration and drift data acquisition times
	if FirstAcquisition==None:FirstAcquisition=satcalStart-dt.timedelta(minutes=1)
	AcqStarts.append(satcalStart-dt.timedelta(minutes=1))
	AcqEnds.append(satcalEnd)


	CmdTime=satcalEnd
	endobs=satcalEnd

	"""
	# ==== now do a digital calibration on CasA
	obsfunc[beam].source='CasA'
	digparms=obsfunc[beam].digcal(CmdTime,elevation=60)
	if digparms==None:quit()

	# ==== and a driftscan
	CmdTime=digparms['endCal']
	#psr_duration=obsfunc[beam].duration
	#obsfunc[beam].duration=dt.timedelta(minutes=20)
	driftparms=obsfunc[beam].driftscan(CmdTime,now=True)
	if driftparms==None:quit()

	AcqStarts.append(digparms['StartTime'])
	AcqEnds.append(driftparms['enddrift'])
	obstag.append('CasA-digcal')
	CmdTime=driftparms['enddrift']
	endobs=driftparms['enddrift']

	# restore the desired duration for pulsar tracking
	#obsfunc[beam].duration=psr_duration
	"""

# ===== end phase calibration ==========================================

# ========= astronomical source pointing ================================
	# === setup pulsar xml files
	for psr in pulsars:
		obsfunc[beam].source=psr

		srcParms=obsfunc[beam].verifySource(obsfunc[beam].source,satcalEnd,obsfunc[beam].offsource)
		if srcParms==None: quit()
		srcEph=srcParms['srcEph']
		offsrcEph=srcParms['offEph']

		# === xml file for ARTEMIS !!!!!!!!!!!! need to change for different pulsars
		xmlfilename[srcEph.name]=obsfunc[beam].mkPulsarXML(srcEph.name,obsfunc[beam].duration-dt.timedelta(minutes=4))
		cmd="scp -p "+xmlfilename[srcEph.name]+" __BACKEND__:EMBRACE"
		backend_cmd.append(cmd)

		runfilename[srcEph.name]=obsfunc[beam].mkRunArtemis(xmlfilename[srcEph.name],srcEph.name)
		cmd="scp -p "+runfilename[srcEph.name]+" __BACKEND__:EMBRACE"
		backend_cmd.append(cmd)
	psr=None # make sure we get an error if this variable is used again
	# === end setup pulsar xml files

	# === setup observing for each day
	for day in range(ndays):
		obsfunc[beam].log(' <><><><><><> Day '+str(day)+' <><><><><><>')

		# === find the logical order of the sources
		transit_time=[]
		for src in sources:
			obsfunc[beam].log('DEBUG1: endobs='+isodate(endobs))
			srcParms=obsfunc[beam].verifySource(src,endobs,obsfunc[beam].offsource)
			obsfunc[beam].log('DEBUG2: endobs='+isodate(endobs))
			if srcParms==None: quit()
			srcEph=srcParms['srcEph']

			src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,True,date=endobs)
			obsfunc[beam].log('DEBUG3: endobs='+isodate(endobs))
			if src_rise==None: quit()

			transit_time.append(src_transit)

		# === order transit times chronologically
		sorted_index=sorted(range(len(transit_time)), key=lambda k: transit_time[k])
		comment='sources will be observed in order: '
		for src_index in sorted_index:comment+=sources[src_index]+', '
		obsfunc[beam].log(comment)
		comment=""
		for src_index in sorted_index:comment+=isodate(transit_time[src_index])+'\n'
		obsfunc[beam].log(comment)

		# === do the pulsars and CasA
		for src_index in sorted_index:

			obsfunc[beam].source=sources[src_index]
			srcParms=obsfunc[beam].verifySource(obsfunc[beam].source,endobs,obsfunc[beam].offsource)
			if srcParms==None: quit()
			srcEph=srcParms['srcEph']
			offsrcEph=srcParms['offEph']

			if srcEph.name[0:3]!='PSR':
				# === setup a drift scan on CasA
				driftparms=obsfunc[beam].driftscan(CmdTime,transit=True)
				if driftparms==None:quit()
				endobs=driftparms['enddrift']
				AcqStarts.append(driftparms['StartTime'])
				AcqEnds.append(driftparms['enddrift'])
				obstag.append(obsfunc[beam].source)
				CmdTime=driftparms['enddrift']
				# === end driftscan on CasA

			else: # it's a pulsar


				# track source around transit time
				src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,True,date=CmdTime)
				if src_rise==None: quit()
			
				# check if we should begin immediately because we just missed the transit
				obsfunc[beam].embraceNancay.date=CmdTime
				location=obsfunc[beam].currentLocation(srcEph)

				if (src_transit > (CmdTime + dt.timedelta(hours=12)))\
					    and  (float(srcEph.alt) > math.radians(60)):
					obsfunc[beam].log('starting observing immediately')
				else:
					CmdTime=src_transit - obsfunc[beam].duration/2


				# ensure that CmdTime is after the end of the previous observation
				if CmdTime<endobs:CmdTime=endobs

				# recalculate setting time to make sure it's after the start time
				src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,True,date=CmdTime)
				if src_rise==None: quit()

				obsfunc[beam].embraceNancay.date=CmdTime
				srcEph.compute(obsfunc[beam].embraceNancay)
				az=math.degrees(srcEph.az)
				alt=math.degrees(srcEph.alt)
				srcEph_ra=math.degrees(srcEph.a_ra)
				srcEph_dec=math.degrees(srcEph.a_dec)

				comment=srcEph.name+"  RA="+str(srcEph_ra)+" dec="+str(srcEph_dec)
				obsfunc[beam].log(comment)
				comment=srcEph.name+" tracking by J2000 at "+isodate(CmdTime)
				psrAcqStart=CmdTime+dt.timedelta(minutes=2)
				obsfunc[beam].log(comment)
				obsfunc[beam].addFITScomment(comment)

				# calculate transit for an RF off pointing, if there's time after the sat cal
				obsfunc[beam].embraceNancay.date=src_transit
				srcEph.compute(obsfunc[beam].embraceNancay)
				transit_az=math.degrees(srcEph.az)
				transit_alt=math.degrees(srcEph.alt)


				# RF beam tracking
				RFBeam.SetCal('current')
				DigBeam.SetCal('current')
				RFBeam.AddSubscan(CmdTime,'J2000',srcEph_ra,srcEph_dec)

				# offsource if requested
				if obsfunc[beam].offsource==None: # by default use an offset 3 deg away in RA
					comment='off beam is 3 degrees away in RA'
					obsfunc[beam].log(comment)
					obsfunc[beam].addFITScomment(comment)
					offsrcEph_ra=srcEph_ra+3
					offsrcEph_dec=srcEph_dec	
				else:
					offsrcEph.compute(obsfunc[beam].embraceNancay)
					offsrcEph_ra=math.degrees(offsrcEph.a_ra)
					offsrcEph_dec=math.degrees(offsrcEph.a_dec)
	
				DigBeam.AddSubscan(CmdTime,'J2000',srcEph_ra,srcEph_dec,offsrcEph_ra,offsrcEph_dec)
				# for the Crab, don't do a 2 hour integration because there's too much data with the fast sampling
				if srcEph.name.upper().find('CRAB')>=0:
					duration=dt.timedelta(minutes=24)
				else:
					duration=obsfunc[beam].duration
				endobs=CmdTime+duration
				psrAcqEnd=endobs-dt.timedelta(minutes=2)

				# acquisition times
				# if the main observation follows directly from the calibration
				# make a single acquisition phase for both
				if not didEmptySky[beam]:
					waitTime=CmdTime-satcalEnd
					waitTimeMins=tot_seconds(waitTime)/60.0
					obsfunc[beam].log("time between satellite end tracking and "+srcEph.name+" track start: "+str(waitTimeMins)+" minutes")
					if waitTime < dt.timedelta(minutes=30):
						AcqEnds[0]=endobs
						obstag[0]+="--"+srcEph.name
				
						# point immediately after calibration to the position where the source rises
						if waitTime > dt.timedelta(minutes=1):
							DigBeam.SetCal('current')
							RFBeam.AddSubscan(satcalEnd,'AZEL',az,alt)
							DigBeam.AddSubscan(satcalEnd,'AZEL',az,alt,az,alt)

					else: #we have time for a blank sky pointing before tracking starts
						AcqStarts.append(CmdTime)
						AcqEnds.append(endobs)
						obstag.append(srcEph.name)
				
						CmdTime=AcqEnds[0]
						AcqEnds[0]+=dt.timedelta(minutes=30)

						RFBeam.SetCal('current')
						DigBeam.SetCal('current')
						RFBeam.AddSubscan(CmdTime,'AZEL',transit_az,transit_alt)
						DigBeam.AddSubscan(CmdTime,'AZEL',transit_az,transit_alt,transit_az,transit_alt)
						comment='pointing at empty sky for 30 minutes starting at '+isodate(CmdTime)
						obsfunc[beam].log(comment)
						obsfunc[beam].addFITScomment(comment)
						didEmptySky[beam]=True
				else:
					AcqStarts.append(CmdTime)
					AcqEnds.append(endobs)
					obstag.append(srcEph.name)



				# === Define commands for pulsar observing on __BACKEND__
				# compose commands for __BACKEND__, send them after job submission
				# kill observations just before beginning
				cmd="ssh __BACKEND__ '/home/artemis/remote_cmd at "\
				    +(psrAcqStart-dt.timedelta(minutes=2)).strftime('%H:%M %Y-%m-%d')\
				    +" -f /home/artemis/EMBRACE/killobs_Beam"+beam+".sh'"
				backend_cmd.append(cmd)

				cmd="ssh __BACKEND__ '/home/artemis/remote_cmd at "\
				    +psrAcqStart.strftime('%H:%M %Y-%m-%d')\
				    +" -f /home/artemis/EMBRACE/"+runfilename[srcEph.name]+"'"
				backend_cmd.append(cmd)


			# === align the acquistion times of the two beams, if they are within a few minutes of one another
			delta=AcqStarts[0] - FirstAcquisition
			if delta>dt.timedelta(minutes=0) and delta<dt.timedelta(minutes=45): AcqStarts[0]=FirstAcquisition

			CmdTime=endobs

		# === end loop over sources

	# === end loop over days


	# === statistics acquisition
	if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
		print "\nObservation NOT submitted\n"
		quit()
# === end loop over RF Beams ==================================
		

# === finally, submit the observation for both beams ===========
if obsfunc[beam].submit():
	obsfunc[beam].submitAtBackend(backend_cmd)



