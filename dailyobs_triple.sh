#!/bin/bash
# $Id: dailyobs_triple.sh
# $auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
# $created: Fri 16 Oct 2015 14:19:07 CEST
# $license: GPLv3 or later, see https://www.gnu.org/licenses/gpl-3.0.txt
#
# setup the three daily observations

# undefine to run for real
# TESTMODE="--test" 

NDAYS=118
NDAYS=236
NDAYS=366

PERIOD=3

python /home/steve/scripts/git/obs/dailyobs.py $TESTMODE --ndays=$NDAYS --period=$PERIOD\
       --trigger=transit --duration=186 --action=./do_CygA.sh --source=CygA\
       --powerup

python /home/steve/scripts/git/obs/dailyobs.py $TESTMODE --ndays=$NDAYS --period=$PERIOD\
       --trigger=transit --duration=186 --action=./do_CasA.sh --source=CasA

python /home/steve/scripts/git/obs/dailyobs.py $TESTMODE --ndays=$NDAYS --period=$PERIOD\
       --trigger=transit --duration=120 --action=./do_B0329.sh --source=b0329\
       --powerdown

#dailyobs_triple.sh
