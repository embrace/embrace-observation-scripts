'''
$Id: horizon2horizon.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Thu 31 Jul 2014 10:05:48 CEST

track a source from horizon to horizon, and do it again with a second source
normally, CygA followed by a satellite tracking

$modified: Thu 31 Jul 2014 12:51:04 CEST
  changing stepsize to reduce AZEL tracking table

$modified: Tue 19 Aug 2014 16:10:45 CEST
  RF off every 30 minutes for 2 minutes

$modified: Wed 20 Aug 2014 16:48:56 CEST
  bug fix: tracking of satellite while visible

$modified: Wed 27 Aug 2014 15:12:40 CEST
  bug fix: RFpointing in between ON and OFF.  now determined in ObsFunctions
'''

helptxt='track a source from horizon to horizon, and do it again with a second source'
import sys,os
import datetime as dt
import ephem as eph
import math
from datefunctions import *

# some predefined options
sys.argv.insert(1,'--beams=both')
sys.argv.insert(1,'--driftduration=120')
sys.argv.insert(1,'--horizon=20')
sys.argv.insert(1,'--cal=f1')
sys.argv.insert(1,"--source=Cygnus A")
sys.argv.insert(1,'--duration=visible')
sys.argv.insert(1,'--stepsize=15')
sys.argv.insert(1,'--RFstepsize=30')
sys.argv.insert(1,'--calibration_stepsize=5')
source2='f2'

# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================

# === Observation title is applied for both beams =====================
# source name given on the command line, there is no default
ObsTitle=''
for beam in beams:
	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	comment="cal on "+obsfunc[beam].satname+", and tracking "+ObsTitle+' from horizon to horizon'
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc[beam].initialize(ObsTitle):quit()
# =====================================================================

# ===== phase calibration ==============================================
# generate the pointings for satellite calibration with driftscan
# default frequencies are given by obsfunc.FindSat() which was called in satorchi_OBSinit.py
backend_cmd=[]
FirstAcquisition=None
for beam in beams:

	# ======== get RFBeam and DigBeam objects ======================
	RFBeam=obsfunc[beam].makeRFBeam()
	if RFBeam==None:quit()
	DigBeam=obsfunc[beam].makeDigitalBeam()
	if DigBeam==None: quit()
	# ==============================================================

	# reinitialize obstag, AcqStarts and AcqEnds for each beam
	obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
	AcqStarts=[]
	AcqEnds=[]

	satcalParms=obsfunc[beam].satcal(digcal=digcal,
					 driftscan=do_satdrift,
					 now=now)
	if satcalParms==None: quit()
	if not satcalParms['PointingStatus']:
		obsfunc[beam].log("Could not complete the satellite calibration sequence")
		quit()

	obstag.append(satcalParms['satname'].replace(' ',''))
	satcalStart=satcalParms['satcalStart']
	satcalEnd=satcalParms['EndTime']

	# calibration and drift data acquisition times
	if FirstAcquisition==None:FirstAcquisition=satcalStart-dt.timedelta(minutes=1)
	AcqStarts.append(satcalStart-dt.timedelta(minutes=1))
	AcqEnds.append(satcalEnd)
# ===== end phase calibration ==========================================

# ========= astronomical source pointing ================================
	srcParms=obsfunc[beam].verifySource(obsfunc[beam].source,satcalEnd,obsfunc[beam].offsource)
	if srcParms==None: quit()
	srcEph=srcParms['srcEph']
	offsrcEph=srcParms['offEph']

	# for the source tracking
	CmdTime=satcalEnd
	if obsfunc[beam].obsdate!=None and obsfunc[beam].obsdate>satcalEnd:
		CmdTime=obsfunc[beam].obsdate

        obsfunc[beam].log('calculating source rise, transit, and set times')
	src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,True,date=CmdTime)
	if src_rise==None: quit()

	if obsfunc[beam].obsdate==None:CmdTime=src_rise

	# ensure that CmdTime is after the calibration end  time
	if CmdTime<satcalEnd:CmdTime=satcalEnd

	# recalculate setting time to make sure it's after the start time
        obsfunc[beam].log("recalculate setting time to make sure it's after the start time")
	src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,True,date=CmdTime)
	if src_rise==None: quit()

	obsfunc[beam].embraceNancay.date=CmdTime
	location=obsfunc[beam].currentLocation(srcEph)
	az=location['az']
	alt=location['alt']
	srcEph_ra=location['RA']
	srcEph_dec=location['dec']

	comment=srcEph.name+" tracking by J2000 at "+isodate(CmdTime)
	trackingStartTime=CmdTime
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)

	# calculate transit for an RF off pointing
	obsfunc[beam].embraceNancay.date=src_transit
	srcEph.compute(obsfunc[beam].embraceNancay)
	transit_az=math.degrees(srcEph.az)
	transit_alt=math.degrees(srcEph.alt)


	# RF beam tracking
	# Henrik's tracking: put the RFpointing in the middle and ON/OFF on either side
	RFpointing_ra=srcParms['RFpointing RA']
	RFpointing_dec=srcParms['RFpointing dec']

	RFBeam.AddSubscan(CmdTime,'J2000',RFpointing_ra,RFpointing_dec)
	obsfunc[beam].log('J2000 RF tracking: RA='+str('%.3f' % RFpointing_ra)+' dec='+str(RFpointing_dec))

	# offsource has been determined by verifySource() called above
        offsrcEph.compute(obsfunc[beam].embraceNancay)
        offsrcEph_ra=math.degrees(offsrcEph.a_ra)
        offsrcEph_dec=math.degrees(offsrcEph.a_dec)
	
        # digital beam tracking
        DigBeam.AddSubscan(CmdTime,'J2000',srcEph_ra,srcEph_dec,offsrcEph_ra,offsrcEph_dec)
	comment='J2000 Dig tracking: RA='+str('%.3f' % srcEph_ra)\
	    +' dec='+str('%.3f' % srcEph_dec)\
	    +' RA='+str('%.3f' % offsrcEph_ra)\
	    +' dec='+str('%.3f' % offsrcEph_dec)
	obsfunc[beam].log(comment)

	# duration of tracking.
	# if duration given on the command line, 
	# it is converted to a timedelta by the parser in satorchi_OBSinit.py
	if obsfunc[beam].duration==None:
		obsfunc[beam].log("setting end acquistion to source setting time: "+isodate(src_set))
		endobs=src_set
	else:
		endobs=CmdTime+obsfunc[beam].duration

	# we will do RF off every 30 mins.  
	# before transit, we point ahead in RA by 15 degrees
	RFoffPointing_dec=RFpointing_dec
	RFoffPointing_ra=RFpointing_ra-15
	# convert RA degrees to hours
	ra=RFoffPointing_ra*24/360.
	# create a source line for pyephem
	line='RFOFFpointing,f,'+str(ra)+','+str(RFoffPointing_dec)+',0,2000'
	RFoffPointingEph=eph.readdb(line)
	offTime=trackingStartTime+dt.timedelta(minutes=30)
	while offTime < src_transit:
		obsfunc[beam].embraceNancay.date=offTime
		offlocation=obsfunc[beam].currentLocation(RFoffPointingEph)
		comment='RF OFF position starting at '+isodate(offTime)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)
		RFBeam.AddSubscan(offTime,'AZEL',offlocation['az'],offlocation['alt'])
		DigBeam.AddSubscan(offTime,'AZEL',offlocation['az'],offlocation['alt'],offlocation['az'],offlocation['alt'])
		# go back to source after 2 minutes
		CmdTime=offTime+dt.timedelta(minutes=2)
		comment=srcEph.name+" tracking by J2000 at "+isodate(CmdTime)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)
		RFBeam.AddSubscan(CmdTime,'J2000',RFpointing_ra,RFpointing_dec)
		DigBeam.AddSubscan(CmdTime,'J2000',srcEph_ra,srcEph_dec,offsrcEph_ra,offsrcEph_dec)
		offTime+=dt.timedelta(minutes=30)


	# after transit,  we point behind in RA by 15 degrees
	RFoffPointing_ra=RFpointing_ra+15
	# convert RA degrees to hours
	ra=RFoffPointing_ra*24/360.
	# create a source line for pyephem
	line='RFOFFpointing,f,'+str(ra)+','+str(RFoffPointing_dec)+',0,2000'
	RFoffPointingEph=eph.readdb(line)
	while offTime < endobs:
		obsfunc[beam].embraceNancay.date=offTime
		offlocation=obsfunc[beam].currentLocation(RFoffPointingEph)
		comment='RF OFF position starting at '+isodate(offTime)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)
		RFBeam.AddSubscan(offTime,'AZEL',offlocation['az'],offlocation['alt'])
		DigBeam.AddSubscan(offTime,'AZEL',offlocation['az'],offlocation['alt'],offlocation['az'],offlocation['alt'])
		# go back to source after 2 minutes
		CmdTime=offTime+dt.timedelta(minutes=2)
		comment=srcEph.name+" tracking by J2000 at "+isodate(CmdTime)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)
		RFBeam.AddSubscan(CmdTime,'J2000',RFpointing_ra,RFpointing_dec)
		DigBeam.AddSubscan(CmdTime,'J2000',srcEph_ra,srcEph_dec,offsrcEph_ra,offsrcEph_dec)
		offTime+=dt.timedelta(minutes=30)
		


	# acquisition times
	# if the main observation follows directly from the calibration
	# make a single acquisition phase for both
	waitTime=trackingStartTime-satcalEnd
	waitTimeMins=tot_seconds(waitTime)/60.0
	obsfunc[beam].log("time between satellite end tracking and "+srcEph.name+" track start: "+str(waitTimeMins)+" minutes")

	if waitTime < dt.timedelta(minutes=30):
		AcqEnds[0]=endobs
		obstag[0]+="--"+srcEph.name

		# point immediately after calibration to the position where the source rises
		if waitTime > dt.timedelta(minutes=1):
			RFBeam.AddSubscan(satcalEnd,'AZEL',az,alt)
			DigBeam.AddSubscan(satcalEnd,'AZEL',az,alt,az,alt)

	else: #we have time for a blank sky pointing before tracking starts
		AcqStarts.append(trackingStartTime)
		AcqEnds.append(endobs)
		obstag.append(srcEph.name)

		CmdTime=AcqEnds[0]
		AcqEnds[0]+=dt.timedelta(minutes=30)

		RFBeam.AddSubscan(CmdTime,'AZEL',transit_az,transit_alt)
		DigBeam.AddSubscan(CmdTime,'AZEL',transit_az,transit_alt,transit_az,transit_alt)
		comment='pointing at empty sky for 30 minutes starting at '+isodate(CmdTime)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)
# === end astronomical pointing configuration ==================

# === Define commands for pulsar observing on Andante or Borsen
	if srcEph.name[0:3]=='PSR':

		psrAcqStart=trackingStartTime+dt.timedelta(minutes=2)
		psrAcqEnd=endobs-dt.timedelta(minutes=2)

		# compose commands for Andante, send them after job submission
		# kill observations just before beginning
		cmd="ssh __BACKEND__ '/home/artemis/remote_cmd at "\
		    +(psrAcqStart-dt.timedelta(minutes=2)).strftime('%H:%M %Y-%m-%d')\
		    +" -f /home/artemis/EMBRACE/killobs_Beam"+beam+".sh'"
		backend_cmd.append(cmd)

		xmlfilename=obsfunc[beam].mkPulsarXML(srcEph.name,psrAcqEnd-psrAcqStart)
		cmd="scp -p "+xmlfilename+" __BACKEND__:EMBRACE"
		backend_cmd.append(cmd)


		runfilename=obsfunc[beam].mkRunArtemis(xmlfilename,srcEph.name)
		cmd="scp -p "+runfilename+" __BACKEND__:EMBRACE"
		backend_cmd.append(cmd)

		cmd="ssh __BACKEND__ '/home/artemis/remote_cmd at "\
		    +psrAcqStart.strftime('%H:%M %Y-%m-%d')\
		    +" -f /home/artemis/EMBRACE/"+runfilename+"'"
		backend_cmd.append(cmd)



# === satellite tracking from horizon to horizon
        CmdTime=endobs
	obsfunc[beam].duration=None
	srcParms=obsfunc[beam].verifySource(source2,CmdTime,obsfunc[beam].offsource)
	if srcParms==None: quit()
	srcEph=srcParms['srcEph']
	offsrcEph=srcParms['offEph']
        obstag.append(srcEph.name)
        obsfunc[beam].source=srcEph.name
        obsfunc[beam].log('calculating source rise, transit, and set times')
	src_rise,src_transit,src_set=obsfunc[beam].next_transit(srcEph,False,date=CmdTime)
	if src_rise==None: quit()

	CmdTime=src_rise

	# ensure that CmdTime is after the calibration end time
	if CmdTime<endobs:CmdTime=endobs

	obsfunc[beam].embraceNancay.date=CmdTime
	location=obsfunc[beam].currentLocation(srcEph)
	az=location['az']
	alt=location['alt']

	comment=srcEph.name+" tracking by AZEL at "+isodate(CmdTime)
	trackingStartTime=CmdTime
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)

	# AZEL tracking
	StartTime,endTracking=obsfunc[beam].SourceTrack(CmdTime)
        AcqStarts.append(StartTime)
        AcqEnds.append(endTracking)
		
# === Define  STATISTICS ACQUISITION  ======
	# align the acquistion times of the two beams, if they are within a few minutes of one another
	delta=AcqStarts[0] - FirstAcquisition
	if delta>dt.timedelta(minutes=0) and delta<dt.timedelta(minutes=45): AcqStarts[0]=FirstAcquisition
	if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
		print "\nObservation NOT submitted\n"
		quit()

# === finally, submit the observation for both beams ===========
if obsfunc[beam].submit() and srcEph.name[0:3]=='PSR':
	obsfunc[beam].submitAtBackend(backend_cmd)



