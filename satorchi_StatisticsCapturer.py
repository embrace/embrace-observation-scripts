#! /usr/bin/env python
"""\   
DESCRIPTION:

$Id: satorchi_StatisticsCapturer.py
$auth: originally by Patrice Renaud
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>

$modified: Thu Mar 29 16:45:53 CEST 2012
  copied and modified to work in my home directory

Data Capturer.
Read and write statistics (ssx, bsx, csx) from backend A and/or B

Design:
- creates as many LCU clients as statistics type in the observation 
(max:6 -> ssxA, ssxb, bsxa, bsxb, csxa, csxb)
- creates an acquisition thread by statistics type (max: 6)

NB1: A ACQ function must be finished before a ACQ function of the same type begins.
(a control is done (to check) during obs submit)
It works,if ssx acq phases is finished (unsubscribe)
before a new ssx acq phase begins (subscribe)

TESTS CASE:
6/10/2011: Acq 1 phase SSX-A OK, Acq 2 phases SSX-A OK, Acq 1 phase SSX-B OK
7/10/01: BSX A et BSX B OK 
SSX-A et B, BSX A et B, lances au meme T0 : OK

$modified: Thu Mar 29 16:45:53 CEST 2012
  slight changes to make it work in my directory

$modified: Mon Jun 25 16:38:18 CEST 2012
  added checking before pickle.load() to avoid error on reading an empty file

  finally changed interrupt key to ctl-alt-space

$modified: Fri 15 Feb 2013 11:58:05 CET
  removed hardwired path for init scripts
  search python path instead, using FullPathname()

$modified: Thu 21 Feb 2013 11:59:28 CET
  check for SSXlist not none (see mail from PR this day)

$modified: Tue 26 Feb 2013 9:54:00 CET  (by PR)
  check for BSXlist and CSX not none (see mail from PR this day)
  no more copy of pickle file before loading pickle file

$modified: Mon 06 Jan 2014 15:28:44 CET
  add logging of times for acquisition subscription
"""
##### variables and parameters used in the script
interruptText="\nHit 'ctl-alt-space' key to stop reading statistics"
interruptKey='\x80'
timeFmt="%Y-%m-%d %H:%M:%S"
from datefunctions import *

from ObsFunctions import FullPathname
initfile=FullPathname('SCU/Tests/SCUinit-DC.py')
if initfile==None:quit()
execfile(initfile)
initfile=FullPathname('SCU/Tests/Observation/DataCapturerInit.py')
if initfile==None:quit()
execfile(initfile)

import time,datetime
from SCU.PackageBackend.ModuleSubbandStatistics import TSubbandStatisticsList

from datetime import timedelta
DATA_DIR="/data"
DATA_OBS="/data/obs"
NEWOBS_LASTHDU_SSX_FILENAME=DATA_OBS+"/NewObs_lastHDU_ssx.fits"  
NEWOBS_LASTHDU_BSX_FILENAME=DATA_OBS+"/NewObs_lastHDU_bsx.fits"  
NEWOBS_LASTHDU_CSX_FILENAME=DATA_OBS+"/NewObs_lastHDU_csx.fits"  
NEWOBS_FILENAME=DATA_OBS+"/NewObs.pickle"  
NEWOBS_RF_FILENAME=DATA_OBS+"/NewObs_RF.txt"  


def DC_StartAcq(StartDate,AcqFn,AcqFnArgList):  #StartTimer is defined in ModuleTools
	TimerId=StartTimer(StartDate,AcqFn,AcqFnArgList)
	return TimerId

def DC_AcqSSX(acqphase):
	#this function must be used in acq phases description of obs script
	#probably must init ssxlist if several call acqSSX in the same obs
	global KeyHasBeenPressed
	
	AcqStopTime=acqphase.GetStopDate()
	filename=acqphase.GetFilename()
	ninte=acqphase.GetNinte()
	if(acqphase.IsBeamA()):
		SSXclient=SSXclient_A
	else:
		SSXclient=SSXclient_B

	SSXlist=SSXclient.backend.ssxlist
	SSXlist.ZeroData()
	SSXlist.SetObservation(Obs) #associates ssxlist with the observation			
	SSXlist.InitFile(filename)
	SSXlist.average.init(ninte)
	if(SSXlist.average.IsEnabled()):
		AverageEnabled=True 
	else: 
		AverageEnabled=False


	irec=0
	print "\nSSXclient Backend Number: \n"+str(SSXclient.GetBackendNumber())	
	#subscribe to LCU in order to receive subbands synchronously
	print '\n'+isodate(dt.datetime.utcnow())+' Subscribe to subband statistics'
	kodret=SSXclient.Subscribe_SubbandsStatistics()
	KeyHasBeenPressed=False
	print interruptText
	#asynchronous stop (key pressed) must be managed inside acq routine in order routine properly stops
	while(not(KeyHasBeenPressed) and not(IsTimeArrived(AcqStopTime))):
		current_time=time.strftime(timeFmt,time.localtime())
		PrintOver('Record # '+str(irec+1)+" Current Time: "+current_time)
		#print 'Record # '+str(irec+1)+" Current Time: "+current_time

		if(AverageEnabled):
			if(SSXlist.average.IsFinished()):
				SSXlist.average.GetResult()
				SSXlist.ToFile()#converts to FITS HDU and append to FITS file	
			else:
				#Read Subband Statistics from LCU computer as Subband Statistics (SSX) OBJECT		
				SSXlist=SSXclient.Update_SubbandsStatistics()
				if(SSXlist <> None):
					SSXlist.SetObservation(Obs) #associates ssxlist with the observation			
					SSXlist.average.add()
		else:
			SSXlist=SSXclient.Update_SubbandsStatistics()
			if(SSXlist <> None):
				SSXlist.SetObservation(Obs) #associates ssxlist with the observation			
				SSXlist.ToFile()#converts to FITS HDU and append to FITS file	
				
		
		irec=irec+1
		
	##don't forget to unsubscribe  !
	print '\n'+isodate(dt.datetime.utcnow())+' UNSubscribe from subband statistics'
	SSX=SSXclient.Update_SubbandsStatistics() #to avoid unsubscribe and update as answer
	kodret=SSXclient.UnSubscribe_SubbandsStatistics()
	print '\n'+isodate(dt.datetime.utcnow())+' OK UNSubscribe from subband statistics'



def DC_AcqBSX(acqphase):
	
	global KeyHasBeenPressed
	AcqStopTime=acqphase.GetStopDate()
	acqfile=acqphase.GetFilename()
	ninte=acqphase.GetNinte()
	if(acqphase.IsBeamA()):
		BSXclient=BSXclient_A
	else:
		BSXclient=BSXclient_B


	#this function must be used in acq phases description of obs script
	#keyInit()  #to be able to be interrupted by key hit
	BSXlist=BSXclient.backend.bsxlist
	BSXlist.SetObservation(Obs) #associates ssxlist with the observation			
	BSXlist.InitFile(acqfile)

	irec=0
	#subscribe to LCU in order to receive subbands synchronously
	print '\n'+isodate(dt.datetime.utcnow())+' Subscribe to beamlet statistics'
	BSXclient.Subscribe_BeamletsStatistics()
	KeyHasBeenPressed=False
	print "\nBSXclient Backend Number: \n"+str(BSXclient.GetBackendNumber())	
	print interruptText
	#asynchronous stop (key pressed) must be managed inside acq routine in order routine properly stops
	while(not(KeyHasBeenPressed) and not(IsTimeArrived(AcqStopTime))):
		current_time=time.strftime(timeFmt,time.localtime())
		#current_time=str(datetime.datetime.today())
		PrintOver('Record # '+str(irec+1)+" Current Time: "+current_time)
		#Read Subband Statistics from LCU computer as Subband Statistics (SSX) OBJECT		
		BSXlist=BSXclient.Update_BeamletsStatistics()
		if(BSXlist <> None):
			BSXlist.SetObservation(Obs) #associates ssxlist with the observation				
			#converts to FITS HDU and append to FITS file
			BSXlist.ToFile()
					
		irec=irec+1
		
	##don't forget to unsubscribe  !
	print '\n'+isodate(dt.datetime.utcnow())+' UNSubscribe from beamlet statistics'
	BSX=BSXclient.Update_BeamletsStatistics() #to avoid unsubscribe and update as answer
	kodret=BSXclient.UnSubscribe_BeamletsStatistics()
	print '\n'+isodate(dt.datetime.utcnow())+'OK UNSubscribe from beamlet statistics'



def DC_AcqCSX(acqphase):
	
	global KeyHasBeenPressed
	AcqStopTime=acqphase.GetStopDate()
	filename=acqphase.GetFilename()
	ninte=acqphase.GetNinte()
	if(acqphase.IsBeamA()):
		CSXclient=CSXclient_A
	else:
		CSXclient=CSXclient_B

	CSX=CSXclient.backend.csx
	CSX.SetObservation(Obs) #associates csx with the observation
	nrcu=24 #csx => data square 24*24
	CSX.InitFile(filename,nrcu)
#	try:
		#read, display and append  statistics data records coming from LCU
		#until key "space" pressed
	irec=0
	#subscribe to LCU in order to receive subbands synchronously
	print '\n'+isodate(dt.datetime.utcnow())+' Subscribe to crosslet statistics'
	kodret=CSXclient.Subscribe_CrossletsStatistics()
	KeyHasBeenPressed=False
	print interruptText
	print "\nCSXclient Backend Number: \n"+str(CSXclient.GetBackendNumber())	
	#asynchronous stop (key pressed) must be managed inside acq routine in order routine properly stops
	while(not(KeyHasBeenPressed) and not(IsTimeArrived(AcqStopTime))):
		current_time=time.strftime(timeFmt,time.localtime())
		#current_time=str(datetime.datetime.today())
		PrintOver('Record # '+str(irec+1)+" Current Time: "+current_time) 

		#Read Subband Statistics from LCU computer as Subband Statistics (SSX) OBJECT		
		CSX=CSXclient.Update_CrossletsStatistics()
		if(CSX <> None):
			CSX.SetObservation(Obs)										
			#converts to FITS HDU and append to FITS file
			CSX.ToFile()
		
		irec=irec+1
		
	##don't forget to unsubscribe  !
	print '\n'+isodate(dt.datetime.utcnow())+' UNSubscribe from crosslets statistic'
	CSX=CSXclient.Update_CrossletsStatistics() #to avoid unsubscribe and update as answer
	kodret=CSXclient.UnSubscribe_CrossletsStatistics()
	print '\n'+isodate(dt.datetime.utcnow())+'OK UNSubscribe from crosslet statistics'

def AbortAcq(timers):
	for i in range(len(timers)):
		try:
			timers[i].cancel()
			print "timer cancel"
			time.sleep(0.5)
		except NameError:
			pass

def WaitNewObs():
	#a new obs has been submitted if "NewObs.pickle" exists
	newobs=False
	while(not(newobs)):
		time.sleep(1)
		if(os.path.exists(NEWOBS_FILENAME)):
			time.sleep(5)  #to be sure obs has finished writing pickle file
			filesize=os.path.getsize(NEWOBS_FILENAME)
			while(not(filesize>0)): #wait pickle file closed
				time.sleep(0.5)
				filesize=os.path.getsize(NEWOBS_FILENAME)
			newobs=True

#############################################################################
#MAIN SCRIPT
#############################################################################

global KeyHasBeenPressed
#to include later in a SCUinit.conf file
TotalRSPboards=2
tilesets='0:15'


while(1):
	#if previous obs bad terminated
	print "\nIMPORTANT: CHECK NO OTHER DATA CAPTURER IS RUNNING\n"
	if(os.path.exists(NEWOBS_FILENAME)):
		os.remove(NEWOBS_FILENAME) 
	#New obs submitted ?
	print "\nWaiting new observation"
	WaitNewObs()
	t=time.strftime('%d%m%y-%H%M%S',time.localtime())
	print "OK: new observation submitted at ",time.strftime(timeFmt,time.localtime())

	#OK, new obs submitted
	print "Analyse Observation"
	#an observation object must exist before pickle loads a new observation
	Obs=TObservation()
	#rebuild obs from obs pickle file
	# first check that the pickle file is not empty
	if os.path.exists(NEWOBS_FILENAME):
		fstatus=os.stat(NEWOBS_FILENAME)
		if fstatus.st_blocks>0:		
			MyFile=open(NEWOBS_FILENAME,"rb")
			Obs=pickle.load(MyFile)
			MyFile.close()
		else:
			print "\nNot reading NewObs pickle file.  It's empty!"
	else:
		print "\nNewObs pickle file does not exist: ",NEWOBS_FILENAME


	#Creates only clients needed to read statistics	
	#it works much better if you create Statistics Clients connection 
	#each time an observation is started (no more "socket disconnected..."
	if(Obs.AcqPhases.ContainsSSX_A()):
		print "contains SSX A"
		SSXclient_A=TBACKENDClient(TotalRSPboards,backend_number=0,lcuconnect=LCUconnect__var,lcuip=LCUip__var) #Virtual access to LCU
		if(not(LCUconnect__var)):
			print 'Test: No connection to LCU'
		if(SSXclient_A.connected == False):   
			print '- Unable to Connect to LCU'
			sys.exit()
		#if you want to select only some RCUs
		SSXclient_A.backend.rcu.SelectNone()
		SSXclient_A.backend.rcu.select(tilesets)

	if(Obs.AcqPhases.ContainsSSX_B()):
		print "contains SSX B"
		SSXclient_B=TBACKENDClient(TotalRSPboards,backend_number=1,lcuconnect=LCUconnect__var,lcuip=LCUip__var) #Virtual access to LCU
		if(not(LCUconnect__var)):
			print 'Test: No connection to LCU'
		if(SSXclient_B.connected == False):   
			print '- Unable to Connect to LCU'
			sys.exit()
		#if you want to select only some RCUs
		SSXclient_B.backend.rcu.SelectNone()
		SSXclient_B.backend.rcu.select(tilesets)
		
	if(Obs.AcqPhases.ContainsBSX_A()):
		print "\nCreating a backend client for Beamlet Statistics Beam A"
		BSXclient_A=TBACKENDClient(TotalRSPboards,backend_number=0,lcuconnect=LCUconnect__var,lcuip=LCUip__var) #Virtual access to LCU
		if(not(LCUconnect__var)):
			print 'Test: No connection to LCU'
		if(BSXclient_A.connected == False):   
			print '- Unable to Connect to LCU'
			sys.exit()
		#beamlet statistics use beamlet mask defined here:
		MyBSXlist_A=BSXclient_A.backend.bsxlist   #it's the backend client, which must have the bsx mask
		#select beamlets for 3 RSP boards,  dir X/Y
		MyBSXlist_A.mask.UnSelectAll()
		for RSPnumber in range(3):
			direction='X'
			MyBSXlist_A.mask.Select(RSPnumber,direction)
			direction='Y'
			MyBSXlist_A.mask.Select(RSPnumber,direction)
		
	if(Obs.AcqPhases.ContainsBSX_B()):	
		print "\nCreating a backend client for Beamlet Statistics Beam B"
		BSXclient_B=TBACKENDClient(TotalRSPboards,backend_number=1,lcuconnect=LCUconnect__var,lcuip=LCUip__var) #Virtual access to LCU
		if(not(LCUconnect__var)):
			print 'Test: No connection to LCU'
		if(BSXclient_B.connected == False):   
			print '- Unable to Connect to LCU'
			sys.exit()
		#beamlet statistics use beamlet mask defined here:
		MyBSXlist_B=BSXclient_B.backend.bsxlist   #it's the backend client, which must have the bsx mask
		#select beamlets for 3 RSP boards,  dir X/Y
		MyBSXlist_B.mask.UnSelectAll()
		for RSPnumber in range(3):
			direction='X'
			MyBSXlist_B.mask.Select(RSPnumber,direction)
			direction='Y'
			MyBSXlist_B.mask.Select(RSPnumber,direction)
		
	if(Obs.AcqPhases.ContainsCSX_A()):	
		CSXclient_A=TBACKENDClient(TotalRSPboards,backend_number=0,lcuconnect=LCUconnect__var,lcuip=LCUip__var) #Virtual access to LCU
		if(not(LCUconnect__var)):
			print 'Test: No connection to LCU'
		if(CSXclient_A.connected == False):   
			print '- Unable to Connect to LCU'
			sys.exit()
		rculist=range(24)
		CSXclient_A.backend.csx.SetRcuList(rculist)
	
	if(Obs.AcqPhases.ContainsCSX_B()):	
		CSXclient_B=TBACKENDClient(TotalRSPboards,backend_number=1,lcuconnect=LCUconnect__var,lcuip=LCUip__var) #Virtual access to LCU
		if(not(LCUconnect__var)):
			print 'Test: No connection to LCU'
		if(CSXclient_B.connected == False):   
			print '- Unable to Connect to LCU'
			sys.exit()
		rculist=range(24)
		CSXclient_B.backend.csx.SetRcuList(rculist)
		

		
	#Launch ACQUISITION THREADS (timers)
	print "start acquisition phases"
	nphases=Obs.AcqPhases.GetSize()
	TimersList=[]  #to be able to cancel timers if obs stopped before end
	for i in range(nphases):
		acqphase=Obs.AcqPhases.GetPhase(i)
		AcqStopDate=acqphase.GetStopDate()
		AcqStartDate=acqphase.GetStartDate()
		now=datetime.datetime.now()
		if(AcqStartDate < now):
			AcqStartDate=now+timedelta(seconds=10)
			print "AcqStartDate for this phase already terminated. Replace with 'now+ 10 sec' StartDate: ",AcqStartDate
		filename=acqphase.GetFilename()
		print "Start Acquisition at:",AcqStartDate,
		print "Stop Acquisition at: ",AcqStopDate
		print "filename: ",filename

		arglist=[acqphase]
		if(acqphase.IsSSX()):
			timer=DC_StartAcq(AcqStartDate, DC_AcqSSX,arglist)
		elif (acqphase.IsBSX()):
			timer=DC_StartAcq(AcqStartDate, DC_AcqBSX,arglist)
		elif (acqphase.IsCSX()):
			timer=DC_StartAcq(AcqStartDate, DC_AcqCSX,arglist)
		TimersList.append(timer)
		time.sleep(1) #necessary to give time to start acq threads 
							
	AcqStopDate=Obs.AcqPhases.GetStopDate()
	#AcqStopDate=datetime.datetime.today()+timedelta(seconds=5)
	KeyInit()  #to be able to be interrupted by key hit
	print "Waiting end of last acquisition phases ",AcqStopDate
	print interruptText

	KeyHasBeenPressed=False  #to be able to stop properly acq INSIDE thread (global variable) and because keypressed is not usable 2 times consecutively 
	while(not(IsTimeArrived(AcqStopDate))):
		time.sleep(1)
		if(KeyPressed(interruptKey)): #if True, not usable again to test keyPressed
			KeyHasBeenPressed=True
			break
		
	#Observation end (normal or key pressed)
	KeyEnd()  #restore keys characteristics
	if(KeyHasBeenPressed):
		time.sleep(2)  #to let time to acq threads to detect key pressed and stop properly
		AbortAcq(TimersList)  #for acq threads that have not yet started

	time.sleep(2) #to let time to threads to stop properly
	print "Observation acquisition phases finished"
	
	#disconnect clients at the end of the observation
	print "Disconnect clients"
	if(Obs.AcqPhases.ContainsSSX_A()):
		SSXclient_A.Disconnect()
	if(Obs.AcqPhases.ContainsSSX_B()):
		SSXclient_B.Disconnect()
	if(Obs.AcqPhases.ContainsBSX_A()):
		BSXclient_A.Disconnect()
	if(Obs.AcqPhases.ContainsBSX_B()):
		BSXclient_B.Disconnect()
	if(Obs.AcqPhases.ContainsCSX_A()):
		CSXclient_A.Disconnect()
	if(Obs.AcqPhases.ContainsCSX_B()):
		CSXclient_B.Disconnect()
	

    
    
