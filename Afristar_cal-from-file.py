#! /usr/bin/env python
"""
$Id: Afristar_cal-from-file.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Tue 08 Jul 2014 13:10:26 CEST
  originally from Afristar.py, last mod: Mon Nov 18 15:53:48 CET 2013

"""

import sys,os
import datetime as dt
import ephem as eph

calTable={}

# measured for Afristar: Tue Jul  8 14:06:50 CEST 2014
calTable['A']=[0, 12, 0, 6, 0, 22, 13, 13, 0, 8, 11, 10, 0, 7, 11, 7, 0, 17, 5, 22, 0, 11, 11, 19, 0, 19, 13, 11, 0, 20, 20, 22, 0, 20, 17, 13, 0, 2, 10, 2, 0, 15, 13, 3, 0, 7, 11, 12, 0, 5, 23, 16, 0, 10, 20, 14, 0, 23, 21, 4, 0, 2, 12, 11]

# measured for Afristar: Tue 08 Jul 2014 14:14:27 CEST
calTable['B']=[0, 2, 19, 21, 0, 17, 13, 4, 0, 19, 11, 14, 0, 21, 9, 20, 0, 19, 15, 22, 0, 5, 19, 10, 0, 23, 16, 15, 0, 1, 17, 4, 0, 6, 4, 5, 0, 12, 21, 2, 0, 6, 4, 7, 0, 20, 21, 20, 0, 21, 12, 8, 0, 0, 2, 6, 0, 5, 7, 14, 0, 9, 17, 11]


#TotalRSPboards=3
#sys.argv.append('--source=Afristar')
# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================

# === Observation title is applied for both beams =====================
# source name given on the command line, there is no default
dtnow=dt.datetime.utcnow()
ObsTitle=''
comment="saved calibration parameters and pointing on Afristar"
az=155.10
alt=32.55
for beam in beams:
	obsfunc[beam].source='Afristar'
	obsfunc[beam].RFcalfreq=1489.0
	if not obsfunc[beam].assignRFcentre(1474.5): quit()

	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc[beam].initialize(ObsTitle):quit()
# =====================================================================

for beam in beams:
# ======== Start digital beam config ===================================
	DigBeam=obsfunc[beam].makeDigitalBeam()
	if DigBeam==None: quit()
# ========= End digital beam config ====================================

# ======== get RF Beam object ==========================================
	RFBeam=obsfunc[beam].getRFBeam()
	if RFBeam==None: quit()
# ======================================================================

# === statistics acquisition ==========================================
# give acquisition start and end times
# default: start in 40 seconds from now, end 20 mins later
	obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
	obstag.append('Afristar')
	AcqStarts=[]
	AcqEnds=[]
	if obsfunc[beam].obsdate==None:
		AcqStarts.append(dtnow+dt.timedelta(seconds=40))
	else:
		AcqStarts.append(obsfunc[beam].obsdate)

	if obsfunc[beam].duration==None:
		AcqEnds.append(AcqStarts[0]+dt.timedelta(minutes=20))
	else:
		AcqEnds.append(AcqStarts[0]+obsfunc[beam].duration)
# =====================================================================

# ========= point to Afristar =========================================
	CmdTime=AcqStarts[0]+dt.timedelta(minutes=1)
	obsfunc[beam].embraceNancay.date=CmdTime

	comment=obsfunc[beam].source+" is at  az="+str(az)+" alt="+str(alt)
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)

	# time to allow for the RF calibration in seconds
	# note that RF cal cannot be done simultaneously on A and B.
	rfcaldelta=dt.timedelta(seconds=obsfunc[beam].rfcaltime)

	comment='RF calibration beginning at '+isodate(CmdTime)
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
	RFBeam.SetCal('scu',fcal=obsfunc[beam].RFcalfreq) 
        RFBeam.BeamCal.CalTable.SetValues(calTable[beam])
	RFBeam.AddSubscan(CmdTime,'AZEL',az,alt)
	CmdTime+=rfcaldelta

	#RFBeam.SetCal('current',fcal=obsfunc[beam].RFcalfreq) 
	RFBeam.AddSubscan(CmdTime,'AZEL',az,alt)

	if digcal:
		comment='digital calibration beginning at '+isodate(CmdTime)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)
		DigBeam.SetCal('none')
		DigBeam.AddSubscan(CmdTime,'AZEL',az,alt)

		# time to allow for the digital calibration in seconds
		digcaltime=10.0+1.1*obsfunc[beam].nBeamlets()
		CmdTime+=dt.timedelta(seconds=digcaltime)

	comment='tracking by AZEL beginning at '+isodate(CmdTime)
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
	DigBeam.SetCal('current')
	DigBeam.AddSubscan(CmdTime,'AZEL',az,alt)

# === end pointing configuration =======================================


# === Define  STATISTICS ACQUISITION phases ============================
	if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
		print "\nObservation NOT submitted\n"
		quit()
# ======================================================================

# === finally, submit the observation ==================================
obsfunc[beam].submit()

