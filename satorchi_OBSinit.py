#! /usr/bin/env python
"""\
DESCRIPTION:
Initial Observation configuration
Creates a Scan client to manage observation with LCU
Creates a Backend Client to read statistics from LCU

$Id: OBSinit.py and SCUinit.py: originally by Patrice Renaud

$modified: Sun Jul 31 13:38:20 CEST 2011
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>

$modified: Wed Aug  3 16:02:43 CEST 2011
 The original SCUinit and OBSinit are here combined

$modified: Thu Sep  8 10:25:39 CEST 2011
 created SCUconf.py with shared stuff between DataCapurer and OBSinit

$modified: Fri Sep 23 16:54:05 CEST 2011
 parse input arguments to the calling observation script

$modified: Thu Nov 17 11:16:31 CET 2011
 set the MyObservation object into the obsfunc class

$modified: Wed Dec  7 16:28:12 CET 2011
 adding arguments to the command line argument parser
 source,offsource
 eventually, this should go in ObsFunctions.parseargs() (so far only partially implemented)

$modified: Wed Dec  7 22:28:54 CET 2011
 adding arguments to command line parser
 caldate : time to start calibration
 obsdate : (already exists) now refers to time to start astro observation (eg. SourceTrack)

$modified: Fri Dec  9 15:40:54 CET 2011
 adding argument to command line parser
 duration : length of time for source tracking

$modified: Mon Mar 26 11:52:46 CEST 2012
 doubling up to define for Beam-A and Beam-B (loop through ['A','B'])

$modified: Thu Mar 29 14:08:30 CEST 2012
 fixed bugs in A/B looping
 argument parsing moved to Obsfunctions and called in SCUconf.py when the obsfunc object is created

$modified: Tue Apr 10 12:01:30 CEST 2012
  duration defaults to 10 minutes rather than None.

$modified: Fri Apr 13 13:47:08 CEST 2012
  argument parsing: RFcentre for use in SunTrack.py

$modified: Fri Apr 13 15:45:01 CEST 2012
  duration defaults to None (back to the way it was before)
  duration converted to a timedelta here (this may break other scripts!)
  new argument keyword:  nohorizon.  ignore EMBRACE horizon (default False)

$modified: Mon May  7 12:09:08 CEST 2012
  argument parsing is moved to top of script
  this is so we can put active tilesets as an argument

$modified: Mon May 14 15:56:36 CEST 2012
  option for digital beam tracking in the arguments parser:  digtrack=True by default
  can specifiy --nodigtrack to make digtrack=False

$modified: Mon May 21 10:29:34 CEST 2012
  default for nbeamlets is now None here
  in ObsFunctions, nbeamlets will default to 62
  this allows me to have a different default in Afristar.py, or whatever comes later

$modified: Wed May 23 12:22:35 CEST 2012
  digcal added to argument list:  default True

$modified: Mon Jun 25 12:01:41 CEST 2012
  allowing argument "--cal=" as a substitue for "--sat="

$modified: Tue Jul  3 10:22:20 CEST 2012
  new argument:  --stats=<comma separated list>
  assign statsTypes, by default statsTypes=['ssx','bsx'] 

$modified: Wed Jul  4 11:17:46 CEST 2012
  pointing stepsize is an option.  default 5 seconds

$modified:  Wed Jul 11 15:59:19 CEST 2012
  new argument:  RFcalfreq, default is defined in FindSat() etc depending on cal source 

  --force is equivalent to --now

$modified: Tue 24 Jul 2012 17:26:40 CEST
  print warning if unknown option

$modified: Tue 24 Jul 2012 18:19:51 CEST
  --statistics is equivalent to --stats

  --comment : add extra comments to the FITS file

$modified: Wed 25 Jul 2012 15:43:29 CEST
  nbeamlets converted to bandwidth here, so it's not required in the main script

$modified: Fri 27 Jul 2012 15:27:50 CEST
  starting to implement possibility to choose different pointing stepsizes
  for digital pointing, RF pointing, and pointing during calibration
   stepsize
   RFstepsize
   calibration_stepsize

  --tobia : do the Tobia Experiment, ie. RFstepsize=15*60

  so far, this is only used in CasA_longtracking.py

$modified: Mon 30 Jul 2012 15:43:54 CEST
  default for statistics is all three: bsx,ssx,csx

$modified: Mon 30 Jul 2012 18:14:13 CEST
  added options:
   --RFstepsize=
   --calibration_stepsize=

$modified: Wed 01 Aug 2012 16:14:37 CEST
  added option
  --beams=   default: ['A']

$modified: Fri 08 Feb 2013 17:25:06 CET
  satname is replaced by obsfunc.satname
  (main scripts don't use satname on it's own anymore)

$modified: Thu 14 Feb 2013 11:43:18 CET
  giving an unknown option is now a fatal error.  script quits

*** IMPORTANT: To do first: locate SCU software base directory   ***
# For this, set python path to search SCU software packages:
# The Development Tree is in /home/scuadmin/software/SCU-develop/
# The stable tree is in the default python location
#       /usr/lib/python2.6/dist-packages 

$modified: Fri 15 Feb 2013 12:40:28 CET
  finding SCUconf.py using FullPathname
  note that PYTHONPATH must include the path to ObsFunctions.py
  usually found in ~/torchinsky/scripts

  modified high speed acquisition setup
  now we have Beam-A and Beam-B, first lane of each on Andante

  FindSat() is now here instead of in the main scripts

$modified: Mon 18 Feb 2013 16:52:26 CET
  Scriptname() returns False if script is too old, and probably incompatible

$modified: Wed 20 Feb 2013 13:53:26 CET
  rearrangement:  argument parsing is done first
                  followed by executing SCUConf

  new option: --testmode
              testmode can be forced on the SCU

$modified: Wed 20 Feb 2013 18:38:52 CET
  adding usage text

$modified: Mon 04 Mar 2013 10:29:11 CET
  number of beamlets per block, for fast data acquisition
    this is a maximum of 62, and set equal to nBeamlets if nBeamlets<62

$modified: Wed 12 Jun 2013 10:25:21 CEST
  new argument:  --driftduration=N
  this is the duration of the drift scan in the calibration phase

$modified: Fri 14 Jun 2013 11:13:52 CEST
  default number of beamlets is 61 instead of 62, until we flash the RSPs

$modified: Mon 08 Jul 2013 14:04:41 CEST
  option for driftduration assigns value to obsfunc.caldriftduration

  all command option arguments should set as class variable in ObsFunctions.py
  this has not been implemented yet.  one day...

$modified: Tue 09 Jul 2013 12:16:44 CEST
  removed variables here: RFcentre and RFcalfreq

$modified: Thu 15 Aug 2013 16:32:31 CEST
  beams:  can accept "both" and also lower case

$modified: Mon 26 Aug 2013 12:40:04 CEST
  str2dt() and tot_seconds() are in datefunctions.py
  no longer a method of class ObsFunctions

$modified: Fri 06 Sep 2013 12:35:13 CEST
  caldate: convert to datetime object

  working on implementing separate pointings for Beam-A and Beam-B
  new options:  --A=<source name> and --B=<source name>
  source,offsource are now class variables (dictionaries) in ObsFunctions
  option --source=<source name> still valid, and it assigns the same source to both Beam-A and Beam-B

  RFBeam creation is now here.

$modified: Fri 15 Nov 2013 15:15:18 CET
  changes towards independence of A and B
  the idea is to have an obsfunc object for each RF beam

$modified: Sun 17 Nov 2013 08:19:47 CET
  continuing towards independence of Beam-A and Beam-B:
  options to allow differences for A and B.  prefix by the beam name
  --A:RFcalfreq=
  --B:RFcentre=
  etc, or
  --RFcentre=   to apply to both beams

$modified: Mon 18 Nov 2013 09:56:03 CET
  continuing...
  duration is now a parameter of the obsfunc object

  makeRFBeam removed.  it was assigning the centre frequency.
  instead, this is done in FindSat() or later in the main script if no calibrator chosen

$modified: Thu 06 Feb 2014 10:50:55 CET
  IP and MAC addresses changed for Beam-B RSP to allow broadcasting 

$modified: Wed 19 Feb 2014 12:40:09 CET
  minor change: comments added by obsfunc[beam] instead of obsfunc['A']

$modified: Tue 18 Mar 2014 11:01:36 CET
  eliminating SCUconf.py, stuff that was there is now here.

$modified: Wed 16 Apr 2014 12:51:57 CEST
  yoffset is given for each Beam separately.
  "offsource=" keyword can also be "off="

$modified: Wed 30 Jul 2014 16:34:33 CEST
  horizon option:  can specify any horizon, instead of 45 degrees

$modified: Thu 31 Jul 2014 10:27:13 CEST
  bug fix: help text erroneously said that default duration is "while visible"
  new duration option: "visible" to observe while visible

$modified: Thu 27 Aug 2015 15:16:41 CEST
  update help text

$modified: Wed 23 Sep 2015 16:55:10 CEST
  backend broadcast mode for Beam-A

"""
submittime=dt.datetime.utcnow()

try:
    if not isinstance(helptxt,str):helptxt=""
except:
    helptxt=""

import sys
def usage(helptxt):
    print '\n'+os.path.split(sys.argv[0])[1]
    print '\n'+helptxt+'\n'
    print 'usage: '+sys.argv[0]+' [options]'

    print 'The options below can be prefixed by beam name followed by a colon'
    print 'to apply the option to only one beam or the other.'
    print 'By default, an option is applied to both beams.'
    print 'for example:  --A:source=b0329\n'

    print 'OPTIONS:\n'
    print ' --cal=<source name>'
    print '      source to use for calibration.  eg. Sun, "GPS BIIF-1", some'
    print '      nicknames are also permitted. eg f1, f2, a26.\n'
    print '      It is also possible to use stored calibration parameters by giving\n'
    print '      the name of a file.  e.g. --cal=$HOME/RFcalibrationParameters_Beam-A_0970MHz.txt\n'

    print ' --date=<observation date>'
    print '      date given in UT in format "YYYY-MM-DD HH:MM" or "YYYY-MM-HH" or "HH:MM"'
    print '      it is also possible to specify "now"'
    print '      the observation will begin at the following transit time of the source'
    print '      (but see "--force" below)'
    print '      default is next transit time of the source\n'

    print ' --caldate=<calibration date>'
    print '      time to start the calibration run'
    print '      default is the next transit time of calibration source\n'

    print ' --yoffset=N'
    print '      during the calibration run, the Y direction of the digital beam will be set at a position'
    print '      corresponding to N time minutes later in the trajectory of the calibration source'
    print '      default is 4 minutes\n'

    print ' --force'
    print '      force the observation to begin at the time requested, and not at the next transit time\n'
    
    print ' --nbeamlets=N'
    print '      number of beamlets per beam (default 61)\n'

    print ' --source=<source name>'
    print '      source name must be included in the catalog, some nicknames are valid'
    print '      examples: CasA, CygA, PSR B0329+54, b0329, ...\n'

    print ' --offsource=<source name>'
    print '      pointing for the off source position'
    print '      keywords such as "ahead" or "behind" are valid\n'

    print ' --nosatdrift'
    print '      do not do a drift scan after the calibration'
    print '      Note that a drift scan is very useful for verifying the calibration\n'

    print ' --driftduration=N'
    print '      duration of the drift scan during the calibration phase in minutes\n'

    print ' --duration=N'
    print '      duration of the observation in minutes (default 30 minutes)'
    print '      to observe while the source is visible, set duration=visible\n'

    print ' --RFcentre=N'
    print '      RF centre frequency in MHz'
    print '      default is the same as RFcalfreq (see below)\n'

    print ' --RFcalfreq=N'
    print '      frequency to use for RF calibration in MHz'
    print '      default is determined by the calibration source'
    print '      eg.  for GPS BIIF-1, RFcalfreq=1176.45 by default\n'
    
    print '--nohorizon'
    print '      ignore the nominal horizon of 45 degrees'
    print '      use this option to point at sources below 45 degrees elevation\n'

    print '--horizon=N'
    print '      reset the horizon to N degrees'
    print '      use this option to point at sources below 45 degrees elevation\n'

    print '--tilesets=<range>'
    print '      select tilesets active for the observation'
    print '      USE WITH CAUTION'
    print '      default is --tilesets=0:15\n'

    print '--nodigtrack'
    print '      track the source on the sky with RF tracking only\n'

    print '--nodigcal'
    print '      do not do a digital calibration\n'

    print ' --statistics=<list>'
    print '      select which statistics are recorded'
    print '      eg. --statistics=bsx,ssx'
    print '      default is all three, ie. --statistics=bsx,ssx,csx\n'

    print '--stepsize=N'
    print '      pointing step size in seconds.'
    print '      while tracking, the pointing is recalculated every N seconds'
    print '      default is N=5'
    print '      NOTE: this is valid only for AZEL tracking (eg. script SourceTrack_AZEL.py)\n'

    print '--RFstepsize=N'
    print '      pointing step size in seconds for RF pointing'
    print '      default is the same as stepsize (see above)'
    print '      NOTE: this is valid only for AZEL tracking (eg. script SourceTrack_AZEL.py)\n'

    print '--calibration_stepsize=N'
    print '      pointing step size during calibration'
    print '      default is the same as stepsize (see above)\n'

    print '--comment="string"'
    print '      add an extra comment to the FITS file'
    print '      this option can be specified any number of times and all comments will be added.\n'
    
    print '--beams=<list>'
    print '      RF beams to configure'
    print '      eg. --beams=A,B or --beams=both'
    print '      default is A only, ie. --beams=A\n'

    print '--testmode'
    print '      do not connect to the LCU'
    print '      run the script in test mode\n'

    print '--help'
    print '      print his help text\n'

    return




import os,time,datetime as dt
from datefunctions import *
from ObsFunctions import *
obsfunc={}
obsfunc['A']=ObsFunctions('A')
obsfunc['B']=ObsFunctions('B')

# parse arguments input into the observation script
# note that since this OBSinit.py script is taken with execfile
# the variables here will live into the observation script
# and sys.argv refers to the calling script
now=False
nbeamlets=None
do_satdrift=True # do a quick satellite drift scan after calibration yes/no
digtrack=True
digcal=True
statsTypes=['ssx','bsx','csx']
stepsize=5.0
RFstepsize=5.0
calibration_stepsize=5.0
tobia=False
extraComments=[]
beams=['A']
testmode=False
tilesets='0:15'
TotalRSPboards=2
for fullarg in sys.argv:
    if fullarg==sys.argv[0]:continue

    # by default, an option is applied to both beams
    arg=fullarg.replace('A:','').replace('B:','')
    if fullarg[0:4]=='--A:':argbeams=['A']
    elif fullarg[0:4]=='--B:':argbeams=['B']
    else: argbeams=['A','B']

    if arg.find("--sat=")==0 or arg.find("--satname=")==0 or arg.find("--cal=")==0:
        satname=arg.split('=')[1]
        for beam in argbeams:obsfunc[beam].satname=satname
        continue
    if arg.find("--date=")==0:
        obsdate=arg.split('=')[1]
        obsdate=str2dt(obsdate)
        for beam in argbeams:obsfunc[beam].obsdate=obsdate
        continue
    if arg.find("--caldate=")==0:
        caldate=arg.split('=')[1]
        caldate=str2dt(caldate)
        for beam in argbeams:obsfunc[beam].caldate=caldate
        continue
    if arg.find("--yoffset=")==0:
        for beam in argbeams:obsfunc[beam].drift_yoffset=float(arg.split('=')[1])
        continue
    if arg.find("--now")==0 or arg.find("--force")==0:
        now=True
        continue
    if arg.find("--beamlets=")==0 or arg.find("--nbeamlets=")==0:
        nbeamlets=int(arg.split('=')[1])
        for beam in argbeams:obsfunc[beam].nBeamlets(nbeamlets)
        continue
    if arg.find("--source=")==0:
        source=arg.split('=')[1]
        for beam in argbeams:obsfunc[beam].source=source
        continue
    if arg.find("--offsource=")==0 or arg.find("--off=")==0:
        offsource=arg.split('=')[1]
        for beam in argbeams:obsfunc[beam].offsource=offsource
        continue
    if arg.find("--nosatdrift")==0:
        do_satdrift=False
        continue
    if arg.find("--driftduration=")==0:
        driftduration=eval(arg.split('=')[1])
        for beam in argbeams:obsfunc[beam].caldriftduration=dt.timedelta(minutes=driftduration)
        continue
    if arg.find("--duration=")==0:
        str_duration=arg.split('=')[1]
        if str_duration.upper()=='VISIBLE':
            duration=None
        else:
            duration=dt.timedelta(minutes=eval(str_duration))
        for beam in argbeams:obsfunc[beam].duration=duration
        continue
    if arg.find("--RFcentre=")==0:
        RFcentre=eval(arg.split('=')[1])
        for beam in argbeams:obsfunc[beam].RFcentre=RFcentre
        continue
    if arg.find("--RFcalfreq=")==0:
        RFcalfreq=eval(arg.split('=')[1])
        for beam in argbeams:obsfunc[beam].RFcalfreq=RFcalfreq
        continue
    if arg.find("--nohorizon")==0:
        for beam in argbeams:obsfunc[beam].embraceNancay.horizon=0.0
        continue
    if arg.find("--horizon=")==0:
        horizon=math.radians(eval(arg.split('=')[1]))
        for beam in argbeams:obsfunc[beam].embraceNancay.horizon=horizon
        continue
    if arg.find("--tilesets=")==0:
        tilesets=arg.split('=')[1]
        continue
    if arg.find("--nodigtrack")==0:
        digtrack=False
        continue
    if arg.find("--nodigcal")==0:
        digcal=False
        continue
    if arg.find("--stats=")==0 or arg.find("--statistics=")==0:
        statsTypes=arg.split('=')[1].split(',')
        continue
    if arg.find("--stepsize=")==0:
        stepsize=eval(arg.split('=')[1])
        for beam in argbeams:obsfunc[beam].stepsize=stepsize
        continue
    if arg.find("--RFstepsize=")==0:
        RFstepsize=eval(arg.split('=')[1])
        for beam in argbeams:obsfunc[beam].RFstepsize=RFstepsize
        continue
    if arg.find("--calibration_stepsize=")==0:
        calibration_stepsize=eval(arg.split('=')[1])
        for beam in argbeams:obsfunc[beam].calibration_stepsize=calibration_stepsize
        continue
    if arg.find("--comment=")==0:
        extraComments.append(arg.split('=')[1])
        continue
    if arg.find("--tobia")==0:
        tobia=True
        continue
    if arg.find("--beams=")==0:
        if arg.split('=')[1].upper()=='BOTH':
            beams=['A','B']
        else:
            beams=arg.split('=')[1].split(',')
            for i in range(len(beams)):beams[i]=beams[i].upper()
        continue
    if arg.find("--testmode")==0:
        testmode=True
        continue
    if arg.find("--help")==0:
        usage(helptxt)
        quit()


    print '\n\nERROR - unknown option : '+arg
    print 'for a list of options try\npython '+sys.argv[0]+' --help\n'
    quit()


#### The original OBSinit began here ##########
from SCU.PackageTools.ModuleShareObjects\
    import LCUip__var,LCUconnect__var
from SCU.PackageFiles.ModuleLogFiles\
    import TLogCmd
from SCU.PackageTools\
    import ModuleShareObjects
ModuleShareObjects.SCUlogfile__obj=TLogCmd()
from SCU.PackageTools.ModuleShareObjects import SCUlogfile__obj

from SCU.PackageObservation.ModuleSCANClient\
    import TSCANClient
from SCU.PackageBackend.ModuleBACKENDClient\
    import TBACKENDClient
from SCU.PackageObservation.ModuleObservation\
    import TObservation
from SCU.PackageTools.ModuleTools\
    import KeyInit,KeyPressed,KeyEnd,WaitUntilTime,IsTimeArrived,PrintOver
from SCU.PackageBackend.ModuleSubbandStatistics\
    import TSubbandStatistics,TSubbandStatisticsList
from SCU.PackageBackend.ModuleBeamletStatistics\
    import TBeamletStatistics,TBeamletStatisticsList
from SCU.PackageBackend.ModuleCrossletStatistics\
    import TCrossletStatistics
from SCU.PackageFiles.ModuleStatisticsFile\
    import TSubbandStatisticsFITSfile,TBeamletStatisticsFITSfile,TCrossletStatisticsFITSfile

if SCUlogfile__obj==None:
    print "ERROR:  SCUlogfile__obj is not defined (satorchi_OBSinit.py line 522)"
    quit()

# Connect to the LCU?
#   True if connect to LCU
#   False for no LCU connection (LCU not available, no LCU, etc for testing)
# the following script sets the LCUconnect__var, 
initfile=FullPathname('ConnectLCU.py')
if initfile==None:quit()
execfile(initfile)


#### MyObservation is the parent object of both RF beams
MyObservation=TObservation()
MyObservation.ObsConfig.SetTelescop("EMBRACE@Nancay")
MyObservation.ObsConfig.SetLongitude("2/11/50")
MyObservation.ObsConfig.SetLatitude("47/22/48")
ModuleShareObjects.OBS__obj=MyObservation  #shares Observation object

# Configure for Beam-A and Beam-B
SCAN={}
Backend_Client={}
index={}
index['A']=0
index['B']=1


####### high speed data acquisition addresses etc ################
macacq={}
ipacq={}
numlane=0 # we're only using the first lane

##################################################################
# MAC for Borsen machine (verify): 00:1B:21:00:3E:77
# this seems to be an error.  It shouldn't be on the Nancay general network
# ipacq=[193,55,144,75];macacq=[0x00,0x1B,0x21,0x00,0x3E,0x77]
##################################################################

# for Aris machine, we use: IP 10.10.0.77, and MAC=00:0E:0C:A9:DF:92
# NOTE:  MAC should be given in reverse order!
#macacq['A']=[0x92,0xDF,0xA9,0x0C,0x0E,0x00]
#ipacq['A']=[10,10,0,77]

####### for broadcasting
macacq['A']=[0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]
ipacq['A']=[10,10,0,255]

####### this was for lane-1 beamA
### macacq=[0x93,0xDF,0xA9,0x0C,0x0E,0x00]
### ipacq=[10,10,1,77]

### using eth2 on Andante
#macacq['B']=[0x93,0xDF,0xA9,0x0C,0x0E,0x00]
#ipacq['B']=[10,11,0,77]

####### for broadcasting
macacq['B']=[0xFF,0xFF,0xFF,0xFF,0xFF,0xFF]
ipacq['B']=[10,11,0,255]

for beam in beams:

    # access to MyObservation within the obsfunc object
    obsfunc[beam].setMyObservation(MyObservation)

    #### give the scriptname for logging
    if not obsfunc[beam].Scriptname(sys.argv[0],submittime=submittime):quit()

    # Backend client to acquire statistics later
    # Not used to configure backend, 
    # because RCU, ECI, Data Output are configured with observation configuration command (scan init)
    Backend_Client[beam]=TBACKENDClient(TotalRSPboards,
                                        backend_number=index[beam],
                                        lcuconnect=LCUconnect__var,
                                        lcuip=LCUip__var)
    if(Backend_Client[beam].connected == False):   
        obsfunc[beam].log('ERROR - Unable to Connect to LCU for Beam '+beam)
        quit()

    # if you want to select only some RCUs
    # it can be specified on the command line.  default: tilesets="0:15" (see SCUconf.py)
    Backend_Client[beam].backend.rcu.SelectNone()
    Backend_Client[beam].backend.rcu.select(tilesets)

    # CREATES SCAN CLIENT FOR LCU COMMANDS (for instance: A for Backend A, B for Backend B)
    # Specify backend number (0 or 1, default=0)
    SCAN[beam]=TSCANClient(TotalRSPboards,
                           backend_number=index[beam],
                           lcuconnect=LCUconnect__var,
                           lcuip=LCUip__var)

    if(SCAN[beam].connected == False):   
        print 'ERROR - Unable to Connect to LCU'
        quit()

    if SCUlogfile__obj==None:
        print "ERROR:  SCUlogfile__obj is not defined (satorchi_OBSinit.py line 615)"
        quit()

    # to ensure that a previous obs is not running
    SCAN[beam].Stop()
    time.sleep(2)  # allow time for LCU to stop processes

    obsfunc[beam].log("Total RSPboards: "+str(TotalRSPboards))

    # select tilesets (12 tilesets connected on 16 sep 2010)
    # select tilesets (16 tilesets connected on  1 jul 2011)
    SCAN[beam].scan.backend.rcu.SelectNone()
    SCAN[beam].scan.backend.rcu.select(tilesets)
    obsfunc[beam].log('using tilesets: '+tilesets)

    ###### high speed data acquisition configuration ###############
    if obsfunc[beam].nBeamlets()>=61:
        nof_beamlet_per_block=61
    else:
        nof_beamlet_per_block=obsfunc[beam].nBeamlets()

    SCAN[beam].scan.backend.SetAcq(ipacq[beam],
				   macacq[beam],
				   numlane,
				   nof_beamlet=nof_beamlet_per_block)
    ####### end configuration high speed data acquisition ###########

    # check if successfully connected to the backend
    if not Backend_Client[beam].connected:
	    obsfunc[beam].log("ERROR: LCU Backend Client not connected for Beam "+beam)
	    quit()

    # Create an observation object with the SCAN object
    # and associate it with backend clients to read statistics 
    #
    # inelegant hack to get around the hard wired A and B in PR's API
    code='MyObservation.BackendEnabled_'+beam+'=True'
    exec code
    code='MyObservation.backend'+beam+'=Backend_Client[beam]'
    exec code
    code='MyObservation.scan'+beam+'=SCAN[beam]'
    exec code
    

    # ===== read the TLE data for the required satellite ==================
    satname=obsfunc[beam].FindSat()
    if satname==None:
        obsfunc[beam].log('WARNING: calibrator has not been chosen.')
    # frequencies have been selected by FindSat() above, 
    # or they can be specified on the command line, or placed in the main script
    # by assigning obsfunc.RFcentre and obsfunc.RFcalfreq before calling obsfunc.satcal()
    # =====================================================================

### end of loop over Beam-A and Beam-B


# add extra comments to the FITS file
# this need only be done for one of the obsfunc objects.  MyObservation is common to both RF beams
for comment in extraComments:
    obsfunc[beam].addFITScomment(comment)

print "End OBSinit"
