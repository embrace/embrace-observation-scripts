"""
$Id: ConnectLCU.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Tue Nov 15 15:51:13 CET 2011  (at least)


set LCUconnect__var to True if you want to connect to the LCU
set it to False if your testing scripts without communication to the LCU

$modified: Tue 19 Feb 2013 17:39:38 CET
  making this script usable on any machine, so I don't keep track separately

  default is to connect if we're on the SCU
  default is test mode otherwise

$modified: Wed 01 Mar 2017 16:01:34 CET
  using LCU ip address on the EMBRACE private network

"""
LCUip__var='192.168.1.5'  # LCU on the private EMBRACE network
#LCUip__var='193.55.144.60'  #official LCU ip
#LCUip__var='193.55.144.168'  #LCU portable ip

import socket
hostname=socket.gethostname()
if hostname=='scu-desktop':
    LCUconnect__var=True
else:
    LCUconnect__var=False

## check if we want to override LCUconnect
try:
    if testmode:LCUconnect__var=False
except:
    pass

ModuleShareObjects.LCUip__var=LCUip__var
ModuleShareObjects.LCUconnect__var=LCUconnect__var

if LCUconnect__var:
    print '*************** CONNECTING TO LCU ****************'
else:
    print '*********** TEST MODE: NO CONNECTION TO LCU ******'


