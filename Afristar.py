#! /usr/bin/env python
"""
$Id: Afristar.py
$auth: Steve Torchinsky <torchinsky@obs-nancay.fr>
$created: Thu May 17 09:28:15 CEST 2012

$modified: Thu 19 Jul 2012 16:46:27 CEST
  changed wait time before starting from 1 minute to 20 seconds

$modified: Wed 25 Jul 2012 15:44:45 CEST
  using obsfunc.makeDigitalBeam()

$modified: Wed 01 Aug 2012 16:13:18 CEST
  beams variable is moved to satorchi_OBSinit.py as an option

  create RF Beam section now a method in ObsFunctions

$modified: Sat 16 Feb 2013 06:34:13 CET
  updating for changes to ObsFunctions
  using FullPathname() to find init file

$modified: Mon 18 Nov 2013 09:16:31 CET
  updating for changes to ObsFunctions and OBSinit
  makeRFBeam() is in satorchi_OBSinit.py
  obsfunc object for each RFBeam

"""

import sys,os
import datetime as dt
import ephem as eph

#TotalRSPboards=3
#sys.argv.append('--source=Afristar')
# ===== initialize, including parsing arguments on the command line ===
from ObsFunctions import *
initfile=FullPathname('satorchi_OBSinit.py')
if initfile==None:quit()
execfile(initfile)
# =====================================================================

# === Observation title is applied for both beams =====================
# source name given on the command line, there is no default
dtnow=dt.datetime.utcnow()
ObsTitle=''
comment="calibration and pointing on Afristar"
az=155.10
alt=32.55
for beam in beams:
	obsfunc[beam].source='Afristar'
	obsfunc[beam].RFcalfreq=1489.0
	if not obsfunc[beam].assignRFcentre(1474.5): quit()

	ttl=obsfunc[beam].ObsTitle()
	if ttl==None: quit()
	ObsTitle+=ttl

	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
# =====================================================================

# ====== Initialize MyObservation  ====================================
if not obsfunc['A'].initialize(ObsTitle):quit()
# =====================================================================

for beam in beams:
# ======== Start digital beam config ===================================
	DigBeam=obsfunc[beam].makeDigitalBeam()
	if DigBeam==None: quit()
# ========= End digital beam config ====================================

# ======== get RF Beam object ==========================================
	RFBeam=obsfunc[beam].getRFBeam()
	if RFBeam==None: quit()
# ======================================================================

# === statistics acquisition ==========================================
# give acquisition start and end times
# default: start in 40 seconds from now, end 20 mins later
	obstag=[] # make sure there are as many AcqStarts and AcqEnds as obstags
	obstag.append('Afristar')
	AcqStarts=[]
	AcqEnds=[]
	if obsfunc[beam].obsdate==None:
		AcqStarts.append(dtnow+dt.timedelta(seconds=40))
	else:
		AcqStarts.append(obsfunc[beam].obsdate)

	if obsfunc[beam].duration==None:
		AcqEnds.append(AcqStarts[0]+dt.timedelta(minutes=20))
	else:
		AcqEnds.append(AcqStarts[0]+obsfunc[beam].duration)
# =====================================================================

# ========= point to Afristar =========================================
	CmdTime=AcqStarts[0]+dt.timedelta(minutes=1)
	obsfunc[beam].embraceNancay.date=CmdTime

	comment=obsfunc[beam].source+" is at  az="+str(az)+" alt="+str(alt)
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)

	# time to allow for the RF calibration in seconds
	# note that RF cal cannot be done simultaneously on A and B.
	rfcaldelta=dt.timedelta(seconds=obsfunc[beam].rfcaltime)

	comment='RF calibration beginning at '+isodate(CmdTime)
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
	RFBeam.SetCal('ssx',fcal=obsfunc[beam].RFcalfreq) 
	RFBeam.AddSubscan(CmdTime,'AZEL',az,alt)
	CmdTime+=rfcaldelta

	RFBeam.SetCal('current',fcal=obsfunc[beam].RFcalfreq) 
	RFBeam.AddSubscan(CmdTime,'AZEL',az,alt)

	if digcal:
		comment='digital calibration beginning at '+isodate(CmdTime)
		obsfunc[beam].log(comment)
		obsfunc[beam].addFITScomment(comment)
		DigBeam.SetCal('csx')
		DigBeam.AddSubscan(CmdTime,'AZEL',az,alt)

		# time to allow for the digital calibration in seconds
		digcaltime=10.0+1.1*obsfunc[beam].nBeamlets()
		CmdTime+=dt.timedelta(seconds=digcaltime)

	comment='tracking by AZEL beginning at '+isodate(CmdTime)
	obsfunc[beam].log(comment)
	obsfunc[beam].addFITScomment(comment)
	DigBeam.SetCal('current')
	DigBeam.AddSubscan(CmdTime,'AZEL',az,alt)

# === end pointing configuration =======================================


# === Define  STATISTICS ACQUISITION phases ============================
	if not obsfunc[beam].Acquire(obstag,AcqStarts,AcqEnds,statsTypes):
		print "\nObservation NOT submitted\n"
		quit()
# ======================================================================

# === finally, submit the observation ==================================
obsfunc[beam].submit()

